@extends('api/header')

@section('content')
<style type="text/css">
	#map {
        height: 100%;
        min-height: 250px;
      }
	.timepicker-hour {
		position: absolute;
		left: 15%;
		top: 38%;
	}

	.timepicker-minute {
		position: absolute;
		right: 15%;
		top: 38%;
	}
</style>

<section class="b-contacts s-shadow">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="b-contacts__form">
					<header class="b-contacts__form-header s-lineDownLeft zoomInUp" data-wow-delay="0.5s">
						<h2 class="s-titleDet">Formulir Pesan Sparepart</h2> 
					</header>

					@if(Session::has('done'))
		                <div class="alert alert-success">
						  	{{Session::get('done')}}
						</div>
		            @endif

					<form action="{{ url('/layanan-pesan-sparepart-api') }}" method="POST" id="contactForm" novalidate class="s-form zoomInUp" data-wow-delay="0.5s">
						
						{!! csrf_field() !!}
						
						<div class="col-md-4">
							@if ($errors->has('cabang_id'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Cabang belum ada yang dipilih !</h6></strong></p>
	                      	@endif
							<div class="s-relative">
								<select name="cabang_id" id="user-topic" class="m-select" required>
									<option value="">Pilih Cabang</option>
									@foreach($cabang as $data_content_cabang)
									<option value="{{$data_content_cabang->id}}">{{$data_content_cabang->name}}</option>
									@endforeach
								</select>
								<span class="fa fa-caret-down"></span>
							</div>
						</div>
						
						<div class="col-md-4">
							@if ($errors->has('jenis_kendaraan'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Jenis Kendaraan tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<div class="s-relative">
								<select name="jenis_kendaraan" id="user-topic" class="m-select" required>
									<option value="">Pilih Jenis Kendaraan</option>
									<option value="cub">CUB / Bebek</option>
			                        <option value="matic">Matic</option>
			                        <option value="sport">Sport</option>
								</select>
								<span class="fa fa-caret-down"></span>
							</div>
						</div>

						<div class="col-md-4">
							@if ($errors->has('product_id'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Kendaraan belum ada yang dipilih !</h6></strong></p>
	                      	@endif
							<div class="s-relative">
								<select name="product_id" id="user-topic" class="m-select" required>
									<option value="">Pilih Kendaraan Anda</option>
									@foreach($produk as $data_content_produk)
									<option value="{{$data_content_produk->id}}">{{$data_content_produk->name}}</option>
									@endforeach
								</select>
								<span class="fa fa-caret-down"></span>
							</div>
						</div>

						<div class="col-md-3">
							@if ($errors->has('fullname'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Nama Lengkap tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input type="text" placeholder="Nama Lengkap" name="fullname" id="user-name" required="required" />
						</div>

						<div class="col-md-3">
							@if ($errors->has('email'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Email tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input type="text" placeholder="Email" name="email" id="user-email" required />
						</div>
						
						<div class="col-md-3">
							@if ($errors->has('no_telepon'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Nomor Telepon tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input type="text" placeholder="No. Telepon" name="no_telepon" id="user-phone" required />
						</div>

						<div class="col-md-3">
							@if ($errors->has('tahun_rakitan'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Tahun Rakitan tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input type="text" placeholder="Tahun Rakitan" name="tahun_rakitan" id="user-phone" required />
						</div>

						<div class="col-md-12">
							<textarea id="user-message" name="description" placeholder="Keterangan" required></textarea>
						</div>

						<div class="col-md-12">
							<button type="submit" class="btn m-btn">Kirim<span class="fa fa-angle-right"></span></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section><!--b-contacts-->

<style type="text/css">
  input::-webkit-input-placeholder{
    color:white !important;
  }
  textarea::-webkit-input-placeholder{
    color:white !important;
  }
  input:-moz-placeholder {
      color:white !important;
  }
  textarea:-moz-placeholder {
      color:white !important;
  }
</style>
@endsection