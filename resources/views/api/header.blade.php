<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>Auto Club</title>

		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

		<link href="{{ URL::asset('public/css/master.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{ URL::asset('/public/css/bootstrap-datepicker3.css') }}"/>
		<!-- SWITCHER -->
		<link rel="stylesheet" id="switcher-css" type="text/css" href="{{ URL::asset('public/assets/switcher/css/switcher.css') }}" media="all" />

		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

	@if($mListTable == 1)
	<?php 
	$body_class = '';
	$body_class = 'm-listTable'; 
	?>
	@else
	<?php $body_class = 'm-index'; ?>
	@endif
	<body class="<?php echo($body_class); ?>" data-scrolling-animations="true" data-equal-height=".b-auto__main-item">

		@yield('content')

		@yield('google_map_script')
		
		<!--Main-->   
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
		<script src="{{ URL::asset('public/js/jquery-1.11.3.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery-ui.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/modernizr.custom.js') }}"></script>

		<script src="{{ URL::asset('public/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/waypoints.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery.easypiechart.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/classie.js') }}"></script>

		<!--Switcher-->
		<script src="{{ URL::asset('public/assets/switcher/js/switcher.js') }}"></script>
		<!--Owl Carousel-->
		<script src="{{ URL::asset('public/assets/owl-carousel/owl.carousel.min.js') }}"></script>
		<!--bxSlider-->
		<script src="{{ URL::asset('public/assets/bxslider/jquery.bxslider.js') }}"></script>
		<!-- jQuery UI Slider -->
		<script src="{{ URL::asset('public/assets/slider/jquery.ui-slider.js') }}"></script>

		<!--Theme-->
		<script src="{{ URL::asset('public/js/jquery.smooth-scroll.js') }}"></script>
		<script src="{{ URL::asset('public/js/wow.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery.placeholder.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/theme.js') }}"></script>

		@yield('datepicker_js')
	</body>
</html>