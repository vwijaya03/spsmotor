@extends('api/header')

@section('content')

<div class="b-items">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-8 col-xs-12">
				<div class="row">
					@if(count($content_product) != null)
						@foreach($content_product as $data_content_product)
						<div class="col-lg-3 col-md-6 col-xs-12">
							<div class="b-items__cell zoomInUp" style="height: 475px !important;">
								<div class="b-items__cars-one-img">
									<img class='img-responsive' height="200px" src="{{ URL::asset($data_content_product->path) }}" alt='chevrolet'/>
								</div>
								<div class="b-items__cell-info">
									<div class="b-items__cell-info-title">
										<h2 class="">{{ $data_content_product->name }}</h2>
									</div>
									<p style="height: 60px;">
										{{ strip_tags(substr($data_content_product->description, 0, 160)) }} 
					                    @if(strlen($data_content_product->description) >= 160)
					                    ...
					                    @else
					                    @endif
									</p>
									<div class="row m-smallPadding">
										<div class="col-xs-6">
											<a href="{{ url('/produk-api/'.$data_content_product->slug) }}" class="btn m-btn">LIHAT DETAIL<span class="fa fa-angle-right"></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p align="middle">Data tidak di temukan.</p>
					@endif
				</div>
			</div>
		</div>
	</div>
</div><!--b-items-->

@endsection