@extends('api/header')

@section('content')

<section class="b-best">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-xs-12">
				<div class="b-best__info">
					<header class="s-lineDownLeft b-best__info-head">
						<h2 class="zoomInUp" data-wow-delay="0s">SPS Motor Dealer Honda Resmi</h2>
					</header>
					<?php echo($profile->description); ?>
				</div>
			</div>
			
		</div>
	</div>
</section><!--b-best-->

@endsection