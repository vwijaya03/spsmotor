@extends('api/header')

@section('content')

<section class="b-detail s-shadow">
	<div class="container">
		<header class="b-detail__head s-lineDownLeft zoomInUp" data-wow-delay="0.5s">
			<div class="row">
				<div class="col-sm-9 col-xs-12">
					<div class="b-detail__head-title">
						<h1>{{$content_product->name}}</h1>
					</div>
				</div>
			</div>
		</header>
		<div class="b-detail__main">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="b-detail__main-info">
						<div class="b-detail__main-info-images zoomInUp" data-wow-delay="0.5s">
							<div class="row m-smallPadding">

								<!-- Large Picture -->
								<div class="col-xs-10">
									<ul class="b-detail__main-info-images-big bxslider enable-bx-slider" data-pager-custom="#bx-pager" data-mode="horizontal" data-pager-slide="true" data-mode-pager="vertical" data-pager-qty="5">
										@if(count($content_product_image) != 0)
											@foreach($content_product_image as $data_content_product_image)
												<li class="s-relative">                                        
													<img class="img-responsive center-block" src="{{ URL::asset($data_content_product_image->path) }}" />
												</li>
											@endforeach
										@else
											Data Tidak Di Temukan.
										@endif
									</ul>
								</div>

								<!-- Small Picture -->
								<div class="col-xs-2 pagerSlider pagerVertical">
									<div class="b-detail__main-info-images-small" id="bx-pager">
										@if(count($content_product_image) != 0)
											<?php $i = 0; ?>
											@foreach($content_product_image as $data_content_product_image)
												<a href="#" data-slide-index="{{$i}}" class="b-detail__main-info-images-small-one">
													<img width="100%" class="img-responsive" src="{{ URL::asset($data_content_product_image->path) }}" />
												</a>
											<?php $i++; ?>
											@endforeach
										@else
											Data Tidak Di Temukan.
										@endif
										
									</div>
								</div>
							</div>
						</div>

						<div class="b-detail__main-info-text zoomInUp" data-wow-delay="0.5s">
							<div class="b-detail__main-aside-about-form-links">
								<a href="#" class="j-tab m-active s-lineDownCenter"><h2>Informasi</h2></a>
							</div>
							<div>
								<span><?php echo($content_product->description); ?></span> 
							</div>
						</div>

						<div class="b-detail__main-info-text">
							<div class="b-detail__main-aside-about-form-links">
								<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#harga_otr'>Harga OTR</a>
								<a href="#" class="j-tab" data-to='#info1'>Mesin</a>
								<a href="#" class="j-tab" data-to='#info2'>Dimensi</a>
								<a href="#" class="j-tab" data-to='#info3'>Rangka</a>
								<a href="#" class="j-tab" data-to='#info4'>Kelistrikan</a>
							</div>
							<div id="harga_otr">
								<table style="max-width: 55% !important;" class="table table-striped">
								    <tbody>
								      <tr>
								        <td width="10%">Harga OTR</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->price) {{$content_product->price}} @else - @endif</td>
								      </tr>
								    </tbody>
								  </table>
							</div>
							<div id="info1">
								<table style="max-width: 55% !important;" class="table" style="background-color:red">
								    <tbody>
								      <tr>
								        <td width="10%">Tipe Mesin</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_mesin) {{$content_product->tipe_mesin}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Volume Langkah</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->volume_langkah) {{$content_product->volume_langkah}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Sistem Pendingin</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->sistem_pendingin) {{$content_product->sistem_pendingin}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Sistem Suplai Bahan Bakar</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->sistem_suplai_bahan_bakar) {{$content_product->sistem_suplai_bahan_bakar}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Diameter x Langkah</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->diameter_x_langkah) {{$content_product->diameter_x_langkah}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Tipe Transmisi</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_transmisi) {{$content_product->tipe_transmisi}} @else - @endif</td>
								      </tr><tr>
								        <td width="10%">Rasio Kompresi</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->rasio_kompresi) {{$content_product->rasio_kompresi}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Daya Maksimum</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->daya_maksimum) {{$content_product->daya_maksimum}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Torsi Maksimum</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->torsi_maksimum) {{$content_product->torsi_maksimum}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Pola Pengoperan Gigi</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->pola_pengoperan_gigi) {{$content_product->pola_pengoperan_gigi}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Tipe Starter</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_starter) {{$content_product->tipe_starter}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Tipe Kopling</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_kopling) {{$content_product->tipe_kopling}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Kapasitas Minyak Pelumas</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->kapasitas_minyak_pelumas) {{$content_product->kapasitas_minyak_pelumas}} @else - @endif</td>
								      </tr>
								    </tbody>
								  </table>

							</div>
							<div id="info2">
								<table style="max-width: 55% !important;" class="table table-striped">
								    <tbody>
								      <tr>
								        <td width="10%">Panjang x Lebar x Tinggi</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->panjang_lebar_tinggi) {{$content_product->panjang_lebar_tinggi}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Jarak Sumbu Roda</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->jarak_sumbu_roda) {{$content_product->jarak_sumbu_roda}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Jarak Terendah ke Tanah</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->jarak_terendah_ke_tanah) {{$content_product->jarak_terendah_ke_tanah}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Curb Weight</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->curb_weight) {{$content_product->curb_weight}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Kapasitas Tangki Bahan Bakar</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->kapasitas_tangki_bbm) {{$content_product->kapasitas_tangki_bbm}} @else - @endif</td>
								      </tr>
								    </tbody>
								  </table>
							</div>
							<div id="info3">
								<table style="max-width: 55% !important;" class="table table-striped">
								    <tbody>
								      <tr>
								        <td width="10%">Tipe Rangka</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_rangka) {{$content_product->tipe_rangka}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Tipe Suspensi Depan</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_suspensi_depan) {{$content_product->tipe_suspensi_depan}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Tipe Suspensi Belakang</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_suspensi_belakang) {{$content_product->tipe_suspensi_belakang}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Ukuran Ban Depan</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->ukuran_ban_depan) {{$content_product->ukuran_ban_depan}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Ukuran Ban Belakang</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->ukuran_ban_belakang) {{$content_product->ukuran_ban_belakang}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Tipe Rem Depan</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_rem_depan) {{$content_product->tipe_rem_depan}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Tipe Rem Belakang</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_rem_belakang) {{$content_product->tipe_rem_belakang}} @else - @endif</td>
								      </tr>
								    </tbody>
								  </table>
							</div>
							<div id="info4">
								<table style="max-width: 55% !important;" class="table table-striped">
								    <tbody>
								      <tr>
								        <td width="10%">Tipe Baterai / Aki</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_aki) {{$content_product->tipe_aki}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Sistem Pengapian</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->sistem_pengapian) {{$content_product->sistem_pengapian}} @else - @endif</td>
								      </tr>
								      <tr>
								        <td width="10%">Tipe Busi</td>
								        <td width="5%">:</td>
								        <td width="10%">@if($content_product->tipe_busi) {{$content_product->tipe_busi}} @else - @endif</td>
								      </tr>
								    </tbody>
								  </table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section><!--b-detail-->

@endsection