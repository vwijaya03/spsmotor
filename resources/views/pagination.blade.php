<?php
$link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
?>

@if ($paginator->lastPage() > 1)
        <ul class="pagination">
            @if($paginator->currentPage() == 1)
                <li id="news_previous" class="paginate_button page-item previous disabled">
                    <a class="page-link" tabindex="0" href="#">Previous</a>
                </li>
            @else
                <li id="news_previous" class="paginate_button page-item previous">
                    <a class="page-link" tabindex="0" href="{{ $paginator->url($paginator->currentPage()-1) }}">Previous</a>
                </li>
            @endif

            
            
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <?php
                    $half_total_links = ceil($link_limit / 2);
                    $from = $paginator->currentPage() - $half_total_links;
                    $to = $paginator->currentPage() + $half_total_links; 

                    // Log::info("From ".$from);
                    // Log::info("To ".$to);

                    if ($paginator->currentPage() < $half_total_links) {
                        $to += $half_total_links - $paginator->currentPage();
                    }

                    if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                        $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                    }
                ?>
                @if($link_limit % 2 == 0)
                    @if($to > $paginator->lastPage())
                        @if ($from-1 < $i && $i < $to+1)
                            <li class="paginate_button page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                                <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                            </li>
                        @endif
                    @else
                        @if ($from < $i && $i < $to+1)
                            <li class="paginate_button page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                                <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                            </li>
                        @endif
                    @endif
                @else
                    @if ($from < $i && $i < $to)
                        <li class="paginate_button page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                            <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        </li>
                    @endif
                @endif
                
            @endfor
            <li id="news_next" class="paginate_button page-item {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                @if($paginator->currentPage() == $paginator->lastPage())
                    <a class="page-link" tabindex="0">End</a>
                @else
                    <a class="page-link" tabindex="0" href="{{ $paginator->url($paginator->currentPage()+1) }}" >Next</a>
                @endif
            </li>
        </ul>
@endif