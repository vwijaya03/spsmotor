@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection

@section('content')

<!-- Page SubHeader -->
<div class="page-head" @if($bgHeaderProduk != null) style="background-image: url({{ URL::asset($bgHeaderProduk->path) }}) !important;" @endif>
   <div class="container">
      <h3>
         {{$content_product->name}}
      </h3>
      <p class="breadcrumb" style="background-color: transparent !important;">
         <a href="#">Detail Produk</a>
      </p>
   </div>
</div>
<!-- .End Page SubHeader -->

<!-- Section Services -->
<div class="section single-car" @if($bgDetailProduk != null) style="background-image: url({{ URL::asset($bgDetailProduk->path) }}) !important;" @endif>
   <div class="container">
      <div class="row">
         <div class="col-seventh">
            <div id="car-slider" class="flexslider cars-slider">
              <ul class="slides">
              @if(count($content_product_image) != 0)
                  @foreach($content_product_image as $data_content_product_image)
                     <li>
                        <!-- <img style="width: 820px !important; height: 417px !important;" src="{{ URL::asset($data_content_product_image->path) }}" alt/> -->
                        <img style="width: 100% !important; height: 100% !important;" src="{{ URL::asset($data_content_product_image->path) }}" alt/>
                     </li>
                  @endforeach
               @else
                  Data Tidak Di Temukan.
               @endif
                
              </ul>
            </div>
            <div id="carousel" class="flexslider cars-carousel">
              <ul class="slides">
                @if(count($content_product_image) != 0)
                  @foreach($content_product_image as $data_content_product_image)
                     <li>
                        <img style="width: 176 !important; height: 92px !important;" src="{{ URL::asset($data_content_product_image->path) }}" alt/>
                     </li>
                  @endforeach
               @else
                  Data Tidak Di Temukan.
               @endif
              </ul>
            </div>
         </div>
         <div class="col-third">
            <div class="car-info">
               <h3>{{$content_product->name}}</h3>
               <p>
                  {{ strip_tags($content_product->description) }}
               </p>
               
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-ninth" style="padding-left: 15px !important; padding-right: 0px !important;">
            <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#mesin">Mesin</a></li>
               <li><a data-toggle="tab" href="#dimensi">Dimensi</a></li>
               <li><a data-toggle="tab" href="#rangka">Rangka</a></li>
               <li><a data-toggle="tab" href="#kelistrikan">Kelistrikan</a></li>
            </ul>

            <div class="tab-content">
               <div id="mesin" class="tab-pane fade in active car-info">
                  <br>
                  <ul>
                     <li>
                        <strong>Panjang x Lebar x Tinggi</strong>
                        <span>@if($content_product->panjang_lebar_tinggi) {{$content_product->panjang_lebar_tinggi}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Jarak Sumbu Roda</strong>
                        <span>@if($content_product->jarak_sumbu_roda) {{$content_product->jarak_sumbu_roda}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Jarak Terendah ke Tanah</strong>
                        <span>@if($content_product->jarak_terendah_ke_tanah) {{$content_product->jarak_terendah_ke_tanah}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Curb Weight</strong>
                        <span>@if($content_product->curb_weight) {{$content_product->curb_weight}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Kapasitas Tangki Bahan Bakar</strong>
                        <span>@if($content_product->kapasitas_tangki_bbm) {{$content_product->kapasitas_tangki_bbm}} @else - @endif</span>
                     </li>
                  </ul>
               </div>
               <div id="dimensi" class="tab-pane fade car-info">
                  <br>
                  <ul>
                     <li>
                        <strong>Tipe Mesin</strong>
                        <span>@if($content_product->tipe_mesin) {{$content_product->tipe_mesin}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Volume Langkah</strong>
                        <span>@if($content_product->volume_langkah) {{$content_product->volume_langkah}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Sistem Pendingin</strong>
                        <span>@if($content_product->sistem_pendingin) {{$content_product->sistem_pendingin}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Sistem Suplai Bahan Bakar</strong>
                        <span>@if($content_product->sistem_suplai_bahan_bakar) {{$content_product->sistem_suplai_bahan_bakar}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Diameter x Langkah</strong>
                        <span>@if($content_product->diameter_x_langkah) {{$content_product->diameter_x_langkah}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Tipe Transmisi</strong>
                        <span>@if($content_product->tipe_transmisi) {{$content_product->tipe_transmisi}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Rasio Kompresi</strong>
                        <span>@if($content_product->rasio_kompresi) {{$content_product->rasio_kompresi}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Daya Maksimum</strong>
                        <span>@if($content_product->daya_maksimum) {{$content_product->daya_maksimum}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Torsi Maksimum</strong>
                        <span>@if($content_product->torsi_maksimum) {{$content_product->torsi_maksimum}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Pola Pengoperan Gigi</strong>
                        <span>@if($content_product->pola_pengoperan_gigi) {{$content_product->pola_pengoperan_gigi}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Tipe Starter</strong>
                        <span>@if($content_product->tipe_starter) {{$content_product->tipe_starter}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Tipe Kopling</strong>
                        <span>@if($content_product->tipe_kopling) {{$content_product->tipe_kopling}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Kapasitas Minyak Pelumas</strong>
                        <span>@if($content_product->kapasitas_minyak_pelumas) {{$content_product->kapasitas_minyak_pelumas}} @else - @endif</span>
                     </li>
                  </ul>
               </div>
               <div id="rangka" class="tab-pane fade car-info">
                  <br>
                  <ul>
                     <li>
                        <strong>Tipe Rangka</strong>
                        <span>@if($content_product->tipe_rangka) {{$content_product->tipe_rangka}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Tipe Suspensi Depan</strong>
                        <span>@if($content_product->tipe_suspensi_depan) {{$content_product->tipe_suspensi_depan}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Tipe Suspensi Belakang</strong>
                        <span>@if($content_product->tipe_suspensi_belakang) {{$content_product->tipe_suspensi_belakang}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Ukuran Ban Depan</strong>
                        <span>@if($content_product->ukuran_ban_depan) {{$content_product->ukuran_ban_depan}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Ukuran Ban Belakang</strong>
                        <span>@if($content_product->ukuran_ban_belakang) {{$content_product->ukuran_ban_belakang}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Tipe Rem Depan</strong>
                        <span>@if($content_product->tipe_rem_depan) {{$content_product->tipe_rem_depan}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Tipe Rem Belakang</strong>
                        <span>@if($content_product->tipe_rem_belakang) {{$content_product->tipe_rem_belakang}} @else - @endif</span>
                     </li>
                  </ul>
               </div>
               <div id="kelistrikan" class="tab-pane fade car-info">
                  <br>
                  <ul>
                     <li>
                        <strong>Tipe Baterai / Aki</strong>
                        <span>@if($content_product->tipe_aki) {{$content_product->tipe_aki}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Sistem Pengapian</strong>
                        <span>@if($content_product->sistem_pengapian) {{$content_product->sistem_pengapian}} @else - @endif</span>
                     </li>
                     <li>
                        <strong>Tipe Busi</strong>
                        <span>@if($content_product->tipe_busi) {{$content_product->tipe_busi}} @else - @endif</span>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      
      <div class="row">
         <div class="col-ninth" style="padding-left: 15px !important; padding-right: 0px !important;">
            <div class="col-md-9">
               <h3 class="single-title">
                  Komentar
               </h3>
               <div class="post-comments">
                  @if(count($comment) != 0)
                     @foreach($comment as $data_comment)
                        <div class="comment">
                           @if($data_comment->role == 'admin')
                              <img src="{{ URL::asset('/public/verified_user.png') }}" alt="">
                           @else
                              <img src="{{ URL::asset('/public/user-icon.png') }}" alt="">
                           @endif
                           
                           <h5 class="user-name">
                              {{$data_comment->comment_fullname}} <span> {{ date('d M, Y H:i', strtotime($data_comment->date_time)) }} </span>
                           </h5>
                           <p>
                              {{$data_comment->comment_description}}
                           </p>
                        </div>
                     @endforeach
                  @else
                     Tidak ada komentar.
                  @endif
               </div>
               
               <br><br>
               <h3 class="single-title">
                  Tinggalkan Komentar
               </h3>
               <div class="comments-form">
                  @if(Session::has('done'))
                     {{Session::get('done')}}<br>
                  @endif
                  <form action="{{ url('/comment-product') }}" method="POST">
                     {!! csrf_field() !!}
                     <div class="row">
                        <div class="col-half">
                           <input class="form-input" type="text" placeholder="Nama Lengkap" name="fullname" required="">
                        </div>
                        <div class="col-half">
                           <input class="form-input" type="email" placeholder="Email" name="email" required="">
                           <input type="hidden" name="product_id" value="{{$content_product->id}}">
                           <input type="hidden" name="product_slug" value="{{$content_product->slug}}">
                        </div>
                     </div>
                  <textarea name="description" class="form-input" placeholder="Komentar" required=""></textarea>
                  <input class="btn-blue" type="submit" value="Kirim Komentar">
                  </form>
               </div>
            </div>
         </div>
      </div>

   </div>
</div>
<!-- .End Section Services -->

@endsection