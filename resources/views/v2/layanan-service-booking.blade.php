@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<style type="text/css">
   .timepicker-hour {
      position: absolute !important;
      left: 11% !important;
   }
   .timepicker-minute {
      position: absolute !important;
      right: 40% !important;
   }
</style>
@endsection

@section('content')

<!-- Subscribe -->
<div class="section sec-blog" style="padding-top: 20px !important;">
   <div class="container">
      <div class="sec-header">
         <h3>
            Formulir Service Booking
         </h3>
         <p class="sub-title">
            Booking Service Di SPS Motor Dengan Mudah
         </p>
      </div>
      <div class="row contact-form">

         @if(Session::has('done'))
             <div class="alert alert-success">
            {{Session::get('done')}}
         </div>
         @endif

         @if ($errors->has('cabang_id'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Cabang belum ada yang dipilih.
         </div>
         @endif

         @if ($errors->has('fullname'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Nama Lengkap tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('email'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Email tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('no_telepon'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Nomor Telepon tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('no_polisi'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Nomor Polisi tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('jenis_kendaraan'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Jenis Kendaraan tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('jenis_service'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Jenis Service tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('tanggal_booking'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Tanggal Booking tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('jam_booking'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Jam Booking tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('description'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Keluhan tidak boleh kosong !
         </div>
         @endif
         
         <form action="{{ url('/layanan-service-booking') }}" method="POST">
            {!! csrf_field() !!}
            <div class="col-tre">
               <p>
                  <select name="cabang_id" class="form-control" id="sel1" required>
                     <option value="">Pilih Cabang</option>
                     @foreach($cabang as $data_content_cabang)
                     <option value="{{$data_content_cabang->id}}">{{$data_content_cabang->name}}</option>
                     @endforeach
                 </select>
               </p>
            </div>
            <div class="col-third">
               <p>
                  <select name="jenis_kendaraan" class="form-control" id="sel1" required>
                     <option value="">Pilih Jenis Kendaraan</option>
                     <option value="cub">CUB / Bebek</option>
                     <option value="matic">Matic</option>
                     <option value="sport">Sport</option>
                 </select>
               </p>
            </div>
            <div class="col-third">
               <p>
                  <select name="jenis_service" class="form-control" id="sel1" required>
                     <option value="">Pilih Jenis Service</option>
                     <option value="servis lengkap">Servis Lengkap (Servis & Ganti Oli)</option>
                     <option value="servis kpb">Servis KPB</option>
                     <option value="servis ganti oli">Servis Ganti Oli</option>
                     <option value="servis sparepart">Servis Sparepart</option>
                 </select>
               </p>
            </div>
            <div class="col-tre">
               <p>
                  <input type="text" placeholder="Nama Lengkap" name="fullname" id="user-name" required="required" />
               </p>
            </div>
            <div class="col-tre">
               <p>
                  <input type="text" placeholder="Email" name="email" id="user-email" required />
               </p>
            </div>
            <div class="col-quarter">
               <p>
                  <input type="text" placeholder="No. Telepon" name="no_telepon" id="user-phone" required />
               </p>
            </div>
            <div class="col-quarter">
               <p>
                  <input type="text" placeholder="No. Polisi" name="no_polisi" id="user-phone" required />
               </p>
            </div>
            
            <div class="col-third">
               <p>
                  <input placeholder="Tanggal Booking" type='text' class="form-control" id="datepicker" name="tanggal_booking" required />
               </p>
            </div>
            <div class="col-two">
               <p style="{{ $css_padding_and_margin }}">
                  <div class="input-group bootstrap-timepicker timepicker">
                     <input placeholder="Jam Booking" id="timepicker" type="text" name="jam_booking" class="form-control input-small" required>
                     <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                 </div>
               </p>
            </div>
            <div class="col-ninth" style="padding-right: 15px !important; padding-left: 15px !important;">
               <textarea name="description" placeholder="Keluhan Tentang Motor Anda" required></textarea>
            </div>
            <div class="col-quarter">
               <button type="submit" class="btn btn-default">Kirim</button>
            </div>
            
         </form>
      </div>
   </div> 
</div>
<!-- .End Subscribe -->

@endsection

@section('datepicker_js')
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
   
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script>
   // var date_input=$('input[name="date"]'); //our date input has the name "date"
   $('#datepicker').datepicker({
      format: 'dd-mm-yyyy',
      todayHighlight: true,
      autoclose: true
   });

   $('#timepicker').datetimepicker({
      format: 'HH:mm'
   });
</script>

@endsection