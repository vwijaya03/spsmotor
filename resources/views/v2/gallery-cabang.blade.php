@extends('v2/header')

@section('content')

<!-- Featured Cars -->
<div class="page-listing" @if($bgGalleryFotoCabang != null) style="background-image: url({{ URL::asset($bgGalleryFotoCabang->path) }}) !important;" @endif>
   <div class="container">
   	  <br><br>
      <div class="row">
      	@if(count($cabang) != null)
			@foreach($cabang as $data_cabang)
				<div class="col-tre">
		            <div class="feat-bloc">
		               <a href="{{ url('/gallery-cabang/'.$data_cabang->slug) }}" class="feat-img">
		                  <img width="370px" height="250px" src="{{ URL::asset($data_cabang->img_path) }}" alt="">
		               </a>
		               <div class="feat-content">
		                  <h3>
		                     <a href="{{ url('/gallery-cabang/'.$data_cabang->slug) }}">{{ $data_cabang->name }}</a>
		                  </h3>
		                  <span class="prix">
		                     {{ date('d M, Y', strtotime($data_cabang->created_at)) }}
		                  </span>
		                  <p>
		                     {{ strip_tags(substr($data_cabang->description, 0, 100)) }} 
		                     @if(strlen($data_cabang->description) >= 100)
		                     ...
		                     @else
		                     @endif
		                  </p>
		               </div>
		               <ul>
		                  <li>
		                  	 <a href="{{ $data_cabang->fb_url }}">
								<i class="ion-social-facebook"></i>
		                  	 </a>
		                  </li>
		                  <li>
		                  	<a href="{{ $data_cabang->twitter_url }}">
								<i class="ion-social-twitter"></i>
		                  	</a>
		                  </li>
		                  <li>
		                  	<a href="{{ $data_cabang->ig_url }}">
								<i class="ion-social-instagram"></i>
		                  	</a>
		                  </li>
		               </ul>
		            </div>
		        </div>
			@endforeach
		@else
			<p align="middle">Data tidak di temukan.</p>
		@endif
        
      </div>
   </div>
</div>
<!-- .End Featured Cars -->

@endsection