@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection

@section('content')

<!-- Page SubHeader -->
<div class="page-head" @if($bgHeaderBerita != null) style="background-image: url({{ URL::asset($bgHeaderBerita->path) }}) !important;" @endif>
   <div class="container">
      <h3>
         Berita
      </h3>
      <p class="breadcrumb" style="background-color: transparent !important;">
         <a href="#">Berita - berita terbaru dari SPS Motor</a>
      </p>
   </div>
</div>
<!-- .End Page SubHeader -->

<!-- Blog -->
<div class="section sec-blog" @if($bgBerita != null) style="background-image: url({{ URL::asset($bgBerita->path) }}) !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            Berita
         </h3>
         <p class="sub-title">
            Berita - Berita Terbaru Dari SPS Motor
         </p>
      </div>
      <div class="row">
         @foreach($news as $data_news)
         <div class="col-tre">
            <div class="bl-blog">
               <img width="370" height="200" src="{{ URL::asset($data_news->img_path) }}" alt="">
               <div class="bl-content">
                  <span class="date">
                     {{ date('d M, Y', strtotime($data_news->date_time)) }}
                  </span>
                  <h3>
                  {{ strip_tags(substr($data_news->title, 0, 40)) }} 
                  @if(strlen($data_news->title) >= 40)
                  ...
                  @else
                  @endif
                  </h3>
                  <a href="{{ url('/news/'.$data_news->slug) }}" class="btn-yellow">
                     Lihat Detail
                  </a>
               </div>
            </div>
         </div>
         @endforeach
      </div>
      {{ $news->links('pagination') }}
   </div> 
</div>
<!-- .End Blog -->

@endsection