@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection

@section('content')

<!-- Featured Cars -->
<div class="page-listing" @if($bgDetailFotoCabang != null) style="background-image: url({{ URL::asset($bgDetailFotoCabang->path) }}) !important;" @endif>
   <div class="container">
   	  <br><br>
      <div class="row">
      	@if(count($gallery_cabang) != 0)
			@foreach($gallery_cabang as $data_gallery_cabang)
				<div class="col-tre">
		            <div class="feat-bloc">
		               <a href="#" class="feat-img">
		                  <img width="370px" height="250px" src="{{ URL::asset($data_gallery_cabang->img_path) }}" alt="">
		               </a>
		               <div class="feat-content">
		                  <h3>
		                     <a href="#">{{ $data_gallery_cabang->title }}</a>
		                  </h3>
		                  <span class="prix">
		                     {{ date('d M, Y', strtotime($data_gallery_cabang->created_at)) }}
		                  </span>
		                  <p>
		                     {{ strip_tags($data_gallery_cabang->description) }}
		                  </p>
		               </div>
		            </div>
		        </div>
			@endforeach
		@else
			<p align="middle">Data tidak di temukan.</p>
		@endif
        
      </div>
      {{ $gallery_cabang->links('pagination') }} 
   </div>
</div>
<!-- .End Featured Cars -->

@endsection