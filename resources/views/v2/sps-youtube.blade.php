@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection

@section('content')

<!-- Section Services -->
<div class="section home-services" @if($bgSpsYoutube != null) style="background-image: url({{ URL::asset($bgSpsYoutube->path) }}) !important;" @endif>
   <div class="container">
		<div class="sec-header">
			<h3>
				SPS Youtube
			</h3>
			<p class="desc">
				Berikut beberapa video dari SPS Motor
			</p>
		</div>

		<div class="row">
			
			@if(count($sps_youtube) != 0)
				@foreach($sps_youtube as $data_sps_youtube)
				
				<div class="col-quarter">
					<div class="serv-bloc">
						
						<iframe class='img-responsive' width="100%" height="200px" src="{{$data_sps_youtube->url}}" frameborder="0" allowfullscreen></iframe>

						<div class="serv-content" style="padding: 0 30px !important;">
							<h3 style="font-size: 15px !important;">
								{{ $data_sps_youtube->title }}
							</h3>
						</div>
					</div>
				</div>
				@endforeach
			@else
				<p align="middle">Data tidak di temukan.</p>
			@endif
		</div>

		{{ $sps_youtube->links('pagination') }} 
   </div>
</div>
<!-- .End Section Services -->

@endsection