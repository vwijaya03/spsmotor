@extends('v2/header')

@section('content')

<!-- Blog -->
<div class="section sec-blog" @if($bgJaringan != null) style="background-image: url({{ URL::asset($bgJaringan->path) }}) !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            Jaringan
         </h3>
         <p class="sub-title">
            Jaringan {{$content_category_cabang->name}}
         </p>
      </div>
      <div class="row">
         @if(count($content_cabang) != null)
            @foreach($content_cabang as $data_content_cabang)
         <div class="col-tre">
            <div class="bl-blog">
               <img width="370" height="200" src="{{ URL::asset($data_content_cabang->img_path) }}" alt="">
               <div class="bl-content">
                  <span class="date">
                     {{ date('d M, Y', strtotime($data_content_cabang->created_at)) }}
                  </span>
                  <h3>
                  {{ strip_tags(substr($data_content_cabang->name, 0, 40)) }} 
                  @if(strlen($data_content_cabang->name) >= 40)
                  ...
                  @else
                  @endif
                  </h3>
                  <a href="{{ url('/cabang/'.$data_content_cabang->slug) }}" class="btn-yellow">
                     Lihat Detail
                  </a>
               </div>
            </div>
         </div>
            @endforeach
         @else
            <p align="middle">Data tidak di temukan.</p>
         @endif
      </div>
   </div> 
</div>
<!-- .End Blog -->

@endsection