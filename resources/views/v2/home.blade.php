@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection

@section('content')

<!-- .End NavBar --><!-- Home Slider -->
<div class="home-slider">
   <div id="home-slider" class="flexslider">
      <ul class="slides">
         @foreach($product as $data_product)
         <li>
            <img src="{{ URL::asset($data_product->path) }}" alt/>
            <div class="slider-content">
               <div class="slider-text">
                  <span class="taxonomy">
                     Recently added
                  </span>
                  <h2>
                     {{$data_product->name}}
                  </h2> 
                  <span class="car-type">
                     {{$data_product->category_name}}
                  </span>
                  <p>
                     {{ strip_tags($data_product->description) }}
                  </p>
                  <a href="{{$data_product->slug}}" class="btn-b-yellow">
                     Lihat Detail
                  </a>
               </div>
            </div>
         </li>
         @endforeach
      </ul>
   </div>
</div>
<!-- .End Home Slider -->

<!-- Section Services -->
<div class="section home-services" @if($bgLayananSPSMotorHome != null) style="background-image: url({{ URL::asset($bgLayananSPSMotorHome->path) }}) !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            Layanan Di SPS Motor
         </h3>
         <p class="desc">
            Berikut beberapa layanan yang di sediakan oleh SPS Motor.
         </p>
      </div>
      <div class="row">
         <div class="col-quarter">
            <div class="serv-bloc">
               <div class="serv-img">
                  <span class="icon-circle">
                     <i class="ion-wrench"></i>
                  </span>
               </div>
               <div class="serv-content">
                  <h3>
                     Service Booking <br><br>
                  </h3>
                  <a href="{{ url('/layanan-service-booking') }}" class="btn-gray">
                     Klik Di Sini
                  </a>
               </div>
            </div>
         </div>
         <div class="col-quarter">
            <div class="serv-bloc">
               <div class="serv-img">
                  <span class="icon-circle">
                     <i class="ion-gear-a"></i>
                  </span>
               </div>
               <div class="serv-content">
                  <h3>
                     Pemesanan Sparepart
                  </h3>
                  <a href="{{ url('/layanan-pesan-sparepart') }}" class="btn-gray">
                     Klik Di Sini
                  </a>
               </div>
            </div>
         </div>
         <div class="col-quarter">
            <div class="serv-bloc">
               <div class="serv-img">
                  <span class="icon-circle">
                     <i class="ion-briefcase"></i>
                  </span>
               </div>
               <div class="serv-content">
                  <h3>
                     Karir <br><br>
                  </h3>
                  <a href="{{ url('/layanan-karir') }}" class="btn-gray">
                     Klik Di Sini
                  </a>
               </div>
            </div>
         </div>
         <div class="col-quarter">
            <div class="serv-bloc">
               <div class="serv-img">
                  <span class="icon-circle">
                     <i class="ion-speakerphone"></i>
                  </span>
               </div>
               <div class="serv-content">
                  <h3>
                     Hubungi Kami <br><br>
                  </h3>
                  <a href="{{ url('/hubungi-kami') }}" class="btn-gray">
                     Klik Di Sini
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .End Section Services -->

<!-- Blog -->
<div class="section sec-blog" @if($bgBeritaTerbaruHome != null) style="background-image: url({{ URL::asset($bgBeritaTerbaruHome->path) }}) !important;" @else style="background-image: url({{ url('/') }}/public/v2/images/services-bg.jpg) !important" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            Berita Terbaru
         </h3>
         <p class="sub-title">
            Berita Terbaru dari SPS Motor
         </p>
      </div>
      <div class="row">
         @foreach($news as $data_news)
         <div class="col-tre">
            <div class="bl-blog">
               <img width="370" height="200" src="{{ URL::asset($data_news->img_path) }}" alt="">
               <div class="bl-content">
                  <span class="date">
                     {{ date('d M, Y', strtotime($data_news->created_at)) }}
                  </span>
                  <h3>
                  {{ strip_tags(substr($data_news->title, 0, 40)) }} 
                  @if(strlen($data_news->title) >= 40)
                  ...
                  @else
                  @endif
                  </h3>
                  <a href="{{ url('/news/'.$data_news->slug) }}" class="btn-yellow">
                     Lihat Detail
                  </a>
               </div>
            </div>
         </div>
         @endforeach
      </div>
   </div> 
</div>
<!-- .End Blog -->

<!-- Subscribe -->
<div class="section sec-blog" @if($bgLanggananBeritaHome != null) style="background-image: url({{ URL::asset($bgLanggananBeritaHome->path) }}) !important; padding-top: 20px !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            Langganan Berita Dengan Kami
         </h3>
         <p class="sub-title">
            Dapatkan Berita Terbaru dari SPS Motor
         </p>
      </div>
      <div class="row contact-form">
         <form action="{{ url('/subscribe') }}" method="POST">
            {!! csrf_field() !!}
            <div class="col-tre">
               <p>
                  <input type="text" name="fullname" placeholder="Nama Lengkap" required>
               </p>
            </div>
            <div class="col-tre">
               <p>
                  <input type="email" name="email" placeholder="Email" required>
               </p>
            </div>
            <div class="col-tre">
               <button type="submit" class="btn btn-default">Kirim</button>
            </div>
         </form>
      </div>
   </div> 
</div>
<!-- .End Subscribe -->

<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <button type="button" class="close" data-dismiss="modal"><h3 style="color: white;">x</h3></button>
      <img @if($bgPopupHome != null) src="{{ URL::asset($bgPopupHome->path) }}" @else src="{{ url('/public/cari_aman.jpg') }}" @endif width="100%">
   </div>
</div>

@endsection

@section('custom_script')

<script type="text/javascript">
    $(window).on('load',function(){
      setTimeout(function(){ 
         $('#myModal').modal('show');
      }, 3000);
        
    });
</script>

@endsection