@extends('v2/header')

@section('content')

<!-- Page SubHeader -->
<div class="page-head" @if($bgHeaderProduk != null) style="background-image: url({{ URL::asset($bgHeaderProduk->path) }}) !important;" @endif>
   <div class="container">
      <h3>
         Kategori {{$content_category_product->name}}
      </h3>
      <p class="breadcrumb">
         <a href="#">{{$content_category_product->name}}</a>
      </p>
   </div>
</div>
<!-- .End Page SubHeader -->

<!-- Blog -->
<div class="section sec-blog" @if($bgProduk != null) style="background-image: url({{ URL::asset($bgProduk->path) }}) !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            Kategori {{$content_category_product->name}}
         </h3>
         <p class="sub-title">
            {{$content_category_product->name}}
         </p>
      </div>
      <div class="row">
         @if(count($content_product) != null)
            @foreach($content_product as $data_content_product)
         <div class="col-tre">
            <div class="bl-blog">
               <img width="370" height="200" src="{{ URL::asset($data_content_product->thumbnail) }}" alt="">
               <div class="bl-content">
                  <span class="date">
                     {{$content_category_product->name}}
                  </span>
                  <h3>
                  {{ strip_tags(substr($data_content_product->name, 0, 80)) }} 
                  @if(strlen($data_content_product->name) >= 80)
                  ...
                  @else
                  @endif
                  </h3>
                  <a href="{{ url('/produk/'.$data_content_product->slug) }}" class="btn-yellow">
                     Lihat Detail
                  </a>
               </div>
            </div>
         </div>
            @endforeach
         @else
            <p align="middle">Data tidak di temukan.</p>
         @endif
      </div>
   </div> 
</div>
<!-- .End Blog -->

@endsection