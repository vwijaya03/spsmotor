@extends('v2/header')

@section('content')

<!-- About me container -->
<div class="section sec-about" @if($bgDetailBerita != null) style="background-image: url({{ URL::asset($bgDetailBerita->path) }}) !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            {{$news->title}}
         </h3>
      </div>
      <div class="row">
         <div class="col-half ab-side-img">
            <!-- <img width="570px" height="430px" alt="{{$news->name}}" src="{{ URL::asset($news->img_path) }}"> -->
            <img width="100%" height="100%" alt="{{$news->name}}" src="{{ URL::asset($news->img_path) }}">
         </div>
         <div class="col-half">
            <div class="ab-text">
               <?php echo htmlspecialchars_decode(htmlspecialchars_decode($news->description)); ?>
               <br>
               Di Post Pada {{ date('d M, Y', strtotime($news->date_time)) }}
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .End About me container -->

@endsection