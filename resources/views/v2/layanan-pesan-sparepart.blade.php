@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection

@section('content')

<!-- Subscribe -->
<div class="section sec-blog" style="padding-top: 20px !important;">
   <div class="container">
      <div class="sec-header">
         <h3>
            Formulir Pesan Sparepart
         </h3>
         <p class="sub-title">
            Pesan Sparepart Di SPS Motor Dengan Mudah
         </p>
      </div>
      <div class="row contact-form">

         @if(Session::has('done'))
             <div class="alert alert-success">
            {{Session::get('done')}}
         </div>
         @endif

         @if ($errors->has('cabang_id'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Cabang belum ada yang dipilih.
         </div>
         @endif

         @if ($errors->has('product_id'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Kendaraan belum ada yang dipilih !
         </div>
         @endif

         @if ($errors->has('jenis_kendaraan'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Jenis Kendaraan tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('fullname'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Nama Lengkap tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('no_telepon'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Nomor Telepon tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('tahun_rakitan'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Tahun Rakitan tidak boleh kosong !
         </div>
         @endif

         @if ($errors->has('description'))
         <div class="alert alert-danger">
            <strong>Peringatan !</strong> Keluhan tidak boleh kosong !
         </div>
         @endif
         
         <form action="{{ url('/layanan-pesan-sparepart') }}" method="POST">
            {!! csrf_field() !!}
            <div class="col-tre">
               <p>
                  <select name="cabang_id" class="form-control" id="sel1" required>
                     <option value="">Pilih Cabang</option>
                     @foreach($cabang as $data_content_cabang)
                     <option value="{{$data_content_cabang->id}}">{{$data_content_cabang->name}}</option>
                     @endforeach
                 </select>
               </p>
            </div>
            <div class="col-third">
               <p>
                  <select name="jenis_kendaraan" class="form-control" id="sel1" required>
                     <option value="">Pilih Jenis Kendaraan</option>
                     <option value="cub">CUB / Bebek</option>
                     <option value="matic">Matic</option>
                     <option value="sport">Sport</option>
                 </select>
               </p>
            </div>
            <div class="col-third">
               <p>
                  <select name="product_id" class="form-control" id="sel1" required>
                     <option value="">Pilih Kendaraan Anda</option>
                     @foreach($produk as $data_content_produk)
                     <option value="{{$data_content_produk->id}}">{{$data_content_produk->name}}</option>
                     @endforeach
                 </select>
               </p>
            </div>
            <div class="col-tre">
               <p>
                  <input type="text" placeholder="Nama Lengkap" name="fullname" id="user-name" required="required" />
               </p>
            </div>
            <div class="col-tre">
               <p>
                  <input type="text" placeholder="Email" name="email" id="user-email" required />
               </p>
            </div>
            <div class="col-quarter">
               <p>
                  <input type="text" placeholder="No. Telepon" name="no_telepon" id="user-phone" required />
               </p>
            </div>
            <div class="col-quarter">
               <p>
                  <input type="text" placeholder="Tahun Rakitan" name="tahun_rakitan" id="user-phone" required />
               </p>
            </div>
            <div class="col-ninth" style="padding-right: 15px !important; padding-left: 15px !important;">
               <textarea name="description" placeholder="Keterangan..." required></textarea>
            </div>
            <div class="col-quarter">
               <button type="submit" class="btn btn-default">Kirim</button>
            </div>
            
         </form>
      </div>
   </div> 
</div>
<!-- .End Subscribe -->

@endsection