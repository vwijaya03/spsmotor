@extends('v2/header')

@section('content')

<!-- About me container -->
<div class="section sec-about">
   <div class="container">
      <div class="sec-header">
         <h3>
            Karir
         </h3>
      </div>
      <div class="row">
         <div class="col-ninth">
            <div class="ab-text">
               <h4>
                  <span class="icon-circle">
                     <i class="ion-ios-paperplane"></i>
                  </span>
                  Karir Di SPS Motor
               </h4>
               <p>
               	@if($karir)
                  <?php echo($karir->description); ?>
                  @else
                  @endif
               </p>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .End About me container -->

@endsection