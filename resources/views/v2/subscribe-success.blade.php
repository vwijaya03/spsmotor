@extends('v2/header')

@section('content')

<!-- Blog -->
<div class="section sec-blog">
   <div class="container">
      <div class="sec-header">
         <h3>
            Langganan Berita SPS Motor Berhasil
         </h3>
      </div>
      <div class="row">
         <h4></h4>
         <p align="middle">
            @if(Session::has('done'))
               {{ Session::get('done') }}        
            @endif
         </p>
      </div>
   </div> 
</div>
<!-- .End Blog -->

@endsection