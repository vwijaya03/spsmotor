@extends('v2/header')

@section('content')

<!-- Page SubHeader -->
<div class="page-head" @if($bgHeaderSejarah != null) style="background-image: url({{ URL::asset($bgHeaderSejarah->path) }}) !important;" @endif>
   <div class="container">
      <h3>
         Tentang Kami
      </h3>
      <p class="breadcrumb">
         <a href="#">SPS Motor</a>
      </p>
   </div>
</div>
<!-- .End Page SubHeader -->

<!-- About me container -->
<div class="section sec-about" @if($bgContentSejarah != null) style="background-image: url({{ URL::asset($bgContentSejarah->path) }}) !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            SPS Motor
         </h3>
      </div>
      <div class="row">
         <div class="col-ninth">
            <div class="ab-text">
               <h4>
                  <span class="icon-circle">
                     <i class="ion-ios-paperplane"></i>
                  </span>
                  Tentang SPS Motor
               </h4>
               <p>
                  <?php echo htmlspecialchars_decode(htmlspecialchars_decode($profile->description)); ?>
               </p>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .End About me container -->

@endsection