<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>DEALER HONDA SPS MOTOR</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700|Lato:400,700,300' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('public/favicon.png') }}" />
   @yield('bootstrap_css')
   <link rel="stylesheet" href="{{ URL::asset('/public/css/bootstrap-datepicker3.css') }}"/>
	<link rel="stylesheet" href="{{ URL::asset('public/v2/css/ionicons.css') }}">
   <link rel="stylesheet" href="{{ URL::asset('public/v2/css/app.css') }}">

</head>
<body>

<!-- NavBar -->
<header class="site-header">
   <div class="container">
      <div class="row">
         <div class="col-two">
            <h1 style="{{ $css_padding_and_margin }}">
               <a href="{{ url('/') }}" class="site-logo">
                  <img src="{{ URL::asset('/public/logo.png') }}">
               </a>
            </h1>
         </div>
         <div class="col-eighth">
            <nav class="site-nav">
               <ul>
                  <li><a href="{{ url('/') }}">Beranda</a></li>
                  <li class="has-menu">
                     <a href="#">
                        Profile
                        <i class="ion-ios-arrow-down"></i>
                     </a>
                     <ul style="{{ $css_padding_and_margin }}">
                        <li>
                           <a href="{{ url('/tentang-kami') }}">
                              Sejarah
                           </a>
                        </li>
                        <li>
                           <a href="{{ url('/gallery-cabang') }}">
                              Foto Cabang
                           </a>
                        </li>
                        <li>
                           <a href="{{ url('/sps-youtube') }}">
                              SPS Youtube
                           </a>
                        </li>
                     </ul>
                  </li>
                  <li class="has-menu">
                     <a href="#">
                        Jaringan
                        <i class="ion-ios-arrow-down"></i>
                     </a>
                     <ul style="{{ $css_padding_and_margin }}">
                        @foreach($category_cabang as $data_category_cabang)
                        <li>
                           <a href="{{ url('/jaringan/'.$data_category_cabang->slug) }}">
                              {{$data_category_cabang->name}}
                           </a>
                        </li>
                        @endforeach
                     </ul>
                  </li>
                  <li class="has-menu">
                     <a href="#">
                        Produk
                        <i class="ion-ios-arrow-down"></i>
                     </a>
                     <ul style="{{ $css_padding_and_margin }}">
                        @foreach($category_product as $data_category_product)
                        <li>
                           <a href="{{ url('/kategori-produk/'.$data_category_product->slug) }}">
                              {{$data_category_product->name}}
                           </a>
                        </li>
                        @endforeach
                     </ul>
                  </li>
                  <li><a href="{{ url('/news') }}">Berita</a></li>
                  <li class="has-menu">
                     <a href="#">
                        Download
                        <i class="ion-ios-arrow-down"></i>
                     </a>
                     <ul style="{{ $css_padding_and_margin }}">
                        <li>
                           <a href="{{ url('/brosur') }}">
                              Brosur
                           </a>
                        </li>
                        <li>
                           <a href="{{ url('/katalog') }}">
                              Katalog
                           </a>
                        </li>
                     </ul>
                  </li>
                  
                  <li class="has-menu">
                     <a href="#">
                        Layanan Kami
                        <i class="ion-ios-arrow-down"></i>
                     </a>
                     <ul style="{{ $css_padding_and_margin }}">
                        <li>
                           <a href="{{ url('/layanan-service-booking') }}">
                              Service Booking
                           </a>
                        </li>
                        <li>
                           <a href="{{ url('/layanan-pesan-sparepart') }}">
                              Pesan Sparepart
                           </a>
                        </li>
                        <li>
                           <a href="{{ url('/layanan-karir') }}">
                              Karir
                           </a>
                        </li>
                        <li>
                           <a href="{{ url('/hubungi-kami') }}">
                              Hubungi Kami
                           </a>
                        </li>
                     </ul>
                  </li>
               </ul>
            </nav>
         </div>
      </div>
      <!-- Mobile Menu Btn -->
      <button class="menu-btn" id="menu-btn">
         <span class="btn-ico">
           <span></span>
           <span></span>
           <span></span>
         </span>
      </button>
      <!-- .End Btn -->
   </div>
</header>

@yield('content')

<footer class="site-footer">
   <div class="container">
      <div class="row">
         <div class="col-quarter">
            <h3>
               About Us
            </h3>
            <p>
               @if($about)
                  {{ strip_tags(substr($about->description, 0, 160)) }} 
                       @if(strlen($about->description) >= 160)
                       ...
                       @else
                       @endif
               @endif
               
            </p>
            <a href="{{ url('/tentang-kami') }}" class="btn m-btn">Baca Lebih Lanjut<span class="fa fa-angle-right"></span></a>
         </div>
         <div class="col-quarter">
            <h3>
               Site links
            </h3>
            <div class="site-links">
               <a href="{{ url('/') }}">
                  Beranda
               </a>
               <a href="{{ url('/cabang') }}">
                  Cabang
               </a>
               <a href="{{ url('/produk') }}">
                  Produk
               </a>
               <a href="{{ url('/news') }}">
                  Berita
               </a>
               <a href="{{ url('/tentang-kami') }}">
                  Tentang Kami
               </a>
               <a href="{{ url('/hubungi-kami') }}">
                  Hubungi Kami
               </a>
            </div>
         </div>
         <div class="col-quarter">
            <h3>
               Produk Terbaru
            </h3>
            <ul class="foot-insta">
               @foreach($latest_product as $data_latest_product)
               <li>
                  <a href="{{ url('/produk/'.$data_latest_product->slug) }}">
                     <img width="65px" height="65px" src="{{ url($data_latest_product->thumbnail) }}" alt="">
                     <span>
                        <i class="ion-ios-search-strong"></i>
                     </span>
                  </a>
               </li>
               @endforeach
            </ul>

         </div>
         <div class="col-quarter">
            <h3>
               Hubungi Kami
            </h3>
            <p> 
               <a href="{{ url('/hubungi-kami') }}">Klik DI Sini</a>
            </p>

         </div>
      </div>
   </div>
</footer>
<p class="copy-footer">
   &copy; <?php echo date('Y'); ?> Copyright SPS Motor. All Right Reserved.
</p>

<script src="{{ URL::asset('public/v2/js/jquery.js') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ URL::asset('public/v2/js/imagesloaded.js') }}"></script>
<script src="{{ URL::asset('public/v2/js/isotop.js') }}"></script>
<script src="{{ URL::asset('public/v2/js/owl.carousel.min.js') }}"></script>
<script src="{{ URL::asset('public/v2/js/jquery.flexslider.js') }}"></script>
<script src="{{ URL::asset('public/v2/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ URL::asset('public/v2/js/app-min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>

@yield('google_map_script')

@yield('datepicker_js')

@yield('custom_script')

</body>
</html>