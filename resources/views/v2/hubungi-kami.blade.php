@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection

@section('content')

<!-- Subscribe -->
<div class="section sec-blog" style="padding-top: 20px !important;">
   <div class="container">
      <div class="sec-header">
         <h3>
            Hubungi Kami
         </h3>
         <p class="sub-title">
            Hubungi SPS Motor Jika Ada Kritik & Saran
         </p>
      </div>
      <div class="row">
         <div id="map" style="min-height: 250px !important; width: 100%;"></div>
         <br>
      </div>
      <div class="row">
         <div class="col-half">
            <div class="row contact-form">

            @if(Session::has('done'))
                <div class="alert alert-success">
               {{Session::get('done')}}
            </div>
            @endif

            @if ($errors->has('cabang_id'))
            <div class="alert alert-danger">
               <strong>Peringatan !</strong> Cabang belum ada yang dipilih.
            </div>
            @endif

            @if ($errors->has('fullname'))
            <div class="alert alert-danger">
               <strong>Peringatan !</strong> Nama Lengkap tidak boleh kosong !
            </div>
            @endif

            @if ($errors->has('no_telepon'))
            <div class="alert alert-danger">
               <strong>Peringatan !</strong> Nomor Telepon tidak boleh kosong !
            </div>
            @endif

            @if ($errors->has('description'))
            <div class="alert alert-danger">
               <strong>Peringatan !</strong> Keluhan tidak boleh kosong !
            </div>
            @endif
            
            <form action="{{ url('/layanan-pesan-sparepart') }}" method="POST">
               {!! csrf_field() !!}
               <div class="col-eighth">
                  <p>
                     <select name="cabang_id" class="form-control" id="sel1" required>
                        <option value="">Pilih Cabang</option>
                        @foreach($cabang as $data_content_cabang)
                        <option value="{{$data_content_cabang->id}}">{{$data_content_cabang->name}}</option>
                        @endforeach
                    </select>
                  </p>
               </div>
               <div class="col-eighth">
                  <p>
                     <input type="text" placeholder="Nama Lengkap" name="fullname" id="user-name" required="required" />
                  </p>
               </div>
               <div class="col-eighth">
                  <p>
                     <input type="text" placeholder="Email" name="email" id="user-email" required />
                  </p>
               </div>
               <div class="col-eighth">
                  <p>
                     <input type="text" placeholder="No. Telepon" name="no_telepon" id="user-phone" required />
                  </p>
               </div>
               <div class="col-eighth" style="padding-right: 15px !important; padding-left: 15px !important;">
                  <textarea name="description" placeholder="Kritik dan Saran" required></textarea>
               </div>
               <div class="col-quarter">
                  <button type="submit" class="btn btn-default">Kirim</button>
               </div>
               
            </form>
         </div>
         <br><br>
         </div>
         <div class="col-half">
            <div class="ab-text">
               <h4>
                  <span class="icon-circle">
                     <i class="ion-ios-paperplane"></i>
                  </span>
                  Lokasi:
                  <br><br><br> 
                  <select name="cabang_id" id="change_cabang" class="form-control" onchange="changeCabang('{{ url('/hubungi-kami/') }}')">
                     <option value="">Pilih Cabang</option>
                     @foreach($cabang as $data_content_cabang)
                     <option value="{{$data_content_cabang->slug}}">{{$data_content_cabang->name}}</option>
                     @endforeach
                  </select>
                  Alamat: 
                  @if($default_cabang != null)
                  {{ $default_cabang->address }}
                  @else
                  @endif

                  <br><br>

                  Nomor Telepon:
                  @if($default_cabang != null)
                  {{ $default_cabang->no_telepon }}
                  @else
                  @endif

                  <br><br>

                  Email: 
                  @if($default_cabang)
                  {{ $default_cabang->email_customer_service_cabang }}
                  @else
                  @endif

                  <br><br>

                  @if($default_cabang)
                     <?php echo $default_cabang->informasi_hari_dan_jam; ?>
                  @else
                  @endif
               </h4>
               <p>
                  
               </p>
            </div>
         </div>
      </div>
   </div> 
</div>
<!-- .End Subscribe -->

@endsection

@section('google_map_script')

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDbhgNiMBQ7soSSGOMxcMOqN1rBPIOs8k&callback=initMap"></script>
<script type="text/javascript">
   function initMap() 
   {
      var myLatLng = {lat: <?php echo $default_cabang->lat; ?>, lng: <?php echo $default_cabang->lng; ?>};

      var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: myLatLng
      });

      var marker = new google.maps.Marker({
      position: myLatLng,
      map: map
      });
   }
</script>

@endsection

@section('custom_script')
   
   <script type="text/javascript">
         // bind change event to select
      function changeCabang(base_url)
      {
         var select = document.getElementById('change_cabang');
         var option_value = select.options[select.selectedIndex].value;
         console.log(option_value);
         window.location.href = base_url+'/'+option_value;
      }
   </script>
@endsection