@extends('v2/header')

@section('content')

<!-- About me container -->
<div class="section sec-about" @if($bgDetailJaringan != null) style="background-image: url({{ URL::asset($bgDetailJaringan->path) }}) !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            Cabang {{$cabang->name}}
         </h3>
         <p class="sub-title">
            SPS Motor Cabang {{$cabang->name}}
         </p>
      </div>
      <div class="row">
         <div class="col-half ab-side-img">
            <img width="100%" height="100%" src="{{ URL::asset($cabang->img_path) }}" alt="">
         </div>
         <div class="col-half">
            <div class="ab-text">
               <?php echo($cabang->description); ?>
               <br>
               <?php echo($cabang->informasi_hari_dan_jam); ?>
               <ul class="ab-social">
                  <li>
                     <a href="{{ $cabang->fb_url }}" target="_blank">
                        <i class="ion-social-facebook"></i>
                     </a>
                  </li>
                  <li>
                     <a href="{{ $cabang->ig_url }}" target="_blank">
                        <i class="ion-social-instagram"></i>
                     </a>
                  </li>
                  <li>
                     <a href="{{ $cabang->twitter_url }}" target="_blank">
                        <i class="ion-social-twitter"></i>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="col-ninth" style="padding-left: 15px !important; padding-right: 0px !important;">
            <div id="map" style="width: 100%; height: 500px;"></div>
         </div>
      </div>
   </div>
</div>
<!-- .End About me container -->

@endsection

@section('google_map_script')

   <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDbhgNiMBQ7soSSGOMxcMOqN1rBPIOs8k&callback=initMap"></script>
   <script type="text/javascript">
      function initMap() 
      {
         // var myLatLng = {lat: 0, lng: 0.01};
         var myLatLng = {lat: <?php echo($cabang->lat); ?>, lng: <?php echo($cabang->lng); ?>};

         var map = new google.maps.Map(document.getElementById('map'), {
         zoom: 19,
         center: myLatLng
         });

         var marker = new google.maps.Marker({
         position: myLatLng,
         map: map
         });
      }
   </script>

@endsection