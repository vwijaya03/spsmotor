@extends('v2/header')

@section('bootstrap_css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection

@section('content')

<!-- Blog -->
<div class="section sec-blog" @if($bgDownload != null) style="background-image: url({{ URL::asset($bgDownload->path) }}) !important;" @endif>
   <div class="container">
      <div class="sec-header">
         <h3>
            Brosur
         </h3>
         <p class="sub-title">
            Daftar Brosur Dari SPS Motor
         </p>
      </div>
      <div class="row contact-form">
         <form action="{{ url('/cari-brosur') }}" method="GET">
            <div class="col-tre">
               <p>
                  <input type="text" name="search_brosur" placeholder="Cari Brosur">
               </p>
            </div>
            <div class="col-tre">
               <button type="submit" class="btn btn-default">Cari Brosur</button>
            </div>
         </form>
      </div>
      <div class="row">
         @if(count($brosur) != 0)
            @foreach($brosur as $data_brosur)
               <div class="col-quarter">
                  <div class="bl-blog">
                     <img width="370" height="200" src="{{ URL::asset('/public/file-icon.png') }}">
                     <div class="bl-content">
                        <span class="date">
                           {{ date('d M, Y', strtotime($data_brosur->created_at)) }}
                        </span>
                        <h3>
                        {{ $data_brosur->title }}
                        </h3>
                        <a href="{{ url($data_brosur->path) }}" target="_blank" class="btn-yellow">
                           Download
                        </a>
                     </div>
                  </div>
               </div>
            @endforeach
         @else
            <p align="middle">Data tidak di temukan.</p>
         @endif
      </div>
      {{ $brosur->links('pagination') }}
   </div> 
</div>
<!-- .End Blog -->

@endsection