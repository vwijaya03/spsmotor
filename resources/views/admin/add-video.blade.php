@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url('/') }}">Dashboard</a></li>
  <li><a href="{{ url('jenis-truck') }}">Add Video</a></li>
  <li class="active">Buat Baru</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Add Video    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            <form method="post" action="{{ url('/auth/admin/add-video') }}" enctype="multipart/form-data" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}

                <div class="form-group filled">
                  <label class="control-label">Category *</label>
                  <select class="form-control" name="category">
                    <option value="">Pilih Category</option>
                    @foreach($categories as $index => $data)
                        <option value="{{$data->id}}">{{$data->name}}</option>
                    @endforeach
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('category'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Category belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Title *</label>
                  <input type="text" class="form-control" name="title">
                  <div class="help-block with-errors">
                      @if ($errors->has('title'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Title tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Description *</label>
                  <textarea class="form-control" name="description"></textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Description tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Upload At *</label>
                  <input type="text" class="form-control datepicker" data-error="This field is required" name="upload_at">
                  <div class="help-block with-errors">
                      @if ($errors->has('upload_at'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Upload At tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <input type="file" class="form-control" data-error="This field is required" name="image">
                  <div class="help-block with-errors">
                      @if ($errors->has('image'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Upload image idak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="input_fields_wrap">
                    <div class="form-group col-md-5">
                      <label>Button Name</label>
                      <input type="text" class="form-control" required placeholder="Button Name" name="btn_name[]">
                      <div class="help-block with-errors">
                          @if(Session::has('btn_name'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Button Name tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="form-group col-md-5">
                      <label>iFrame Url</label>
                      <input type="text" class="form-control" required placeholder="iFrame Url" name="iframe_url[]">
                      <div class="help-block with-errors">
                          @if(Session::has('iframe_url'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>iFrame Url tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                      <button type="button" class="add_field_button btn btn_primary left">Add More Fields</button>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('addedScript')

<script charset="utf-8" src="{{ URL::asset('assets/js/priceformat.js') }}"></script>
<script>
  $('#gaji').priceFormat({
    prefix: 'IDR ',
    thousandsSeparator: '.',
    centsLimit: 0
  });
</script>

@endsection