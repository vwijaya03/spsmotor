@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/cabang') }}">Data Cabang</a></li>
  <li class="active">Ubah Data Cabang</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Ubah Data Cabang    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/edit-cabang/'.$data->slug) }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}
                
                <div class="form-group filled">
                  <label class="control-label">Category Cabang</label>
                  <select class="form-control" name="category_cabang_id">
                    <option value="">Pilih Category Cabang</option>
                    <option selected value="{{$selected_category_cabang->id}}">{{$selected_category_cabang->name}}</option>
                    @foreach($diff_category_cabang as $data_diff)
                        <option value="{{$data_diff->id}}">{{$data_diff->name}}</option>
                    @endforeach
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('category_cabang_id'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Category Cabang belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label">Email Bengkel Cabang *</label>
                  <input type="text" class="form-control" name="email_bengkel_cabang" value="{{$data->email_bengkel_cabang}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('email_bengkel_cabang'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Email Bengkel Cabang tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Email PIC Cabang *</label>
                  <input type="text" class="form-control" name="email_pic_cabang" value="{{$data->email_pic_cabang}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('email_pic_cabang'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Email PIC Cabang tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Email Technical Service Department Cabang *</label>
                  <input type="text" class="form-control" name="email_technical_service_department_cabang" value="{{$data->email_technical_service_department_cabang}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('email_technical_service_department_cabang'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Email Technical Service Department Cabang tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Email Customer Service Department Cabang *</label>
                  <input type="text" class="form-control" name="email_customer_service_cabang" value="{{$data->email_customer_service_cabang}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('email_customer_service_cabang'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Email Customer Service Department Cabang tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Alamat Facebook Cabang * (alamat url harus ada http:// Contoh http://facebook.com tidak boleh facebook.com saja)</label>
                  <input type="text" class="form-control" name="fb_url" value="{{$data->fb_url}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('fb_url'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Alamat Facebook Cabang tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Alamat Twitter Cabang * (alamat url harus ada http:// Contoh http://twitter.com tidak boleh twitter.com saja)</label>
                  <input type="text" class="form-control" name="twitter_url" value="{{$data->twitter_url}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('twitter_url'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Alamat Twitter Cabang tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Alamat Instagram Cabang * (alamat url harus ada http:// Contoh http://instagram.com tidak boleh instagram.com saja)</label>
                  <input type="text" class="form-control" name="ig_url" value="{{$data->ig_url}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('ig_url'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Alamat Instagram Cabang tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Nama Cabang</label>
                  <input type="text" class="form-control" name="name" value="{{$data->name}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Cabang tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Konten</label>
                  <textarea name="description" id="description_editor">{{$data->description}}</textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Konten tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <label>Gambar</label><br>
                  @if($data->img_path != null)
                  <label>Untuk melihat gambar saat ini klik <a href="{{ url($data->img_path) }}" target="_blank">disini</a>, untuk mengganti gambar yang baru, tekan choose file, pilih gambar nya lalu tekan simpan.</label>
                  @else
                  <label>gambar tidak ada.</label>
                  @endif
                  <input type="file" class="form-control" name="img">
                  <div class="help-block with-errors">
                      @if ($errors->has('img'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Gambar tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Alamat *</label>
                  <input type="text" class="form-control" name="address" value="{{$data->address}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('address'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Alamat tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-8">
                      <label>No. Telepon *</label>
                      <input type="text" class="form-control" name="no_telepon" value="{{$data->no_telepon}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('no_telepon'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>No Telepon tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label>Informasi Hari Buka dan Jam Buka *</label>
                  <textarea name="informasi_hari_dan_jam" id="informasi_hari_dan_jam_buka">{{$data->informasi_hari_dan_jam}}</textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('informasi_hari_dan_jam'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Informasi Hari Buka dan Jam Buka tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Latitude</label>
                  <input type="text" class="form-control" name="lat" value="{{$data->lat}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('lat'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Latitude tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Longitude</label>
                  <input type="text" class="form-control" name="lng" value="{{$data->lng}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('lng'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Longitude tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );

        CKEDITOR.replace( 'informasi_hari_dan_jam_buka', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection