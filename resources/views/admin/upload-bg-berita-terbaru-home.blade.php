@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
</ul>

@endsection

@section('content')

@if($add_button == true)
<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i> Upload Background Berita Terbaru Beranda</h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/add-upload-bg-berita-terbaru-home') }}" class="form-floating">
              <fieldset>
                <legend>Di Sarankan Ukuran Gambar 1280px X 803px</legend>

                {!! csrf_field() !!}

                <div class="form-group">
                  <label>Gambar *</label>
                  <input type="file" class="form-control" name="file">
                  <div class="help-block with-errors">
                      @if ($errors->has('file'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Gambar tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endif

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" @if($add_button == 'true') style="padding-top: 0 !important;" @endif>
  <section class="tables-data">
    <div class="card">
      <div>
        <div class="datatables">
          <div class="page-header">
            <h1>@if($add_button == false) <i class="md md-work"></i> Upload Background Berita Terbaru Beranda @endif</h1>
          </div>
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>File</th>
                <th>Tanggal Upload</th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($file as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td><a href="{{ url($data->path) }}" target="_blank">Lihat Gambar</a></td>
                <td>{{ date('d M Y H:i:s', strtotime($data->created_at)) }}</td>
                <td><a onclick="deleteBackgroundImage('{{$data->id}}', '{{$delete_url}}', '{{$redirect_url}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection