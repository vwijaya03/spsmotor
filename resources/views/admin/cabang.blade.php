@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Cabang</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Cabang    </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-cabang') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Category</th>
                <th>Nama</th>
                <th>Gambar</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($cabang as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->category_name}}</td>
                <td>{{$data->cabang_name}}</td>
                <td><a href="{{ url($data->cabang_image) }}" target="_blank">Gambar</a></td>
                <td><a href="{{ url($url_admin_prefix.'/edit-cabang/'.$data->cabang_slug) }}">Edit</a></td>
                <td><a onclick="deleteCabang('{{$data->cabang_slug}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection