@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/user') }}">Data User</a></li>
  <li class="active">Buat Baru</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Tambah User    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/add-user') }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}
                
                <div class="form-group">
                  <label class="control-label">Nama Lengkap *</label>
                  <input type="text" class="form-control" name="fullname" value="{{ old('fullname') }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('fullname'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Lengkap tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Username *</label>
                  <input type="text" class="form-control" name="username" value="{{ old('username') }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('username'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Username tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Email *</label>
                  <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('email'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Email tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Password *</label>
                  <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('password'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Password tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">No. Telepon *</label>
                  <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('phone'))
                          <span class="help-block">
                              <p class="text-danger"><strong>No. Telepon tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );

        CKEDITOR.replace( 'informasi_hari_dan_jam_buka', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection