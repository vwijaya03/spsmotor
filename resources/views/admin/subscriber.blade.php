@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Pelanggan Berita</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Pelanggan Berita    </h1>
      <br>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nama Lengkap</th>
                <th>Email</th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($subscriber as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->fullname}}</td>
                <td>{{$data->email}}</td>
                <td><a onclick="deleteSubscriber('{{$data->id}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection