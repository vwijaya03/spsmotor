@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Gallery Product {{$header->name}}</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Gallery Product {{$header->name}}   </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-gallery-product/'.$product_id) }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nama Product</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($gallery_product as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->product_name}}</td>
                <td>
                    @if($data->set_image == 1)
                        Di Aturkan Sebagai Slideshow / Foto Produk
                    @else
                        -
                    @endif
                </td>
                <td><a href="{{ url($data->path) }}" target="_blank">Lihat Gambar</a></td>
                <td>
                    <a onclick="setImageAsDefaultSlideshow('{{ url('/admin/backend/') }}', '{{$product_id}}', '{{$data->id}}')">Atur Sebagai Gambar Slideshow</a> / <a onclick="removeImageAsDefaultSlideshow('{{ url('/admin/backend/') }}', '{{$product_id}}', '{{$data->id}}')">Batalkan </a>
                </td>
                <td><a href="{{ url($url_admin_prefix.'/product/'.$product_id.'/gallery/'.$data->id) }}">Edit</a></td>
                <td><a onclick="deleteGalleryProduct('{{ url('/admin/backend/') }}', '{{$product_id}}', '{{$data->id}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection