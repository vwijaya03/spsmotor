@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/product') }}">Data Product</a></li>
  <li class="active">Ubah Data Product</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Ubah Data Product    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/edit-product/'.$data->slug) }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}
                
                <div class="form-group filled">
                  <label class="control-label">Category Product</label>
                  <select class="form-control" name="category_id">
                    <option value="">Pilih Category Product</option>
                    <option selected value="{{$selected_category_product->id}}">{{$selected_category_product->name}}</option>
                    @foreach($diff_category_product as $data_diff)
                        <option value="{{$data_diff->id}}">{{$data_diff->name}}</option>
                    @endforeach
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('category_id'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Category Product belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Nama Product</label>
                  <input type="text" class="form-control" name="name" value="{{$data->name}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Product tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Thumbnail Product</label><br>
                  @if($data->thumbnail != null)
                  <label>Untuk melihat thumbnail saat ini klik <a href="{{ url($data->thumbnail) }}" target="_blank">disini</a>, untuk mengganti gambar yang baru, tekan choose file, pilih thumbnail nya lalu tekan simpan.</label>
                  @else
                  <label>thumbnail tidak ada.</label>
                  @endif
                  <input type="file" class="form-control" name="thumbnail">
                  <div class="help-block with-errors">
                      @if ($errors->has('thumbnail'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Thumbnail Product tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Konten</label>
                  <textarea name="description" id="description_editor">{{$data->description}}</textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Konten tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Harga OTR</label>
                  <input type="text" class="form-control" name="price" value="{{$data->price}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('price'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Product tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-3">
                      <label>Tipe Mesin *</label>
                      <input type="text" class="form-control" name="tipe_mesin" value="{{ $data->tipe_mesin }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_mesin'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Mesin tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Volume Langkah *</label>
                      <input type="text" class="form-control" name="volume_langkah" value="{{ $data->volume_langkah }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('volume_langkah'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Volume Langkah tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Sistem Pendingin *</label>
                      <input type="text" class="form-control" name="sistem_pendingin" value="{{ $data->sistem_pendingin }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('sistem_pendingin'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Sistem Pendingin tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Sistem Suplai Bahan Bakar *</label>
                      <input type="text" class="form-control" name="sistem_suplai_bahan_bakar" value="{{ $data->sistem_suplai_bahan_bakar }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('sistem_suplai_bahan_bakar'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Sistem Suplai Bahan Bakar tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Diameter x Langkah *</label>
                      <input type="text" class="form-control" name="diameter_x_langkah" value="{{ $data->diameter_x_langkah }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('diameter_x_langkah'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Diameter x Langkah tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Transmisi *</label>
                      <input type="text" class="form-control" name="tipe_transmisi" value="{{ $data->tipe_transmisi }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_transmisi'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Transmisi tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Rasio Kompresi *</label>
                      <input type="text" class="form-control" name="rasio_kompresi" value="{{ $data->rasio_kompresi }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('rasio_kompresi'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Rasio Kompresi tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Daya Maksimum *</label>
                      <input type="text" class="form-control" name="daya_maksimum" value="{{ $data->daya_maksimum }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('daya_maksimum'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Daya Maksimum tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Torsi Maksimum *</label>
                      <input type="text" class="form-control" name="torsi_maksimum" value="{{ $data->torsi_maksimum }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('torsi_maksimum'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Torsi Maksimum tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Pola Pengoperan Gigi *</label>
                      <input type="text" class="form-control" name="pola_pengoperan_gigi" value="{{ $data->pola_pengoperan_gigi }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('pola_pengoperan_gigi'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Pola Pengoperan Gigi tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Starter *</label>
                      <input type="text" class="form-control" name="tipe_starter" value="{{ $data->tipe_starter }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_starter'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Starter tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Kopling *</label>
                      <input type="text" class="form-control" name="tipe_kopling" value="{{ $data->tipe_kopling }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_kopling'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Kopling tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Kapasitas Minyak Pelumas *</label>
                      <input type="text" class="form-control" name="kapasitas_minyak_pelumas" value="{{ $data->kapasitas_minyak_pelumas }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('kapasitas_minyak_pelumas'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Kapasitas Minyak Pelumas tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Panjang x Lebar x Tinggi *</label>
                      <input type="text" class="form-control" name="panjang_lebar_tinggi" value="{{ $data->panjang_lebar_tinggi }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('panjang_lebar_tinggi'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Panjang x Lebar x Tinggi tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Jarak Sumbu Roda *</label>
                      <input type="text" class="form-control" name="jarak_sumbu_roda" value="{{ $data->jarak_sumbu_roda }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('jarak_sumbu_roda'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Jarak Sumbu Roda tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Jarak Terendah ke Tanah *</label>
                      <input type="text" class="form-control" name="jarak_terendah_ke_tanah" value="{{ $data->jarak_terendah_ke_tanah }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('jarak_terendah_ke_tanah'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Jarak Terendah ke Tanah tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Curb Weight *</label>
                      <input type="text" class="form-control" name="curb_weight" value="{{ $data->curb_weight }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('curb_weight'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Curb Weight tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Kapasitas Tangki Bahan Bakar *</label>
                      <input type="text" class="form-control" name="kapasitas_tangki_bbm" value="{{ $data->kapasitas_tangki_bbm }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('kapasitas_tangki_bbm'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Kapasitas Tangki Bahan Bakar tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Rangka *</label>
                      <input type="text" class="form-control" name="tipe_rangka" value="{{ $data->tipe_rangka }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_rangka'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Rangka tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Suspensi Depan *</label>
                      <input type="text" class="form-control" name="tipe_suspensi_depan" value="{{ $data->tipe_suspensi_depan }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_suspensi_depan'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Suspensi Depan tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Suspensi Belakang *</label>
                      <input type="text" class="form-control" name="tipe_suspensi_belakang" value="{{ $data->tipe_suspensi_belakang }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_suspensi_belakang'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Suspensi Belakang tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Ukuran Ban Depan *</label>
                      <input type="text" class="form-control" name="ukuran_ban_depan" value="{{ $data->ukuran_ban_depan }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('ukuran_ban_depan'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Ukuran Ban Depan tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Ukuran Ban Belakang *</label>
                      <input type="text" class="form-control" name="ukuran_ban_belakang" value="{{ $data->ukuran_ban_belakang }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('ukuran_ban_belakang'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Ukuran Ban Belakang tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Rem Depan *</label>
                      <input type="text" class="form-control" name="tipe_rem_depan" value="{{ $data->tipe_rem_depan }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_rem_depan'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Rem Depan tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Rem Belakang *</label>
                      <input type="text" class="form-control" name="tipe_rem_belakang" value="{{ $data->tipe_rem_belakang }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_rem_belakang'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Rem Belakang tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Baterai / Aki *</label>
                      <input type="text" class="form-control" name="tipe_aki" value="{{ $data->tipe_aki }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_aki'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Baterai / Aki tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Sistem Pengapian *</label>
                      <input type="text" class="form-control" name="sistem_pengapian" value="{{ $data->sistem_pengapian }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('sistem_pengapian'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Sistem Pengapian tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-3">
                      <label>Tipe Busi *</label>
                      <input type="text" class="form-control" name="tipe_busi" value="{{ $data->tipe_busi }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tipe_busi'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tipe Busi tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection