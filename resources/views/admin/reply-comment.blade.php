@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/comment') }}">Data Komentar</a></li>
  <li class="active">Balas Komentar</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
<!--       <h1>      <i class="md md-input"></i>      Tambah Product    </h1>
 -->    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/reply-comment/'.$comment_id) }}" class="form-floating">
              <fieldset>
                <legend>Balas Komentar</legend>

                {!! csrf_field() !!}
                
                <div class="form-group">
                  <textarea name="comment" class="form-control" style="min-height: 80px !important;"></textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('comment'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Komentar tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
              
                  </div>
                </div>

                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection