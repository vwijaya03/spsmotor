@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Service Booking</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Service Booking    </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-service-booking') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th width="8%">No.</th>
                <th width="10%">Cabang</th>
                <th width="15%">Data Customer</th>
                <th width="10%">No. Polisi</th>
                <th width="10%">Jenis Kendaraan</th>
                <th width="10%">Jenis Service</th>
                <th width="12%">Tanggal & Jam</th>

                <th width="20%">Keluhan</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($service_booking as $data)
              <tr>
                <td>{{ $data->id }}</td>
                <td>{{ $data->cabang_name }}</td>
                <td>
                    {{ $data->fullname }} <br>
                    {{ $data->email }} <br>
                    {{ $data->no_telepon }}
                </td>
                <td>{{ $data->no_polisi }}</td>
                <td>
                    {{ $data->jenis_kendaraan }}
                </td>
                <td>{{ $data->jenis_service }}</td>
                <td>{{ date('d M, Y', strtotime($data->tanggal_booking)) }} / Jam {{$data->jam_booking}}</td>
                <td>{{ $data->description }}</td>
                <td><a href="{{ url($url_admin_prefix.'/edit-service-booking/'.$data->id) }}">Edit</a></td>
                <td><a onclick="deleteServiceBooking('{{ $data->id }}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection