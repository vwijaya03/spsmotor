@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Product</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Product    </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-product') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Category</th>
                <th>Nama Product</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($product as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->category_product_name}}</td>
                <td>{{$data->name}}</td>
                <td><a href="{{ url($url_admin_prefix.'/product/'.$data->id.'/gallery') }}" target="_blank">Lihat Gallery Gambar Product Ini</a></td>
                <td><a href="{{ url($url_admin_prefix.'/edit-product/'.$data->slug) }}">Edit</a></td>
                <td><a onclick="deleteProduct('{{$data->slug}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection