@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data User</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data User    </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-user') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nama Lengkap</th>
                <th>Username</th>
                <th>E-mail</th>
                <th>No. Telepon</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($data_users as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->fullname}}</td>
                <td>{{$data->username}}</td>
                <td>{{$data->email}}</td>
                <td>{{$data->phone}}</td>
                <td><a href="{{ url($url_admin_prefix.'/edit-user/'.$data->id) }}">Ubah</a></td>
                <td><a onclick="deleteUser('{{$data->id}}')">Hapus</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection