@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Sejarah Perusahaan</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Sejarah Perusahaan    </h1>
      <br>
      @if($add_button == 'true')
        <button onclick="location.href='{{ url($url_admin_prefix.'/add-sejarah') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
      @endif
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Description</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($sejarah as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>
                    {{ strip_tags(substr($data->description, 0, 160)) }} 
                    @if(strlen($data->description) >= 160)
                    ...
                    @else
                    @endif
                </td>
                <td><a href="{{ url($url_admin_prefix.'/edit-sejarah/'.$data->id) }}">Lihat Detail</a></td>
                <td><a onclick="deleteSejarah('{{$data->id}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection