@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/semua-category-product') }}">Data Kategori Product</a></li>
  <li class="active">Buat Baru</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Tambah Kategori Product    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            <form method="post" action="{{ url($url_admin_prefix.'/add-category-product') }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}

                <div class="form-group">
                  <label class="control-label">Nama Product *</label>
                  <input type="text" class="form-control" name="name">
                  <div class="help-block with-errors">
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Product tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection