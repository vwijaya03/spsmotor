@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url('/') }}">Dashboard</a></li>
  <li class="active">Data Video</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Video    </h1>
      <br>
      <button onclick="location.href='{{ url('/auth/admin/add-video') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($videos as $index => $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->title}}</td>
                <td>{{$data->description}}</td>
                <td><a href="{{ url('/auth/admin/edit-video/'.$data->id) }}">Edit</a></td>
                <td><a onclick="deleteVideo({{$data->id}})">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection