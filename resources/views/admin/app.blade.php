<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Tera</title>
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="{{ URL::asset('assets/img/favicon/mstile-144x144.png') }}">
    <meta name="msapplication-config" content="{{ URL::asset('assets/img/favicon/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/android-chrome-192x192.png') }}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="{{ URL::asset('assets/img/favicon/manifest.json') }}">
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon/favicon.ico') }}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
    <link href="{{ URL::asset('assets/css/vendors.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/css/styles.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/css/sweet-alert.css') }}" rel="stylesheet" />
    <!-- <script charset="utf-8" src="//maps.google.com/maps/api/js?sensor=true"></script> -->
  </head>
  <body scroll-spy="" id="top" class=" theme-template-dark theme-pink alert-open alert-with-mat-grow-top-right">  
    <main>
      <aside class="sidebar fixed" style="width: 260px; left: 0px; ">
        <div class="brand-logo">
          <div id="logo">
            <div class="foot1"></div>
            <div class="foot2"></div>
            <div class="foot3"></div>
            <div class="foot4"></div>
          </div> TERA </div>
        <div class="user-logged-in">
          <div class="content">
            <div class="user-name">{{$users->fullname}} <span class="text-muted f9">Admin</span></div>
            <div class="user-email">{{$users->email}}</div>
            <div class="user-actions"> <a href="{{ url($url_admin_prefix.'/logout') }}">logout</a> </div>
          </div>
        </div>
        <ul class="menu-links">
          <li> <a href="#" data-toggle="collapse" data-target="#UICabang" aria-expanded="false" aria-controls="UICabang" class="collapsible-header waves-effect"><i class="md md-store"></i>&nbsp;Cabang</a>
            <ul id="UICabang" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/semua-category-cabang') }}"><span>Data Kategori Cabang</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/cabang') }}"><span>Data Cabang</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/gallery-cabang') }}"><span>Data Gallery Cabang</span></a></li>
            </ul>
          </li>

          <li> <a href="#" data-toggle="collapse" data-target="#UIProduct" aria-expanded="false" aria-controls="UIProduct" class="collapsible-header waves-effect"><i class="md md-assignment"></i>&nbsp;Product</a>
            <ul id="UIProduct" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/semua-category-product') }}"><span>Data Kategori Product</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/product') }}"><span>Data Product</span></a></li>
            </ul>
          </li>

          <li> <a href="#" data-toggle="collapse" data-target="#UIProfile" aria-expanded="false" aria-controls="UIProfile" class="collapsible-header waves-effect"><i class="md md-account-box"></i>&nbsp;Data Profile</a>
            <ul id="UIProfile" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/sejarah') }}"><span>Sejarah SPS</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/sps-youtube') }}"><span>SPS Youtube</span></a></li>
            </ul>
          </li>
          
          <li icon="md md-content-paste"> <a href="{{ url($url_admin_prefix.'/news') }}"><i class="md md-content-paste"></i>&nbsp;<span><b>Data Berita</b></span></a></li>

          <li icon="md md-assignment"> <a href="{{ url($url_admin_prefix.'/service-booking') }}"><i class="md md-assignment"></i>&nbsp;<span><b>Data Service Booking</b></span></a></li>
          
          <li icon="md md-work"> <a href="{{ url($url_admin_prefix.'/karir') }}"><i class="md md-work"></i>&nbsp;<span><b>Data Karir</b></span></a></li>

          <li icon="md md-attachment"> <a href="{{ url($url_admin_prefix.'/download-file') }}"><i class="md md-attachment"></i>&nbsp;<span><b>Data Download File</b></span></a></li>

          <li> <a href="#" data-toggle="collapse" data-target="#UIUploadBackgroundBeranda" aria-expanded="false" aria-controls="UIUploadBackgroundBeranda" class="collapsible-header waves-effect"><i class="md md-add-to-photos"></i>&nbsp;Upload Background Menu Beranda</a>
            <ul id="UIUploadBackgroundBeranda" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-layanan-spsmotor-home') }}"><span>Bagian Layanan SPS Motor</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-berita-terbaru-home') }}"><span>Bagian Berita Terbaru</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-langganan-berita-home') }}"><span>Bagian Langganan Berita</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-popup-home') }}"><span>Bagian Pop Up</span></a></li>
            </ul>
          </li>

          <li> <a href="#" data-toggle="collapse" data-target="#UIUploadBackgroundSejarah" aria-expanded="false" aria-controls="UIUploadBackgroundSejarah" class="collapsible-header waves-effect"><i class="md md-add-to-photos"></i>&nbsp;Upload Background Menu Sejarah</a>
            <ul id="UIUploadBackgroundSejarah" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-header-sejarah') }}"><span>Bagian Header Sejarah</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-content-sejarah') }}"><span>Bagian Content Sejarah</span></a></li>
            </ul>
          </li>

          <li> <a href="#" data-toggle="collapse" data-target="#UIUploadBackgroundFotoCabang" aria-expanded="false" aria-controls="UIUploadBackgroundFotoCabang" class="collapsible-header waves-effect"><i class="md md-add-to-photos"></i>&nbsp;Upload Background Menu Foto Cabang</a>
            <ul id="UIUploadBackgroundFotoCabang" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-gallery-foto-cabang') }}"><span>Bagian Gallery Foto Cabang</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-detail-foto-cabang') }}"><span>Bagian Detail Foto Cabang</span></a></li>
            </ul>
          </li>

          <li icon="md md-add-to-photos"> <a href="{{ url($url_admin_prefix.'/upload-bg-sps-youtube') }}"><i class="md md-add-to-photos"></i>&nbsp;<span><b>Upload Background Menu SPS Youtube</b></span></a></li>

          <li> <a href="#" data-toggle="collapse" data-target="#UIUploadBackgroundJaringan" aria-expanded="false" aria-controls="UIUploadBackgroundJaringan" class="collapsible-header waves-effect"><i class="md md-add-to-photos"></i>&nbsp;Upload Background Menu Jaringan</a>
            <ul id="UIUploadBackgroundJaringan" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-jaringan') }}"><span>Bagian Jaringan</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-detail-jaringan') }}"><span>Bagian Detail Jaringan</span></a></li>
            </ul>
          </li>

          <li> <a href="#" data-toggle="collapse" data-target="#UIUploadBackgroundProduk" aria-expanded="false" aria-controls="UIUploadBackgroundProduk" class="collapsible-header waves-effect"><i class="md md-add-to-photos"></i>&nbsp;Upload Background Menu Produk</a>
            <ul id="UIUploadBackgroundProduk" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-header-produk') }}"><span>Bagian Header Produk</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-produk') }}"><span>Bagian Produk</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-detail-produk') }}"><span>Bagian Detail Produk</span></a></li>
            </ul>
          </li>

          <li> <a href="#" data-toggle="collapse" data-target="#UIUploadBackgroundBerita" aria-expanded="false" aria-controls="UIUploadBackgroundBerita" class="collapsible-header waves-effect"><i class="md md-add-to-photos"></i>&nbsp;Upload Background Menu Berita</a>
            <ul id="UIUploadBackgroundBerita" class="collapse">
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-header-berita') }}"><span>Bagian Header Berita</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-berita') }}"><span>Bagian Berita</span></a></li>
              <li> <a href="{{ url($url_admin_prefix.'/upload-bg-detail-berita') }}"><span>Bagian Detail Berita</span></a></li>
            </ul>
          </li>
          
          <li icon="md md-add-to-photos"> <a href="{{ url($url_admin_prefix.'/upload-bg-download') }}"><i class="md md-add-to-photos"></i>&nbsp;<span><b>Upload Background Menu Download</b></span></a></li>

          <li icon="md md-comment"> <a href="{{ url($url_admin_prefix.'/comment') }}"><i class="md md-comment"></i>&nbsp;<span><b>Data Komentar</b></span></a></li>

          <li icon="md md-add-shopping-cart"> <a href="{{ url($url_admin_prefix.'/pesan-sparepart') }}"><i class="md md-add-shopping-cart"></i>&nbsp;<span><b>Data Pemesanan Sparepart</b></span></a></li>

          <li icon="md md-wallet-membership"> <a href="{{ url($url_admin_prefix.'/subscriber') }}"><i class="md md-wallet-membership"></i>&nbsp;<span><b>Data Pelanggan Berita</b></span></a></li>

          <li icon="md md-speaker-notes"> <a href="{{ url($url_admin_prefix.'/kritik-dan-saran') }}"><i class="md md-speaker-notes"></i>&nbsp;<span><b>Data Kritik dan Saran</b></span></a></li>

          <li icon="md md-settings"> <a href="{{ url($url_admin_prefix.'/user') }}"><i class="md md-settings"></i>&nbsp;<span><b>Data User</b></span></a></li>
        </ul>
      </aside>
      <div class="main-container">        
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header pull-left">
              <button type="button" class="navbar-toggle pull-left m-15" data-activates=".sidebar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              
              @yield('breadcrumb')

            </div>
          </div>
        </nav>
        
        @yield('content')
        
      </div>
    </main>
    <style>
    .glyphicon-spin-jcs {
      -webkit-animation: spin 1000ms infinite linear;
      animation: spin 1000ms infinite linear;
    }
    
    @-webkit-keyframes spin {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
      }
    }
    
    @keyframes spin {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
      }
    }
    </style>

    <script charset="utf-8" src=" {{ URL::asset('/assets/js/vendors.min.js') }} "></script>
    <script charset="utf-8" src=" {{ URL::asset('/assets/js/app.min.js') }} "></script>
    <script charset="utf-8" src=" {{ URL::asset('/assets/js/custom.js') }} "></script>
    <script charset="utf-8" src=" {{ URL::asset('/assets/js/forms.js') }} "></script>
    <script charset="utf-8" src=" {{ URL::asset('/assets/js/sweet-alert.min.js') }} "></script>
    
    @yield('addedScript')
    @yield('ckeditorScript')

  </body>
</html>