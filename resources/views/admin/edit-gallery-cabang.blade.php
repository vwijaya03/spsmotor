@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/gallery-cabang') }}">Data Gallery Cabang</a></li>
  <li class="active">Ubah Data Gallery Cabang</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Ubah Data Cabang    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/edit-gallery-cabang/'.$data->id) }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}
                
                <div class="form-group filled">
                  <label class="control-label">Cabang</label>
                  <select class="form-control" name="cabang_id">
                    <option value="">Pilih Cabang</option>
                    <option selected value="{{$selected_cabang->id}}">{{$selected_cabang->name}}</option>
                    @foreach($diff_cabang as $data_diff)
                        <option value="{{$data_diff->id}}">{{$data_diff->name}}</option>
                    @endforeach
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('cabang_id'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Cabang belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input type="text" class="form-control" name="title" value="{{$data->title}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('title'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Title tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Konten</label>
                  <textarea name="description" id="description_editor">{{$data->description}}</textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Konten tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <label>Gambar</label><br>
                  @if($data->img_path != null)
                  <label>Untuk melihat gambar saat ini klik <a href="{{ url($data->img_path) }}" target="_blank">disini</a>, untuk mengganti gambar yang baru, tekan choose file, pilih gambar nya lalu tekan simpan.</label>
                  @else
                  <label>gambar tidak ada.</label>
                  @endif
                  <input type="file" class="form-control" name="img">
                  <div class="help-block with-errors">
                      @if ($errors->has('img'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Gambar tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog,wordcount',
            wordcount: {
                showParagraphs: false,
                showWordCount: false,
                showCharCount: true,
                countSpacesAsChars:true,
                countHTML:false,
                maxWordCount: -1,
                maxCharCount: 200
            }
        } );
    </script>
@endsection