@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/download-file') }}">Data Download File</a></li>
  <li class="active">Buat Baru</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Tambah Data Download File    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/add-download-file') }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}

                <div class="form-group">
                  <label class="control-label">Title *</label>
                  <input type="text" class="form-control" name="title">
                  <div class="help-block with-errors">
                      @if ($errors->has('title'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Title tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group filled">
                  <label class="control-label">Tipe *</label>
                  <select class="form-control" name="tipe">
                    <option value="">Pilih Tipe</option>
                    <option value="0">Brosur</option>
                    <option value="1">Katalog</option>
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('tipe'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Tipe belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>File *</label>
                  <input type="file" class="form-control" name="file">
                  <div class="help-block with-errors">
                      @if ($errors->has('file'))
                          <span class="help-block">
                              <p class="text-danger"><strong>File tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection