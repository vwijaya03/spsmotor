@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data SPS Youtube</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data SPS Youtube    </h1>
      <br>
        <button onclick="location.href='{{ url($url_admin_prefix.'/add-sps-youtube') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Url</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($sps_youtube as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->title}}</td>
                <td>{{$data->url}}</td>
                <td><a href="{{ url($url_admin_prefix.'/edit-sps-youtube/'.$data->id) }}">Lihat Detail</a></td>
                <td><a onclick="deleteSpsYoutube('{{$data->id}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection