@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Gallery Cabang</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Gallery Cabang    </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-gallery-cabang') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Cabang</th>
                <th>Title</th>
                <th>Description</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($gallery_cabang as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->cabang_name}}</td>
                <td>{{$data->title}}</td>
                <td>{{strip_tags($data->description)}}</td>
                <td><a href="{{ url($data->img_path) }}" target="_blank">Lihat Gambar</a></td>
                <td><a href="{{ url($url_admin_prefix.'/edit-gallery-cabang/'.$data->id) }}">Edit</a></td>
                <td><a onclick="deleteGalleryCabang('{{$data->id}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection