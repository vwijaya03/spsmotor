@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Pengaturan Website</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Pengaturan Website    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/edit-setting/'.$data->id) }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}

                <div class="form-group">
                  <label>Alamat *</label>
                  <input type="text" class="form-control" name="address" value="{{$data->address}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('address'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Alamat tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-4">
                      <label>Email *</label>
                      <input type="text" class="form-control" name="email" value="{{$data->email}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Email tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-4">
                      <label>No. Telepon *</label>
                      <input type="text" class="form-control" name="no_telepon" value="{{$data->no_telepon}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('no_telepon'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>No Telepon tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-4">
                      <label>No. Fax *</label>
                      <input type="text" class="form-control" name="no_fax" value="{{$data->no_fax}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('no_fax'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>No. Fax tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                  </div>
                </div>

                <div class="form-group">
                  <label>Informasi Hari Buka dan Jam Buka *</label>
                  <textarea name="informasi_hari_dan_jam" id="description_editor">
                    {{$data->informasi_hari_dan_jam}}
                  </textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('informasi_hari_dan_jam'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Informasi Hari Buka dan Jam Buka tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                      <label>Latitude Perusahaan *</label>
                      <input type="text" class="form-control" name="lat" value="{{$data->lat}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('lat'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Latitude Perusahaan tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Longitude Perusahaan *</label>
                      <input type="text" class="form-control" name="lng" value="{{$data->lng}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('lng'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Longitude Perusahaan tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection