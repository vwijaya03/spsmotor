
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>Auto Club</title>

		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

		<link href="{{ URL::asset('public/css/master.css') }}" rel="stylesheet">

		<!-- SWITCHER -->
		<link rel="stylesheet" id="switcher-css" type="text/css" href="{{ URL::asset('public/assets/switcher/css/switcher.css') }}" media="all" />

		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	
	@if($mListTable == 1)
	<?php 
	$body_class = '';
	$body_class = 'm-listTable'; 
	?>
	@else
	<?php $body_class = 'm-index'; ?>
	@endif
	<body class="<?php echo($body_class); ?>" data-scrolling-animations="true" data-equal-height=".b-auto__main-item">

		<!-- Loader -->
		<div id="page-preloader"><span class="spinner"></span></div>
		<!-- Loader end -->

		<header class="b-topBar wow slideInDown" data-wow-delay="0.3s">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-xs-6">
						<div class="b-topBar__addr">
							<span class="fa fa-map-marker"></span>
							202 W 7TH ST, LOS ANGELES, CA 90014
						</div>
					</div>
					<div class="col-md-2 col-xs-6">
						<div class="b-topBar__tel">
							<span class="fa fa-phone"></span>
							1-800- 624-5462
						</div>
					</div>
				</div>
			</div>
		</header><!--b-topBar-->

		<nav class="b-nav">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-xs-4">
						<div class=" wow slideInLeft" data-wow-delay="0.3s">
							<img src="{{ URL::asset('/public/logo.png') }}">
						</div>
					</div>
					<div class="col-sm-9 col-xs-8">
						<div class="b-nav__list wow slideInRight" data-wow-delay="0.3s">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="collapse navbar-collapse navbar-main-slide" id="nav">
								<ul class="navbar-nav-menu">
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="home.html">Home <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="{{ url('/') }}">Home Page 1</a></li>
										</ul>
									</li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">Grid <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="{{ url('/listing') }}">listing 1</a></li>
										</ul>
									</li>
									<li><a href="{{ url('/about') }}">About</a></li>
									<li><a href="{{ url('/article') }}">Services</a></li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">Blog <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="{{ url('/blog') }}">Blog 1</a></li>
											<li><a href="404.html">Page 404</a></li>
										</ul>
									</li>
									<li><a href="submit1.html">Shop</a></li>
									<li><a href="contacts.html">Contact</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav><!--b-nav-->

		@yield('content')

		@yield('google_map_script')
		
		<!--Main-->   
		<script src="{{ URL::asset('public/js/jquery-1.11.3.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery-ui.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/modernizr.custom.js') }}"></script>

		<script src="{{ URL::asset('public/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/waypoints.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery.easypiechart.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/classie.js') }}"></script>

		<!--Switcher-->
		<script src="{{ URL::asset('public/assets/switcher/js/switcher.js') }}"></script>
		<!--Owl Carousel-->
		<script src="{{ URL::asset('public/assets/owl-carousel/owl.carousel.min.js') }}"></script>
		<!--bxSlider-->
		<script src="{{ URL::asset('public/assets/bxslider/jquery.bxslider.js') }}"></script>
		<!-- jQuery UI Slider -->
		<script src="{{ URL::asset('public/assets/slider/jquery.ui-slider.js') }}"></script>

		<!--Theme-->
		<script src="{{ URL::asset('public/js/jquery.smooth-scroll.js') }}"></script>
		<script src="{{ URL::asset('public/js/wow.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery.placeholder.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/theme.js') }}"></script>

	</body>
</html>