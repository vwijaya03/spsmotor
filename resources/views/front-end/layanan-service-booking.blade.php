@extends('front-end/header')

@section('content')
<style type="text/css">
	#map {
        height: 100%;
        min-height: 250px;
      }
	.timepicker-hour {
		position: absolute;
		left: 15%;
		top: 38%;
	}

	.timepicker-minute {
		position: absolute;
		right: 15%;
		top: 38%;
	}
</style>

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0.5s">Service Booking</h1>
		<div class="b-pageHeader__search zoomInRight" data-wow-delay="0.5s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow zoomInUp" data-wow-delay="0.5s">
	<div class="container">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Beranda</a><span class="fa fa-angle-right"></span><a href="{{ url ('/layanan-service-booking') }}" class="b-breadCumbs__page m-active">Service Booking</a>
	</div>
</div><!--b-breadCumbs-->

<section class="b-contacts s-shadow">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="b-contacts__form">
					<header class="b-contacts__form-header s-lineDownLeft zoomInUp" data-wow-delay="0.5s">
						<h2 class="s-titleDet">Formulir Service Booking</h2> 
					</header>

					@if(Session::has('done'))
		                <div class="alert alert-success">
						  	{{Session::get('done')}}
						</div>
		            @endif

					<form action="{{ url('/layanan-service-booking') }}" method="POST" id="contactForm" novalidate class="s-form zoomInUp" data-wow-delay="0.5s">
						
						{!! csrf_field() !!}
						
						<div class="col-md-4">
							@if ($errors->has('cabang_id'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Cabang belum ada yang dipilih !</h6></strong></p>
	                      	@endif
							<div class="s-relative">
								<select name="cabang_id" id="user-topic" class="m-select" required>
									<option value="">Pilih Cabang</option>
									@foreach($cabang as $data_content_cabang)
									<option value="{{$data_content_cabang->id}}">{{$data_content_cabang->name}}</option>
									@endforeach
								</select>
								<span class="fa fa-caret-down"></span>
							</div>
						</div>

						<div class="col-md-4">
							@if ($errors->has('fullname'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Nama Lengkap tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input type="text" placeholder="Nama Lengkap" name="fullname" id="user-name" required="required" />
						</div>

						<div class="col-md-4">
							@if ($errors->has('email'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Email tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input type="text" placeholder="Email" name="email" id="user-email" required />
						</div>
						
						<div class="col-md-4">
							@if ($errors->has('no_telepon'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Nomor Telepon tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input type="text" placeholder="No. Telepon" name="no_telepon" id="user-phone" required />
						</div>

						<div class="col-md-4">
							@if ($errors->has('no_polisi'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Nomor Polisi tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input type="text" placeholder="No. Polisi" name="no_polisi" id="user-phone" required />
						</div>

						<div class="col-md-4">
							@if ($errors->has('jenis_kendaraan'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Jenis Kendaraan tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<div class="s-relative">
								<select name="jenis_kendaraan" id="user-topic" class="m-select" required>
									<option value="">Pilih Jenis Kendaraan</option>
									<option value="cub">CUB / Bebek</option>
			                        <option value="matic">Matic</option>
			                        <option value="sport">Sport</option>
								</select>
								<span class="fa fa-caret-down"></span>
							</div>
						</div>

						<div class="col-md-8">
							@if ($errors->has('jenis_service'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Jenis Service tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<div class="s-relative">
								<select name="jenis_service" id="user-topic" class="m-select" required>
									<option value="">Pilih Jenis Service</option>
			                        <option value="servis lengkap">Servis Lengkap (Servis & Ganti Oli)</option>
			                        <option value="servis kpb">Servis KPB</option>
			                        <option value="servis ganti oli">Servis Ganti Oli</option>
			                        <option value="servis sparepart">Servis Sparepart</option>
								</select>
								<span class="fa fa-caret-down"></span>
							</div>
						</div>

						<div class="col-md-6">
							@if ($errors->has('tanggal_booking'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Tanggal Booking tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input class="datepicker" name="tanggal_booking" placeholder="Tanggal Service Booking" type="text" required/>
						</div>

						<div class="col-md-6">
							@if ($errors->has('jam_booking'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Jam Booking tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<input class="timepicker" name="jam_booking" placeholder="Jam Service Booking" type="text" required/>
						</div>

						<div class="col-md-12">
							@if ($errors->has('description'))
	                      		<p class="text-danger"><strong><h6 style="color:red !important;">Keluhan tidak boleh kosong !</h6></strong></p>
	                      	@endif
							<textarea id="user-message" name="description" placeholder="Keluhan Tentang Motor Anda" required></textarea>
						</div>

						<div class="col-md-12">
							<button type="submit" class="btn m-btn">Kirim<span class="fa fa-angle-right"></span></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section><!--b-contacts-->

<style type="text/css">
  input::-webkit-input-placeholder{
    color:white !important;
  }
  textarea::-webkit-input-placeholder{
    color:white !important;
  }
  input:-moz-placeholder {
      color:white !important;
  }
  textarea:-moz-placeholder {
      color:white !important;
  }
</style>

@endsection

@section('datepicker_js')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
	
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script>
	// var date_input=$('input[name="date"]'); //our date input has the name "date"
	$('.datepicker').datepicker({
		format: 'dd-mm-yyyy',
		todayHighlight: true,
		autoclose: true,
		maxViewMode: 0
	});

	$('.timepicker').datetimepicker({
		format: 'HH:mm'
	});
</script>

@endsection