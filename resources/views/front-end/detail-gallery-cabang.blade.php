@extends('front-end/header')

@section('content')

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0s">Gallery Foto Cabang {{$cabang->name}}</h1>
		<div class="b-pageHeader__search zoomInRight">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container zoomInUp" data-wow-delay="0s">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="{{ url('/gallery-cabang/'.$cabang->slug) }}" class="b-breadCumbs__page m-active">Gallery Foto Cabang {{$cabang->name}}</a>
	</div>
</div><!--b-breadCumbs-->

<div class="b-items">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-8 col-xs-12">
				<div class="row">
					@if(count($gallery_cabang) != 0)
						@foreach($gallery_cabang as $data_gallery_cabang)
						<div class="col-lg-4 col-md-6 col-xs-12">
							<div class="b-items__cell zoomInUp">
								<div class="b-items__cars-one-img">
									<img class='img-responsive' height="300px" src="{{ URL::asset($data_gallery_cabang->img_path) }}" alt='chevrolet'/>
								</div>
								<div class="b-items__cell-info">
									<div class="b-items__cell-info-title">
										<h2 class="">{{ $data_gallery_cabang->title }}</h2>
									</div>
									<p style="height: 80px;">
										{{ strip_tags(substr($data_gallery_cabang->description, 0, 160)) }} 
					                    @if(strlen($data_gallery_cabang->description) >= 160)
					                    ...
					                    @else
					                    @endif
									</p>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p align="middle" style="color: white;">Data tidak di temukan.</p>
					@endif
				</div>
			</div>
		</div>
		{{ $gallery_cabang->links('pagination') }} 
	</div>
</div><!--b-items-->

@endsection