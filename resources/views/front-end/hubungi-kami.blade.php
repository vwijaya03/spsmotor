@extends('front-end/header')

@section('content')
<style type="text/css">
	#map {
        height: 100%;
        min-height: 250px;
      }
</style>

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0.5s">Hubungi Kami</h1>
		<div class="b-pageHeader__search zoomInRight" data-wow-delay="0.5s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow zoomInUp" data-wow-delay="0.5s">
	<div class="container">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Beranda</a><span class="fa fa-angle-right"></span><a href="{{ url ('/hubungi-kami') }}" class="b-breadCumbs__page m-active">Hubungi Kami</a>
	</div>
</div><!--b-breadCumbs-->

<section class="b-contacts s-shadow">
	<div class="container">
		<div id="map">
			
		</div><br>
		<div class="row">
			<div class="col-xs-6">
				<div class="b-contacts__form">
					<header class="b-contacts__form-header s-lineDownLeft zoomInUp" data-wow-delay="0.5s">
						<h2 class="s-titleDet">Contact Form</h2> 
					</header>

					@if(Session::has('done'))
		                <div class="alert alert-success">
						  	{{Session::get('done')}}
						</div>
		            @endif

					<form action="{{ url('/hubungi-kami') }}" method="POST" id="contactForm" novalidate class="s-form zoomInUp" data-wow-delay="0.5s">

						{!! csrf_field() !!}
						@if ($errors->has('cabang_id'))
                      		<p class="text-danger"><strong><h6 style="color:red !important;">Cabang belum ada yang dipilih !</h6></strong></p>
                      	@endif
						<div class="s-relative">
							<select name="cabang_id" id="user-topic" class="m-select">
								<option value="">Pilih Cabang</option>
								@foreach($cabang as $data_content_cabang)
								<option value="{{$data_content_cabang->id}}">{{$data_content_cabang->name}}</option>
								@endforeach
							</select>
							<span class="fa fa-caret-down"></span>
						</div>
						
						@if ($errors->has('fullname'))
                      		<p class="text-danger"><strong><h6 style="color:red !important;">Nama Lengkap tidak boleh kosong !</h6></strong></p>
                      	@endif
						<input type="text" placeholder="Nama Lengkap" value="" name="fullname" id="user-name" />

						@if ($errors->has('email'))
                      		<p class="text-danger"><strong><h6 style="color:red !important;">Email tidak boleh kosong !</h6></strong></p>
                      	@endif
						<input type="text" placeholder="Email" value="" name="email" id="user-email" />

						@if ($errors->has('no_telepon'))
                      		<p class="text-danger"><strong><h6 style="color:red !important;">Nomor Telepon tidak boleh kosong !</h6></strong></p>
                      	@endif
						<input type="text" placeholder="No. Telepon" value="" name="no_telepon" id="user-phone" />

						@if ($errors->has('description'))
                      		<p class="text-danger"><strong><h6 style="color:red !important;">Kritik / Saran tidak boleh kosong !</h6></strong></p>
                      	@endif
						<textarea id="user-message" name="description" placeholder="Kritik / Saran"></textarea>

						<button type="submit" class="btn m-btn">Kirim<span class="fa fa-angle-right"></span></button>
					</form>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="b-contacts__address">

					<div class="b-contacts__address-info">
						<h2 class="s-titleDet zoomInUp" data-wow-delay="0.5s">Lokasi</h2> 
						
						<div class="s-relative">
							<select style="width: 50%; color: white;" name="cabang_id" id="change_cabang" class="m-select" onchange="changeCabang('{{ url('/hubungi-kami/') }}')">
								<option value="">Pilih Cabang</option>
								@foreach($cabang as $data_content_cabang)
								<option value="{{$data_content_cabang->slug}}">{{$data_content_cabang->name}}</option>
								@endforeach
							</select>
							
						</div> 
						<br>
						<address class="b-contacts__address-info-main zoomInUp" data-wow-delay="0.5s">
							<div class="b-contacts__address-info-main-item">
								<span class="fa fa-home" style="color: black !important;"></span>
								<span style="color: black !important;">Alamat</span>
								<p style="color: black;">
								@if($default_cabang != null)
								{{ $default_cabang->address }}
								@else
								@endif
								</p>
							</div>
							<div class="b-contacts__address-info-main-item">
								<div class="row">
									<div class="col-lg-3 col-md-4 col-xs-12">
										<span class="fa fa-phone" style="color: black;"></span>
										<span style="color: black !important;">PHONE</span>
									</div>
									<div class="col-lg-9 col-md-8 col-xs-12">
										<em style="color: black !important;">
										@if($default_cabang != null)
										{{ $default_cabang->no_telepon }}
										@else
										@endif
										</em>
									</div>
								</div>
							</div>

							<div class="b-contacts__address-info-main-item">
								<div class="row">
									<div class="col-lg-3 col-md-4 col-xs-12">
										<span class="fa fa-envelope" style="color: black;"></span>
										<span style="color: black !important;">EMAIL</span>
									</div>
									<div class="col-lg-9 col-md-8 col-xs-12">
										<em style="color: black;">
										@if($default_cabang)
										{{ $default_cabang->email_customer_service_cabang }}
										@else
										@endif
										</em>
									</div>
								</div>
							</div>
							<br>
							@if($default_cabang)
								<?php echo $default_cabang->informasi_hari_dan_jam; ?>
							@else
							@endif
						</address>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!--b-contacts-->

<style type="text/css">
	input::-webkit-input-placeholder{
		color:white !important;
	}
	textarea::-webkit-input-placeholder{
		color:white !important;
	}
	input:-moz-placeholder {
	    color:white !important;
	}
	textarea:-moz-placeholder {
	    color:white !important;
	}
</style>
@endsection

@section('google_map_script')

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDbhgNiMBQ7soSSGOMxcMOqN1rBPIOs8k&callback=initMap"></script>
<script type="text/javascript">
	function initMap() 
	{
		var myLatLng = {lat: <?php echo $default_cabang->lat; ?>, lng: <?php echo $default_cabang->lng; ?>};

		var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 17,
		center: myLatLng
		});

		var marker = new google.maps.Marker({
		position: myLatLng,
		map: map
		});
	}
</script>

@endsection

@section('custom_script')
	
	<script type="text/javascript">
	      // bind change event to select
		function changeCabang(base_url)
		{
			var select = document.getElementById('change_cabang');
			var option_value = select.options[select.selectedIndex].value;
			console.log(option_value);
			window.location.href = base_url+'/'+option_value;
		}
	</script>
@endsection