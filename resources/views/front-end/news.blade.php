@extends('front-end/header')

@section('content')

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0s">Berita</h1>
		<div class="b-pageHeader__search zoomInRight" data-wow-delay="0s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container zoomInUp" data-wow-delay="0s">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Beranda</a><span class="fa fa-angle-right"></span><a href="{{ url('/news') }}" class="b-breadCumbs__page m-active">Berita</a>
	</div>
</div><!--b-breadCumbs-->

<div class="b-items">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-8 col-xs-12">
				<div class="row">
					@if(count($news) != null)
						@foreach($news as $data_news)
						<div class="col-lg-3 col-md-6 col-xs-12">
							<div class="b-items__cell zoomInUp">
								<div class="b-items__cars-one-img">
									<img class='img-responsive' height="200px" src="{{ URL::asset($data_news->img_path) }}" alt='chevrolet'/>
								</div>
								<div class="b-items__cell-info">
									<div class="b-items__cell-info-title">
										<h2 class="">
										{{ strip_tags(substr($data_news->title, 0, 40)) }} 
					                    @if(strlen($data_news->title) >= 40)
					                    ...
					                    @else
					                    @endif
										</h2>
									</div>
									<p style="height: 80px;">
										{{ strip_tags(substr($data_news->description, 0, 160)) }} 
					                    @if(strlen($data_news->description) >= 160)
					                    ...
					                    @else
					                    @endif
									</p>
									<div class="row m-smallPadding">
										<div class="col-xs-6">
											<a href="{{ url('/news/'.$data_news->slug) }}" class="btn m-btn">LIHAT DETAIL BERITA<span class="fa fa-angle-right"></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p align="middle">Data tidak di temukan.</p>
					@endif
				</div>
			</div>
		</div>
		{{ $news->links('pagination') }} 
	</div>
</div><!--b-items-->

@endsection