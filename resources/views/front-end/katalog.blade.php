@extends('front-end/header')

@section('content')

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0s">Katalog SPS Motor</h1>
		<div class="b-pageHeader__search zoomInRight" data-wow-delay="0s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container">
		<div class="row">
			<form action="{{ url('/cari-katalog') }}" method="GET">
				<div class="col-lg-4 col-sm-8 col-xs-12">
					<input type="text" class="form-control" name="search_katalog" placeholder="Cari Katalog...">
				</div>
				<div class="col-lg-2 col-sm-8 col-xs-12">
					<button type="submit" class="form-control">Cari Katalog<span class="fa fa-angle-right"></span></button>
				</div>
			</form>
		</div>
	</div>
</div><!--b-breadCumbs-->

<div class="b-items">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-8 col-xs-12">
				<div class="row">
					@if(count($katalog) != 0)
						@foreach($katalog as $data_katalog)
						<div class="col-lg-3 col-md-6 col-xs-12">
							<div class="b-items__cell zoomInUp" style="height: 400px !important;">
								<div class="b-items__cars-one-img">
									<img align="middle" class='img-responsive' height="200px" src="{{ URL::asset('/public/file-icon.png') }}" alt='chevrolet'/>
								</div>
								<div class="b-items__cell-info">
									<div class="b-items__cell-info-title">
										@if(strlen($data_katalog->title) >= 27)
										<?php
											$font_size = 0;
											$font_size = 2;
										?>
										@else
											<?php $font_size = 3;?>
										@endif
										<h2><font size="<?php echo($font_size); ?>">{{ $data_katalog->title }}</font></h2>
									</div>
									<p style="height: 0px;">
									</p>
									<div class="row m-smallPadding">
										<div class="col-xs-6">
											<a href="{{ url($data_katalog->path) }}" class="btn m-btn" target="_blank">Download Katalog<span class="fa fa-angle-right"></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p align="middle">Data tidak di temukan.</p>
					@endif
				</div>
			</div>
		</div>
		{{ $katalog->links('pagination') }} 
	</div>
</div><!--b-items-->

@endsection