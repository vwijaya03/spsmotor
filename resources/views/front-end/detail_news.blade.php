@extends('front-end/header')

@section('content')
<style type="text/css">
	#map {
        height: 100%;
        min-height: 250px;
      }
</style>

<section class="b-pageHeader">
	<div class="container">
		<h1 class="wow zoomInLeft" data-wow-delay="0s">{{$news->title}}</h1>
		<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Beranda</a>
		<span class="fa fa-angle-right"></span>
		<a href="{{ url('/news') }}" class="b-breadCumbs__page">Berita</a>
		<span class="fa fa-angle-right"></span>
		<a href="{{ url('/news/'.$news->slug) }}" class="b-breadCumbs__page m-active">{{$news->title}}</a>
	</div>
</div><!--b-breadCumbs-->

<section class="b-best">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-xs-12">
				<div class="b-best__info">
					<header class="s-lineDownLeft b-best__info-head">
						<h2 class="zoomInUp" data-wow-delay="0s">{{$news->title}}</h2>
					</header>
					<?php echo($news->description); ?>
				</div>
			</div>
			
			<div class="col-sm-4 col-xs-12">
				<img class="img-responsive center-block zoomInUp" alt="{{$news->name}}" src="{{ URL::asset($news->img_path) }}" />
			</div>
		</div>
	</div>
</section><!--b-best-->

@endsection