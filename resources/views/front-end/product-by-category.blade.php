@extends('front-end/header')

@section('content')

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0s">Kategori {{$content_category_product->name}}</h1>
		<div class="b-pageHeader__search zoomInRight" data-wow-delay="0s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container zoomInUp" data-wow-delay="0s">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="{{ url('/kategori-produk/'.$content_category_product->slug) }}" class="b-breadCumbs__page m-active">Kategori {{$content_category_product->name}}</a>
	</div>
</div><!--b-breadCumbs-->

<div class="b-items">
	<div class="container product_by_category_container">
		<div class="row" style="background-color: rgba(51, 51, 51, 0.8) !important;">
			<div class="col-lg-12 col-sm-8 col-xs-12">
				<div class="row">
					@if(count($content_product) != null)
						@foreach($content_product as $data_content_product)
						<div class="col-lg-3 col-md-6 col-xs-12">
							<div class="b-items__cell zoomInUp" style="height: 475px !important;">
								<div class="b-items__cars-one-img">
									<img class='img-responsive' height="200px" src="{{ URL::asset($data_content_product->path) }}" alt='chevrolet'/>
								</div>
								<div class="b-items__cell-info">
									<div class="b-items__cell-info-title">
										<h2 class="">{{ $data_content_product->name }}</h2>
									</div>
									<p style="height: 60px;">
										{{ strip_tags(substr($data_content_product->description, 0, 160)) }} 
					                    @if(strlen($data_content_product->description) >= 160)
					                    ...
					                    @else
					                    @endif
									</p>
									<div class="row m-smallPadding">
										<div class="col-xs-6">
											<a href="{{ url('/produk/'.$data_content_product->slug) }}" class="btn m-btn">LIHAT DETAIL<span class="fa fa-angle-right"></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p align="middle" style="color: white;">Data tidak di temukan.</p>
					@endif
				</div>
			</div>
		</div>
	</div>
</div><!--b-items-->

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal"><h3 style="color: white;">x</h3></button>
		<img src="{{ url('/public/genuine_parts.jpg') }}" width="100%">
	</div>
</div>

@endsection

@section('custom_script')

<script type="text/javascript">
    $(window).on('load',function(){
    	setTimeout(function(){ 
    		$('#myModal').modal('show');
    	}, 3000);
        
    });
</script>

@endsection