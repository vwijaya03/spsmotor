@extends('front-end/header')

@section('content')

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0s">Produk</h1>
		<div class="b-pageHeader__search zoomInRight" data-wow-delay="0s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container zoomInUp" data-wow-delay="0s">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="{{ url('/produk') }}" class="b-breadCumbs__page m-active">Produk</a>
	</div>
</div><!--b-breadCumbs-->

<div class="b-items">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-8 col-xs-12">
				<div class="row">
					@if(count($content_product) != null)
						@foreach($content_product as $data_content_product)
						<div class="col-lg-3 col-md-6 col-xs-12">
							<div class="b-items__cell zoomInUp" style="height: 495px !important;">
								<div class="b-items__cars-one-img">
									<img class='img-responsive' height="200px" src="{{ URL::asset($data_content_product->path) }}" alt='chevrolet'/>
								</div>
								<div class="b-items__cell-info">
									<div class="b-items__cell-info-title">
										@if(strlen($data_content_product->name) >= 27)
										<?php
											$font_size = 0;
											$font_size = 2;
										?>
										@else
											<?php $font_size = 3;?>
										@endif
										<h2><font size="<?php echo($font_size); ?>">{{ $data_content_product->name }}</font></h2>
									</div>
									<p style="height: 80px;">
										{{ strip_tags(substr($data_content_product->description, 0, 160)) }} 
					                    @if(strlen($data_content_product->description) >= 160)
					                    ...
					                    @else
					                    @endif
									</p>
									<div class="row m-smallPadding">
										<div class="col-xs-6">
											<a href="{{ url('/produk/'.$data_content_product->slug) }}" class="btn m-btn">LIHAT GALLERY<span class="fa fa-angle-right"></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p align="middle">Data tidak di temukan.</p>
					@endif
				</div>
			</div>
		</div>
	</div>
</div><!--b-items-->

@endsection