@extends('front-end/header')

@section('content')
<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInUp">Langganan Berita Berhasil</h1>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow">
	<div class="container">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Beranda</a>
	</div>
</div><!--b-breadCumbs-->

<section class="b-error s-shadow">
	<div class="container">
		<h1 class="zoomInUp">
		@if(Session::has('done'))
        	{{ Session::get('done') }}        
        @endif
        </h1>
</section><!--b-error-->

<section class="b-error s-shadow">
	<div class="container">
		<h1 class="zoomInUp">
        </h1>
</section><!--b-error-->

<section class="b-error s-shadow">
	<div class="container">
		<h1 class="zoomInUp">
        </h1>
</section><!--b-error-->

@endsection