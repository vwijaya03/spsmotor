@extends('front-end/header')

@section('content')

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0s">SPS Youtube</h1>
		<div class="b-pageHeader__search zoomInRight">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container zoomInUp" data-wow-delay="0s">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="{{ url('/sps-youtube/') }}" class="b-breadCumbs__page m-active">SPS Youtube</a>
	</div>
</div><!--b-breadCumbs-->

<div class="b-items">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-8 col-xs-12">
				<div class="row">
					@if(count($sps_youtube) != 0)
						@foreach($sps_youtube as $data_sps_youtube)
						<div class="col-lg-3 col-md-6 col-xs-12">
							<div class="b-items__cell zoomInUp" style="height: 240px !important;">
								<div class="b-items__cars-one-img">
									<iframe class='img-responsive' width="100%" src="{{$data_sps_youtube->url}}" frameborder="0" allowfullscreen></iframe>
								</div>
								<div class="b-items__cell-info">
									<div class="b-items__cell-info-title">
										<h2 class="">{{ $data_sps_youtube->title }}</h2>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p align="middle">Data tidak di temukan.</p>
					@endif
				</div>
			</div>
		</div>
		{{ $sps_youtube->links('pagination') }} 
	</div>
</div><!--b-items-->

@endsection