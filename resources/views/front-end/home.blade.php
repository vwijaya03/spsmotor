@extends('front-end/header')

@section('content')

<section class="b-slider"> 
	<div id="carousel" class="slide carousel carousel-fade" style="background: url(public/images/sirkuit.jpg); width: 100%;">
		<div class="carousel-inner">
			<?php $i = 1;?>
			@foreach($product as $data_product)

				@if($i == 1)
					<div class="item active">
						<img align="middle" src="{{ URL::asset($data_product->path) }}" alt="sliderImg" />
						<div class="container">
							<div class="carousel-caption b-slider__info">
								<h2><p><span>{{$data_product->name}}</span></p></h2>
								<p><span>{{$data_product->category_name}}</span></p>
								<a class="btn m-btn" href="{{ url('/produk/'.$data_product->slug) }}">Lihat Detail<span class="fa fa-angle-right"></span></a>
							</div>
						</div>
					</div>
				@else
					<div class="item">
						<img align="middle" src="{{ URL::asset($data_product->path) }}" alt="sliderImg" />
						<div class="container">
							<div class="carousel-caption b-slider__info">
								<h2><p><span>{{$data_product->name}}</span></p></h2>
								<p><span>{{$data_product->category_name}}</span></p>
								<a class="btn m-btn" href="{{ url('/produk/'.$data_product->slug) }}">Lihat Detail<span class="fa fa-angle-right"></span></a>
							</div>
						</div>
					</div>
				@endif
			<?php $i++; ?>
			@endforeach
		</div>
		<a class="carousel-control right" href="#carousel" data-slide="next">
			<span class="fa fa-angle-right m-control-right"></span>
		</a>
		<a class="carousel-control left" href="#carousel" data-slide="prev">
			<span class="fa fa-angle-left m-control-left"></span>
		</a>
	</div>
</section><!--b-slider-->

<section class="b-world">
	<div class="container news_container">
		<h2 class="s-title" data-wow-delay="0s" data-wow-offset="100">Berita Terbaru</h2>
		<div class="row" style="background-color: rgba(51, 51, 51, 0.8) !important;">
			
			@foreach($news as $data_news)
			<div class="col-sm-4 col-xs-12">
				<div class="b-world__item wow zoomInUp">
					<img class="img-responsive" width="370" height="200" src="{{ URL::asset($data_news->img_path) }}" />
					<div class="b-world__item-val">
					</div>
					<h2>
					{{ strip_tags(substr($data_news->title, 0, 40)) }} 
                    @if(strlen($data_news->title) >= 40)
                    ...
                    @else
                    @endif
					</h2>
					<p>{{ strip_tags(substr($data_news->description, 0, 152)) }}</p>
					<a href="{{ url('/news/'.$data_news->slug) }}" class="btn m-btn">LIHAT DETAIL<span class="fa fa-angle-right"></span></a>

				</div>
				<br>
			</div>
			@endforeach
		</div>
	</div>
</section><!--b-world-->

<section class="b-welcome">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-md-offset-2 col-sm-6 col-xs-12">
				<div class="b-welcome__text fadeInLeft" data-wow-delay="0s" data-wow-offset="100">
					<h2>Dealer Honda Resmi</h2>
					<h3>Selamat Datang Di SPS Motor</h3>
					<p>Dealer resmi sepeda motor HONDA PT. Sumber Purnama Sakti yang dikenal juga dengan sebutan SPS MOTOR adalah perusahaan perdagangan otomotif yang ada dibeberapa wilayah Indonesia, dengan perkembangan tekonologi saat ini SPS Motor bergerak dalam perkembangan perusahaan berbasis teknologi dan kami memperkenalkan Website resmi SPS Motor dan tersedia juga di Smartphone Android serta Sistem Administrasi dan Management kami berbasis teknologi yang memudahkan customer serta perhitungan yang tepat, cepat dan akurat untuk melayani customer SPS Motor tentunya.</p>
				</div>
			</div>
			<div class="col-md-5 col-sm-6 col-xs-12">
				<div class="b-welcome__services fadeInRight" data-wow-delay="0s" data-wow-offset="100">
					<div class="row">
						<div class="col-xs-6 m-padding">
							<div class="b-welcome__services-auto">
								<a style="color: black;" href="{{ url('/layanan-service-booking') }}">
									<div class="b-welcome__services-img m-auto">
										<span class="fa fa-cab"></span>
									</div>
									<h3>Service Booking</h3>
								</a>
								
							</div>
						</div>
						<div class="col-xs-6 m-padding">
							<div class="b-welcome__services-trade">
								<a style="color: black;" href="{{ url('/layanan-karir') }}">
									<div class="b-welcome__services-img m-trade">
										<span class="fa fa-male"></span>
									</div>
									<h3>Karir</h3>
								</a>
							</div>
						</div>
						<div class="col-xs-12 text-center">
							<span class="b-welcome__services-circle"></span>
						</div>
						<div class="col-xs-6 m-padding">
							<div class="b-welcome__services-buying">
								<a style="color: black;" href="{{ url('/layanan-pesan-sparepart') }}">
									<div class="b-welcome__services-img m-buying">
										<span class="fa fa-cog"></span>
									</div>
									<h3>Pemesanan Sparepart</h3>
								</a>
								
							</div>
						</div>
						<div class="col-xs-6 m-padding">
							<div class="b-welcome__services-support">
								<a style="color: black;" href="{{ url('/hubungi-kami') }}">
								<div class="b-welcome__services-img m-support">
									<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										width="45px" height="45px" viewBox="0 0 612 612" xml:space="preserve">
										<g>
											<path d="M257.938,336.072c0,17.355-14.068,31.424-31.423,31.424c-17.354,0-31.422-14.068-31.422-31.424
												c0-17.354,14.068-31.423,31.422-31.423C243.87,304.65,257.938,318.719,257.938,336.072z M385.485,304.65
												c-17.354,0-31.423,14.068-31.423,31.424c0,17.354,14.069,31.422,31.423,31.422c17.354,0,31.424-14.068,31.424-31.422
												C416.908,318.719,402.84,304.65,385.485,304.65z M612,318.557v59.719c0,29.982-24.305,54.287-54.288,54.287h-39.394
												C479.283,540.947,379.604,606.412,306,606.412s-173.283-65.465-212.318-173.85H54.288C24.305,432.562,0,408.258,0,378.275v-59.719
												c0-20.631,11.511-38.573,28.46-47.758c0.569-84.785,25.28-151.002,73.553-196.779C149.895,28.613,218.526,5.588,306,5.588
												c87.474,0,156.105,23.025,203.987,68.43c48.272,45.777,72.982,111.995,73.553,196.779C600.489,279.983,612,297.925,612,318.557z
												M497.099,336.271c0-13.969-0.715-27.094-1.771-39.812c-24.093-22.043-67.832-38.769-123.033-44.984
												c7.248,8.15,13.509,18.871,17.306,32.983c-33.812-26.637-100.181-20.297-150.382-79.905c-2.878-3.329-5.367-6.51-7.519-9.417
												c-0.025-0.035-0.053-0.062-0.078-0.096l0.006,0.002c-8.931-12.078-11.976-19.262-12.146-11.31
												c-1.473,68.513-50.034,121.925-103.958,129.46c-0.341,7.535-0.62,15.143-0.62,23.08c0,28.959,4.729,55.352,12.769,79.137
												c30.29,36.537,80.312,46.854,124.586,49.59c8.219-13.076,26.66-22.205,48.136-22.205c29.117,0,52.72,16.754,52.72,37.424
												c0,20.668-23.604,37.422-52.72,37.422c-22.397,0-41.483-9.93-49.122-23.912c-30.943-1.799-64.959-7.074-95.276-21.391
												C198.631,535.18,264.725,568.41,306,568.41C370.859,568.41,497.099,486.475,497.099,336.271z M550.855,264.269
												C547.4,116.318,462.951,38.162,306,38.162S64.601,116.318,61.145,264.269h20.887c7.637-49.867,23.778-90.878,48.285-122.412
												C169.37,91.609,228.478,66.13,306,66.13c77.522,0,136.63,25.479,175.685,75.727c24.505,31.533,40.647,72.545,48.284,122.412
												H550.855L550.855,264.269z"/>
										</g>
									</svg>

								</div>
								<h3>Hubungi Kami</h3>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!--b-welcome-->

<section class="b-contact">
	<div class="container">
		<div class="row zoomInLeft" data-wow-delay="0s" data-wow-offset="100">
			<div class="col-xs-4">
				<div class="b-contact-title">
					<h5>Berita &amp; Promo Terbaru</h5><br />
					<h2>Langganan Berita</h2>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="b-contact__form">
					<p>Kami Akan Mengirimkan Email Ketika Ada Berita Atau Promo Terbaru !</p>
					<form action="{{ url('/subscribe') }}" method="POST">
						{!! csrf_field() !!}
						<div><span class="fa fa-user"></span><input type="text" name="fullname" placeholder="Nama Lengkap" /></div>
						<div><span class="fa fa-envelope"></span><input type="text" name="email" placeholder="Email" /></div>
						<button type="submit"><span class="fa fa-angle-right"></span></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section><!--b-contact-->

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal"><h3 style="color: white;">x</h3></button>
		<img src="{{ url('/public/cari_aman.jpg') }}" width="100%">
	</div>
</div>

@endsection

@section('custom_script')

<script type="text/javascript">
    $(window).on('load',function(){
    	setTimeout(function(){ 
    		$('#myModal').modal('show');
    	}, 3000);
        
    });
</script>

@endsection