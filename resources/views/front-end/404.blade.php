@extends('header')

@section('content')
<section class="b-pageHeader">
	<div class="container">
		<h1 class="wow zoomInUp" data-wow-delay="0.7s">Error 404 Page</h1>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow">
	<div class="container">
		<a href="home.html" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="404.html" class="b-breadCumbs__page m-active">Page Not Found</a>
	</div>
</div><!--b-breadCumbs-->

<section class="b-error s-shadow">
	<div class="container">
		<h1 class="wow zoomInUp" data-wow-delay="0.7s">Error</h1>
		<img class="img-responsive center-block wow zoomInUp" data-wow-delay="0.7s" src="{{ URL::asset('public/images/backgrounds/404.png') }}" alt="404" />
		<h2 class="s-lineDownCenter wow zoomInUp" data-wow-delay="0.7s">page not found</h2>
		<p class="wow zoomInUp" data-wow-delay="0.7s">The page you are looking for is not available and might have been removed, its name changed or is temprarily unavailable.</p>
		<h3 class="s-title wow zoomInUp" data-wow-delay="0.7s">TRY to finD a page</h3>
		<form class="b-blog__aside-search" action="/" method="post">
			<div>
				<input type="text" name="search" value=""/>
				<button type="submit"><span class="fa fa-search"></span></button>
			</div>        
		</form>
	</div>
	<img alt="audi" src="{{ URL::asset('public/images/backgrounds/404Bg.jpg') }}" class="img-responsive center-block b-error-img" />
</section><!--b-error-->

<div class="b-features">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
				<ul class="b-features__items">
					<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Low Prices, No Haggling</li>
					<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Largest Car Dealership</li>
					<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Multipoint Safety Check</li>
				</ul>
			</div>
		</div>
	</div>
</div><!--b-features-->

<div class="b-info">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-6">
				<aside class="b-info__aside wow zoomInLeft" data-wow-delay="0.3s">
					<article class="b-info__aside-article">
						<h3>OPENING HOURS</h3>
						<div class="b-info__aside-article-item">
							<h6>Sales Department</h6>
							<p>Mon-Sat : 8:00am - 5:00pm<br />
								Sunday is closed</p>
						</div>
						<div class="b-info__aside-article-item">
							<h6>Service Department</h6>
							<p>Mon-Sat : 8:00am - 5:00pm<br />
								Sunday is closed</p>
						</div>
					</article>
					<article class="b-info__aside-article">
						<h3>About us</h3>
						<p>Vestibulum varius od lio eget conseq
							uat blandit, lorem auglue comm lodo 
							nisl non ultricies lectus nibh mas lsa 
							Duis scelerisque aliquet. Ante donec
							libero pede porttitor dacu msan esct
							venenatis quis.</p>
					</article>
					<a href="about.html" class="btn m-btn">Read More<span class="fa fa-angle-right"></span></a>
				</aside>
			</div>
			<div class="col-md-3 col-xs-6">
				<div class="b-info__latest">
					<h3>LATEST AUTOS</h3>
					<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
						<div class="b-info__latest-article-photo m-audi"></div>
						<div class="b-info__latest-article-info">
							<h6><a href="detail.html">MERCEDES-AMG GT S</a></h6>
							<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
						</div>
					</div>
					<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
						<div class="b-info__latest-article-photo m-audiSpyder"></div>
						<div class="b-info__latest-article-info">
							<h6><a href="#">AUDI R8 SPYDER V-8</a></h6>
							<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
						</div>
					</div>
					<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
						<div class="b-info__latest-article-photo m-aston"></div>
						<div class="b-info__latest-article-info">
							<h6><a href="#">ASTON MARTIN VANTAGE</a></h6>
							<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-xs-6">
				<div class="b-info__twitter">
					<h3>from twitter</h3>
					<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s">
						<div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
						<div class="b-info__twitter-article-content">
							<p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
							<span>20 minutes ago</span>
						</div>
					</div>
					<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s">
						<div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
						<div class="b-info__twitter-article-content">
							<p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
							<span>20 minutes ago</span>
						</div>
					</div>
					<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s">
						<div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
						<div class="b-info__twitter-article-content">
							<p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
							<span>20 minutes ago</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-xs-6">
				<address class="b-info__contacts wow zoomInUp" data-wow-delay="0.3s">
					<p>contact us</p>
					<div class="b-info__contacts-item">
						<span class="fa fa-map-marker"></span>
						<em>202 W 7th St, Suite 233 Los Angeles,
							California 90014 USA</em>
					</div>
					<div class="b-info__contacts-item">
						<span class="fa fa-phone"></span>
						<em>Phone:  1-800- 624-5462</em>
					</div>
					<div class="b-info__contacts-item">
						<span class="fa fa-fax"></span>
						<em>FAX:  1-800- 624-5462</em>
					</div>
					<div class="b-info__contacts-item">
						<span class="fa fa-envelope"></span>
						<em>Email:  info@domain.com</em>
					</div>
				</address>
				<address class="b-info__map">
					<a href="contacts.html">Open Location Map</a>
				</address>
			</div>
		</div>
	</div>
</div><!--b-info-->

<footer class="b-footer">
	<a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>
	<div class="container">
		<div class="row">
			<div class="col-xs-4">
				<div class="b-footer__company wow fadeInLeft" data-wow-delay="0.3s">
					<div class="b-nav__logo">
						<h3><a href="home.html">Auto<span>Club</span></a></h3>
					</div>
					<p>&copy; 2015 Designed by Templines &amp; Powered by WordPress.</p>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="b-footer__content wow fadeInRight" data-wow-delay="0.3s">
					<div class="b-footer__content-social">
						<a href="#"><span class="fa fa-facebook-square"></span></a>
						<a href="#"><span class="fa fa-twitter-square"></span></a>
						<a href="#"><span class="fa fa-google-plus-square"></span></a>
						<a href="#"><span class="fa fa-instagram"></span></a>
						<a href="#"><span class="fa fa-youtube-square"></span></a>
						<a href="#"><span class="fa fa-skype"></span></a>
					</div>
					<nav class="b-footer__content-nav">
						<ul>
							<li><a href="home.html">Home</a></li>
							<li><a href="404.html">Pages</a></li>
							<li><a href="listings.html">Inventory</a></li>
							<li><a href="about.html">About</a></li>
							<li><a href="404.html">Services</a></li>
							<li><a href="blog.html">Blog</a></li>
							<li><a href="listTable.html">Shop</a></li>
							<li><a href="contacts.html">Contact</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</footer><!--b-footer-->

@endsection