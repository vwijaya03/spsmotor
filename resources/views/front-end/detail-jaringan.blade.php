@extends('front-end/header')

@section('content')

<section class="b-pageHeader">
	<div class="container">
		<h1 class="zoomInLeft" data-wow-delay="0s">Jaringan {{$content_category_cabang->name}}</h1>
		<div class="b-pageHeader__search zoomInRight">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container zoomInUp" data-wow-delay="0s">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="{{ url('/jaringan/'.$content_category_cabang->slug) }}" class="b-breadCumbs__page m-active">Jaringan {{$content_category_cabang->name}}</a>
	</div>
</div><!--b-breadCumbs-->

<div class="b-items">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-8 col-xs-12">
				<div class="row">
					@if(count($content_cabang) != null)
						@foreach($content_cabang as $data_content_cabang)
						<div class="col-lg-3 col-md-6 col-xs-12">
							<div class="b-items__cell wow zoomInUp">
								<div class="b-items__cars-one-img">
									<img class='img-responsive' height="200px" src="{{ URL::asset($data_content_cabang->img_path) }}" alt='chevrolet'/>
								</div>
								<div class="b-items__cell-info">
									<div class="b-items__cell-info-title">
										<h2 class="">{{ $data_content_cabang->name }}</h2>
									</div>
									<p style="height: 80px;">
										{{ strip_tags(substr($data_content_cabang->description, 0, 160)) }} 
					                    @if(strlen($data_content_cabang->description) >= 160)
					                    ...
					                    @else
					                    @endif
									</p>
									<div class="row m-smallPadding">
										<div class="col-xs-6">
											<a href="{{ url('/cabang/'.$data_content_cabang->slug) }}" class="btn m-btn">LIHAT DETAIL CABANG<span class="fa fa-angle-right"></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p align="middle">Data tidak di temukan.</p>
					@endif
				</div>
			</div>
		</div>
	</div>
</div><!--b-items-->

@endsection