@extends('front-end/header')

@section('content')

<section class="b-pageHeader">
	<div class="container">
		<h1 class="wow zoomInLeft" data-wow-delay="0s">Karir SPS Motor</h1>
		<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Beranda</a><span class="fa fa-angle-right"></span><a href="{{ url('/layanan-karir') }}" class="b-breadCumbs__page m-active">Karir</a>
	</div>
</div><!--b-breadCumbs-->

<section class="b-best">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-xs-12">
				<div class="b-best__info">
					<header class="s-lineDownLeft b-best__info-head">
						<h1 class="wow zoomInUp" data-wow-delay="0s">Karir</h1>
					</header>
					@if($karir)
					<?php echo($karir->description); ?>
					@else
					@endif
				</div>
			</div>
			
		</div>
	</div>
</section><!--b-best-->

@endsection