<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>DEALER HONDA SPS MOTOR</title>

		<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('public/favicon.png') }}" />

		<link href="{{ URL::asset('public/css/master.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{ URL::asset('/public/css/bootstrap-datepicker3.css') }}"/>
		<!-- SWITCHER -->
		<link rel="stylesheet" id="switcher-css" type="text/css" href="{{ URL::asset('public/assets/switcher/css/switcher.css') }}" media="all" />

		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

	@if($mListTable == 1)
	<?php 
	$body_class = '';
	$body_class = 'm-listTable'; 
	?>
	@else
	<?php $body_class = 'm-index'; ?>
	@endif
	<body class="<?php echo($body_class); ?>" data-scrolling-animations="true" data-equal-height=".b-auto__main-item">

		<!-- Loader -->
		<div id="page-preloader"><span class="spinner"></span></div>
		<!-- Loader end -->

		<header class="b-topBar wow slideInDown" data-wow-delay="0.3s">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-xs-6">
						<div class="b-topBar__addr">
							<marquee direction = "left"><h4 style="color: white;">Ngomong Motor Honda Topnya Beli Motor Di SPS Motor Tempatnya</h4></marquee>
						</div>
					</div>
				</div>
			</div>
		</header><!--b-topBar-->

		<nav class="b-nav">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-xs-4">
						<div class="slideInLeft" data-wow-delay="0s">
							<img src="{{ URL::asset('/public/logo.png') }}">
						</div>
					</div>
					<div class="col-sm-9 col-xs-8">
						<div class="b-nav__list slideInRight" data-wow-delay="0s">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="collapse navbar-collapse navbar-main-slide" id="nav">
								<ul class="navbar-nav-menu">
									<li class="dropdown">
										<a href="{{ url('/') }}">Beranda</a>
									</li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">Profile <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="{{ url('/tentang-kami') }}">Sejarah</a></li>
											<li><a href="{{ url('/gallery-cabang') }}">Foto Cabang</a></li>
											<li><a href="{{ url('/sps-youtube') }}">SPS Youtube</a></li>
										</ul>
									</li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">Jaringan <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											@foreach($category_cabang as $data_category_cabang)
											<li><a href="{{ url('/jaringan/'.$data_category_cabang->slug) }}">{{$data_category_cabang->name}}</a></li>
											@endforeach
										</ul>
									</li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">Produk <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											@foreach($category_product as $data_category_product)
											<li><a href="{{ url('/kategori-produk/'.$data_category_product->slug) }}">{{$data_category_product->name}}</a></li>
											@endforeach
										</ul>
									</li>
									<li><a href="{{ url('/news') }}">Berita</a></li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">Download <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="{{ url('/brosur') }}">Brosur</a></li>
											<li><a href="{{ url('/katalog') }}">Katalog</a></li>
										</ul>
									</li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">Layanan Kami <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="{{ url('/layanan-service-booking') }}">Service Booking</a></li>
											<li><a href="{{ url('/layanan-pesan-sparepart') }}">Pesan Sparepart</a></li>
											<li><a href="{{ url('/layanan-karir') }}">Karir</a></li>
											<li><a href="{{ url('/hubungi-kami') }}">Hubungi Kami</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav><!--b-nav-->

		@yield('content')
		
		<div class="b-features">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
						<ul class="b-features__items">
							
						</ul>
					</div>
				</div>
			</div>
		</div><!--b-features-->

		<div class="b-info">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-xs-6">
						<aside class="b-info__aside zoomInLeft" data-wow-delay="0s">
							<article class="b-info__aside-article">
								<h3>About us</h3>
								<p>
									@if($about)
										{{ strip_tags(substr($about->description, 0, 160)) }} 
					                    @if(strlen($about->description) >= 160)
					                    ...
					                    @else
					                    @endif
									@endif
									
								</p>
							</article>
							<a href="{{ url('/tentang-kami') }}" class="btn m-btn">Baca Lebih Lanjut<span class="fa fa-angle-right"></span></a>
						</aside>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-info__latest">
							<h3>PRODUK TERBARU</h3>
							@foreach($latest_product as $data_latest_product)
							<div class="b-info__latest-article zoomInUp" data-wow-delay="0s">
								<div class="b-info__latest-article-photo">
									<img width="80px" height="65px" src="{{ url($data_latest_product->path) }}">
								</div>
								<div class="b-info__latest-article-info">
									<h6><a href="{{ url('/produk/'.$data_latest_product->slug) }}">{{ $data_latest_product->name }}</a></h6>
									<p><span class="fa fa-tachometer"></span> {{$data_latest_product->category_name}} </p>
								</div>
							</div>
							@endforeach
						</div>
					</div>

					<div class="col-md-3 col-xs-6">
						<address class="b-info__contacts zoomInUp" data-wow-delay="0s">
							<p>HUBUNGI KAMI</p>
							<div class="b-info__contacts-item">
								<span style="visibility: hidden;" class="fa fa-map-marker"></span>
								<em>
								<a style="color: white;" href="{{ url('/hubungi-kami') }}">Klik Disini</a>
								</em>
							</div>
						</address>
					</div>
				</div>
			</div>
		</div><!--b-info-->

		<footer class="b-footer">
			<a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>
			<div class="container">
				<div class="row">
					<div class="col-xs-4">
						<div class="b-footer__company fadeInLeft" data-wow-delay="0s">
							<p>&copy; <?php echo date('Y'); ?> Copyright SPS Motor. All Right Reserved.</p>
						</div>
					</div>
					<div class="col-xs-8">
						<div class="b-footer__content fadeInRight" data-wow-delay="0s">
							<nav class="b-footer__content-nav">
								<ul>
									<li><a href="{{ url('/') }}">Beranda</a></li>
									<li><a href="{{ url('/cabang') }}">Cabang</a></li>
									<li><a href="{{ url('/produk') }}">Produk</a></li>
									<li><a href="{{ url('/news') }}">Berita</a></li>
									<li><a href="{{ url('/tentang-kami') }}">Tentang Kami</a></li>
									<li><a href="{{ url('/hubungi-kami') }}">Hubungi Kami</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</footer><!--b-footer-->
		@yield('google_map_script')

		
		<!--Main-->   
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
		<script src="{{ URL::asset('public/js/jquery-1.11.3.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery-ui.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/modernizr.custom.js') }}"></script>

		<script src="{{ URL::asset('public/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/waypoints.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery.easypiechart.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/classie.js') }}"></script>

		<!--Switcher-->
		<script src="{{ URL::asset('public/assets/switcher/js/switcher.js') }}"></script>
		<!--Owl Carousel-->
		<script src="{{ URL::asset('public/assets/owl-carousel/owl.carousel.min.js') }}"></script>
		<!--bxSlider-->
		<script src="{{ URL::asset('public/assets/bxslider/jquery.bxslider.js') }}"></script>
		<!-- jQuery UI Slider -->
		<script src="{{ URL::asset('public/assets/slider/jquery.ui-slider.js') }}"></script>

		<!--Theme-->
		<script src="{{ URL::asset('public/js/jquery.smooth-scroll.js') }}"></script>
		<script src="{{ URL::asset('public/js/wow.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/jquery.placeholder.min.js') }}"></script>
		<script src="{{ URL::asset('public/js/theme.js') }}"></script>

		@yield('datepicker_js')

		@yield('custom_script')
	</body>
</html>