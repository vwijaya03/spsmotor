@extends('front-end/header')

@section('content')
<style type="text/css">
	#map {
        height: 100%;
        min-height: 250px;
      }
</style>

<section class="b-pageHeader">
	<div class="container">
		<h1 class="wow zoomInLeft" data-wow-delay="0s">Cabang {{$cabang->name}}</h1>
		<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0s">
			<h3>SPS Motor Dealer Honda Resmi</h3>
		</div>
	</div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs">
	<div class="container">
		<a href="{{ url('/') }}" class="b-breadCumbs__page">Beranda</a><span class="fa fa-angle-right"></span><a href="{{ url('/cabang/'.$cabang->slug) }}" class="b-breadCumbs__page m-active">Cabang {{$cabang->name}}</a>
	</div>
</div><!--b-breadCumbs-->

<section class="b-best">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-xs-12">
				<div class="b-best__info" style="color: white;">
					<header class="s-lineDownLeft b-best__info-head">
						<h2 class="zoomInUp" data-wow-delay="0s">{{$cabang->name}}</h2>
					</header>
					<?php echo($cabang->description); ?>
					<br>
					<?php echo($cabang->informasi_hari_dan_jam); ?>

					<div class="b-footer__content-social">
						<a href="{{ $cabang->fb_url }}" target="_blank"><span class="fa fa-facebook-square"></span></a>
						<a href="{{ $cabang->twitter_url }}" target="_blank"><span class="fa fa-twitter-square"></span></a>
						<a href="{{ $cabang->ig_url }}" target="_blank"><span class="fa fa-instagram"></span></a>
					</div>
				</div>
			</div>
			
			<div class="col-sm-4 col-xs-12">
				<img class="img-responsive center-block zoomInUp" alt="{{$cabang->name}}" src="{{ URL::asset($cabang->img_path) }}" />
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="b-best__info">
					<header class="s-lineDownLeft b-best__info-head">
						<h2 class="zoomInUp" data-wow-delay="0s">Lokasi Kami</h2>
					</header>
					<div id="map"></div>
				</div>
			</div>
		</div>
	</div>
</section><!--b-best-->

@endsection

@section('google_map_script')

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDbhgNiMBQ7soSSGOMxcMOqN1rBPIOs8k&callback=initMap"></script>
	<script type="text/javascript">
		function initMap() 
		{
			// var myLatLng = {lat: 0, lng: 0.01};
			var myLatLng = {lat: <?php echo($cabang->lat); ?>, lng: <?php echo($cabang->lng); ?>};

			var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 19,
			center: myLatLng
			});

			var marker = new google.maps.Marker({
			position: myLatLng,
			map: map
			});
		}
	</script>

@endsection