<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesanSparepartModel extends Model
{
    protected $table = 'pesan_sparepart';
    protected $primaryKey = 'id';
    protected $fillable = ['cabang_id', 'jenis_kendaraan', 'product_id', 'fullname', 'email', 'no_telepon', 'tahun_rakitan', 'description', 'delete'];
}
