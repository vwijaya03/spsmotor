<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscribeMailModel extends Model
{
    protected $table = 'subscribe_mail';
    protected $primaryKey = 'id';
    protected $fillable = ['email', 'fullname', 'status', 'delete'];
}
