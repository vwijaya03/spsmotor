<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CabangModel extends Model
{
    protected $table = 'cabang';
    protected $primaryKey = 'id';
    protected $fillable = ['category_cabang_id', 'user_id', 'slug', 'email_bengkel_cabang', 'email_pic_cabang', 'email_technical_service_department_cabang', 'email_customer_service_cabang', 'fb_url', 'twitter_url', 'ig_url', 'name', 'description', 'img_path', 'address', 'no_telepon', 'informasi_hari_dan_jam', 'date_time', 'lat', 'lng', 'delete'];
}
