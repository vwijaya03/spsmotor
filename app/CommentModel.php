<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    protected $table = 'comment';
    protected $primaryKey = 'id';
    protected $fillable = ['tipe', 'product_or_news_id', 'comment_fullname', 'comment_email', 'comment_description', 'approval', 'date_time', 'role', 'delete'];
}
