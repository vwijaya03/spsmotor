<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $fillable = ['category_id', 'user_id', 'slug', 'name', 'description', 'price', 'thumbnail', 'tipe_mesin', 'volume_langkah', 'sistem_pendingin', 'sistem_suplai_bahan_bakar', 'diameter_x_langkah', 'tipe_transmisi', 'rasio_kompresi', 'daya_maksimum', 'torsi_maksimum', 'pola_pengoperan_gigi', 'tipe_starter', 'tipe_kopling', 'kapasitas_minyak_pelumas', 'panjang_lebar_tinggi', 'jarak_sumbu_roda', 'jarak_terendah_ke_tanah', 'curb_weight', 'kapasitas_tangki_bbm', 'tipe_rangka', 'tipe_suspensi_depan', 'tipe_suspensi_belakang', 'ukuran_ban_depan', 'ukuran_ban_belakang', 'tipe_rem_depan', 'tipe_rem_belakang', 'tipe_aki', 'sistem_pengapian', 'tipe_busi', 'date_time', 'delete'];
}
