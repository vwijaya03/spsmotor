<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryCabangModel extends Model
{
    protected $table = 'gallery_cabang';
    protected $primaryKey = 'id';
    protected $fillable = ['cabang_id', 'user_id', 'slug', 'title', 'description', 'img_path', 'date_time', 'delete'];
}
