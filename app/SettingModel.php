<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingModel extends Model
{
    protected $table = 'setting';
    protected $primaryKey = 'id';
    protected $fillable = ['address', 'email', 'no_telepon', 'no_fax', 'informasi_hari_dan_jam', 'lat', 'lng', 'delete'];
}
