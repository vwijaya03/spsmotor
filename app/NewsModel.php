<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsModel extends Model
{
    protected $table = 'news';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'user_id', 'slug', 'title', 'description', 'img_path', 'date_time', 'delete'];
}
