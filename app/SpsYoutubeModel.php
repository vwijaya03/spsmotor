<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpsYoutubeModel extends Model
{
    protected $table = 'sps_youtube';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'title', 'url', 'date_time', 'delete'];
}
