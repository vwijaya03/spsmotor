<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Schema;
use App\SettingModel;
use App\ProductModel;
use App\CategoryProductModel;
use App\CategoryCabangModel;
use App\SpsProfileModel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $url_admin_prefix = '/admin/backend';
        $latest_product = '';
        $category_product = '';
        $category_cabang = '';
        $about = '';
        $css_padding_and_margin = "margin:0; padding:0;list-style: none;";
        
        if(Schema::hasTable('product'))
        {
            // $latest_product = ProductModel::select('product_image.path', 'category_product.name as category_name', 'product.slug', 'product.name')
            // ->join('category_product', 'category_product.id', '=', 'product.category_id')
            // ->join('product_image', 'product_image.product_id', '=', 'product.id')
            // ->where('product_image.set_image', 1)
            // ->where('product_image.delete', 0)
            // ->where('category_product.delete', 0)
            // ->where('product.delete', 0)
            // ->orderBy('product.date_time', 'desc')
            // ->limit(3)
            // ->get();

            $latest_product = ProductModel::select('product.id', 'product.slug', 'product.name', 'product.description', 'product.thumbnail')
            ->where('product.delete', 0)
            ->orderBy('product.date_time', 'desc')
            ->limit(9)
            ->get();
        }
        
        if(Schema::hasTable('category_product'))
        {
            $category_product = CategoryProductModel::select('id', 'slug', 'name')->where('delete', 0)->get();
        }
        
        if(Schema::hasTable('category_cabang'))
        {
            $category_cabang = CategoryCabangModel::select('id', 'slug', 'name')->where('delete', 0)->get();
        }
        
        if(Schema::hasTable('sps_profile'))
        {
            $about = SpsProfileModel::select('description')->where('delete', 0)->first();
        }
        

        View::share(['url_admin_prefix' => $url_admin_prefix, 'latest_product' => $latest_product, 'category_product' => $category_product, 'category_cabang' => $category_cabang, 'about' => $about, 'css_padding_and_margin' => $css_padding_and_margin]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
