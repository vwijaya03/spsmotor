<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KritikDanSaranModel extends Model
{
    protected $table = 'kritik_dan_saran';
    protected $primaryKey = 'id';
    protected $fillable = ['cabang_id', 'fullname', 'email', 'no_telepon', 'pesan', 'date_time', 'delete'];
}
