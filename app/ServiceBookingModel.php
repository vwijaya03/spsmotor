<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBookingModel extends Model
{
    protected $table = 'service_booking';
    protected $primaryKey = 'id';
    protected $fillable = ['cabang_id', 'fullname', 'email', 'no_polisi', 'no_telepon', 'jenis_kendaraan', 'jenis_service', 'tanggal_booking', 'jam_booking', 'description', 'delete'];
}
