<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryCabangModel extends Model
{
    protected $table = 'category_cabang';
    protected $primaryKey = 'id';
    protected $fillable = ['slug', 'name', 'delete'];
}
