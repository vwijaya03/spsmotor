<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KarirModel extends Model
{
    protected $table = 'karir';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'description', 'path', 'date_time', 'delete'];
}
