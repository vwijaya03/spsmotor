<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProductModel extends Model
{
    protected $table = 'category_product';
    protected $primaryKey = 'id';
    protected $fillable = ['slug', 'name', 'delete'];
}
