<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackgroundImagesModel extends Model
{
    protected $table = 'background_images';
    protected $primaryKey = 'id';
    protected $fillable = ['path', 'tipe', 'delete'];
}
