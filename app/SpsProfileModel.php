<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpsProfileModel extends Model
{
    protected $table = 'sps_profile';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'description', 'date_time', 'delete'];
}
