<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddHubungiKamiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cabang_id' => 'required', 
            'fullname' => 'required',
            'email' => 'required',
            'no_telepon' => 'required',
            'description' => 'required'
        ];
    }
}
