<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCabangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_cabang_id' => 'required',
            'email_bengkel_cabang' => 'required',
            'email_pic_cabang' => 'required',
            'email_technical_service_department_cabang' => 'required',
            'email_customer_service_cabang' => 'required',
            'fb_url' => 'required',
            'twitter_url' => 'required',
            'ig_url' => 'required',
            'name' => 'required',
            'description' => 'required',
            'img' => '',
            'address' => 'required',
            'no_telepon' => 'required',
            'informasi_hari_dan_jam' => 'required',
            'lat' => 'required',
            'lng' => 'required'
        ];
    }
}
