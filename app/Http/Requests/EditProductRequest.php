<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'price' => '',
            'thumbnail' => '',
            'tipe_mesin' => '', 
            'volume_langkah' => '', 
            'sistem_pendingin' => '', 
            'sistem_suplai_bahan_bakar' => '', 
            'diameter_x_langkah' => '', 
            'tipe_transmisi' => '', 
            'rasio_kompresi' => '', 
            'daya_maksimum' => '', 
            'torsi_maksimum' => '', 
            'pola_pengoperan_gigi' => '', 
            'tipe_starter' => '', 
            'tipe_kopling' => '', 
            'kapasitas_minyak_pelumas' => '', 
            'panjang_lebar_tinggi' => '', 
            'jarak_sumbu_roda' => '', 
            'jarak_terendah_ke_tanah' => '', 
            'curb_weight' => '', 
            'kapasitas_tangki_bbm' => '', 
            'tipe_rangka' => '', 
            'tipe_suspensi_depan' => '', 
            'tipe_suspensi_belakang' => '', 
            'ukuran_ban_depan' => '', 
            'ukuran_ban_belakang' => '', 
            'tipe_rem_depan' => '', 
            'tipe_rem_belakang' => '', 
            'tipe_aki' => '', 
            'sistem_pengapian' => '', 
            'tipe_busi' => ''
        ];
    }
}
