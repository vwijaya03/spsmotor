<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPesanSparepartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cabang_id' => 'required',
            'jenis_kendaraan' => 'required',
            'product_id' => 'required',
            'fullname' => 'required',
            'email' => '',
            'no_telepon' => 'required',
            'tahun_rakitan' => 'required',
            'description' => 'required'
        ];
    }
}
