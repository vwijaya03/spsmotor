<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddCommentNewsRequest;
use App\Http\Requests\AddCommentProductRequest;
use App\Http\Requests\AddHubungiKamiRequest;
use App\Http\Requests\AddFrontEndBookingServiceRequest;
use App\Http\Requests\AddSubscribeRequest;
use App\Http\Requests\AddPesanSparepartRequest;
use App\Http\Requests\PostSearchBrosurRequest;
use App\Http\Requests\PostSearchKatalogRequest;
use App\CabangModel;
use App\CategoryCabangModel;
use App\CategoryProductModel;
use App\ProductModel;
use App\ProductImageModel;
use App\CommentModel;
use App\DownloadBrosurDanSukuCadangModel;
use App\KarirModel;
use App\GalleryCabangModel;
use App\NewsModel;
use App\KritikDanSaranModel;
use App\ServiceBookingModel;
use App\SettingModel;
use App\SpsProfileModel;
use App\SpsYoutubeModel;
use App\SubscribeMailModel;
use App\PesanSparepartModel;
use App\BackgroundImagesModel;
use Auth, Hash, DB, Log;

class FrontEndController extends Controller
{
    private $bgLayananSPSMotorHome = 'bg-layanan-spsmotor-home';
    private $bgBeritaTerbaruHome = 'bg-berita-terbaru-home';
    private $bgLanggananBeritaHome = 'bg-langganan-berita-home';
    private $bgPopupHome = 'bg-pop-up-home';
    private $bgHeaderSejarah = 'bg-header-sejarah';
    private $bgContentSejarah = 'bg-content-sejarah';
    private $bgGalleryFotoCabang = 'bg-gallery-foto-cabang';
    private $bgDetailFotoCabang = 'bg-detail-foto-cabang';
    private $bgSpsYoutube = 'bg-sps-youtube';
    private $bgJaringan = 'bg-jaringan';
    private $bgDetailJaringan = 'bg-detail-jaringan';
    private $bgHeaderProduk = 'bg-header-produk';
    private $bgProduk = 'bg-produk';
    private $bgDetailProduk = 'bg-detail-produk';
    private $bgHeaderBerita = 'bg-header-berita';
    private $bgBerita = 'bg-berita';
    private $bgDetailBerita = 'bg-detail-berita';
    private $bgDownload = 'bg-download';

    public function getHome()
    {
    	$product = ProductModel::select('category_product.name as category_name', 'product.id', 'product.name', 'product.slug', 'product.description', 'product_image.path', 'product_image.updated_at')
    	->join('product_image', 'product_image.product_id', '=', 'product.id')
    	->join('category_product', 'category_product.id', '=', 'product.category_id')
    	->where('product.delete', 0)
    	->where('product_image.set_image', 1)
    	->where('product_image.delete', 0)
        ->orderBy('product_image.updated_at', 'desc')
        ->limit(5)
    	->get();

    	$news = NewsModel::select('slug', 'title', 'description', 'img_path', 'created_at')
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->limit(6)
        ->get();

        $bgLayananSPSMotorHome = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgLayananSPSMotorHome)->where('delete', 0)->first();

        $bgBeritaTerbaruHome = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgBeritaTerbaruHome)->where('delete', 0)->first();

        $bgLanggananBeritaHome = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgLanggananBeritaHome)->where('delete', 0)->first();

        $bgPopupHome = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgPopupHome)->where('delete', 0)->first();

        // return view('front-end/home', ['product' => $product, 'news' => $news, 'mListTable' => 0]);
    	return view('v2/home', ['product' => $product, 'news' => $news, 'bgLayananSPSMotorHome' => $bgLayananSPSMotorHome, 'bgBeritaTerbaruHome' => $bgBeritaTerbaruHome, 'bgLanggananBeritaHome' => $bgLanggananBeritaHome, 'bgPopupHome' => $bgPopupHome]);
    }

    public function getTentangKami()
    {
        $profile = SpsProfileModel::select('description')->where('delete', 0)->first();

        $bgHeaderSejarah = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderSejarah)->where('delete', 0)->first();

        $bgContentSejarah = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgContentSejarah)->where('delete', 0)->first();

        // return view('front-end/about', ['profile' => $profile, 'mListTable' => 0]);
        return view('v2/about', ['profile' => $profile, 'bgHeaderSejarah' => $bgHeaderSejarah, 'bgContentSejarah' => $bgContentSejarah]);
    }

    public function getTentangKamiApi()
    {
        $profile = SpsProfileModel::select('description')->where('delete', 0)->first();

        return view('api/profile', ['profile' => $profile, 'mListTable' => 0]);
    }

    public function getGalleryCabangFrontEnd()
    {
        $cabang = CabangModel::select('id', 'slug', 'name', 'description', 'img_path', 'fb_url', 'twitter_url', 'ig_url', 'created_at')->where('delete', 0)->get();

        $bgGalleryFotoCabang = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgGalleryFotoCabang)->where('delete', 0)->first();

        // return view('front-end/gallery-cabang', ['mListTable' => 1, 'cabang' => $cabang]);
        return view('v2/gallery-cabang', ['cabang' => $cabang, 'bgGalleryFotoCabang' => $bgGalleryFotoCabang]);
    }

    public function getGalleryCabangApi()
    {
        $cabang = CabangModel::select('id', 'slug', 'name', 'description', 'img_path')->where('delete', 0)->get();

        return view('api/gallery-cabang', ['mListTable' => 1, 'cabang' => $cabang]);
    }

    public function getDetailGalleryCabangFrontEnd($slug)
    {
        $cabang = CabangModel::select('id', 'slug', 'name')->where('slug', $slug)->where('delete', 0)->first();

        if($cabang == null)
        {
            return redirect()->route('getGalleryCabangFrontEnd');
        }

        $bgDetailFotoCabang = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailFotoCabang)->where('delete', 0)->first();

        $gallery_cabang = GalleryCabangModel::select('title', 'description', 'img_path', 'created_at')->where('cabang_id', $cabang->id)->where('delete', 0)->paginate(6);

        // return view('front-end/detail-gallery-cabang', ['gallery_cabang' => $gallery_cabang, 'cabang' => $cabang, 'mListTable' => 1]);
        return view('v2/detail-gallery-cabang', ['gallery_cabang' => $gallery_cabang, 'cabang' => $cabang, 'bgDetailFotoCabang' => $bgDetailFotoCabang]);
    }

    public function getDetailGalleryCabangApi($slug)
    {
        $cabang = CabangModel::select('id', 'slug', 'name')->where('slug', $slug)->where('delete', 0)->first();

        if($cabang == null)
        {
            return redirect()->route('getGalleryCabangFrontEnd');
        }

        $gallery_cabang = GalleryCabangModel::select('title', 'description', 'img_path')->where('cabang_id', $cabang->id)->where('delete', 0)->get();

        return view('api/detail-gallery-cabang', ['gallery_cabang' => $gallery_cabang, 'cabang' => $cabang, 'mListTable' => 1]);
    }

    public function getSpsYoutube()
    {
        $sps_youtube = SpsYoutubeModel::select('id', 'title', 'url')
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->paginate(8);

        $bgSpsYoutube = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgSpsYoutube)->where('delete', 0)->first();

        // return view('front-end/sps-youtube', ['sps_youtube' => $sps_youtube, 'mListTable' => 1]);
        return view('v2/sps-youtube', ['sps_youtube' => $sps_youtube, 'mListTable' => 1, 'bgSpsYoutube' => $bgSpsYoutube]);
    }

    public function getDetailJaringan($slug)
    {
        $content_category_cabang = CategoryCabangModel::select('id', 'slug', 'name')->where('slug', $slug)->where('delete', 0)->first();

        if($content_category_cabang == null)
        {
            return redirect()->route('getHome');
        }

        $bgJaringan = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgJaringan)->where('delete', 0)->first();

        $content_cabang = CabangModel::select('id', 'slug', 'name', 'description', 'img_path', 'created_at')->where('category_cabang_id', $content_category_cabang->id)->where('delete', 0)->get();

        // return view('front-end/detail-jaringan', ['content_category_cabang' => $content_category_cabang, 'content_cabang' => $content_cabang, 'mListTable' => 1]);
        return view('v2/detail-jaringan', ['content_category_cabang' => $content_category_cabang, 'content_cabang' => $content_cabang, 'bgJaringan' => $bgJaringan]);
    }

    public function getCabang()
    {
        $cabang = CabangModel::select('id', 'slug', 'name', 'description', 'img_path', 'lat', 'lng')->where('delete', 0)->get();

        return view('front-end/cabang', ['mListTable' => 1, 'cabang' => $cabang]);
    }

    public function getDetailCabang($slug)
    {
        $cabang = CabangModel::select('id', 'slug', 'name', 'description', 'informasi_hari_dan_jam', 'img_path', 'lat', 'lng', 'fb_url', 'twitter_url', 'ig_url')->where('slug', $slug)->where('delete', 0)->first();

        if($cabang == null)
        {
            return redirect()->route('getCabang');
        }

        $bgDetailJaringan = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailJaringan)->where('delete', 0)->first();
        // return view('front-end/detail-cabang', ['mListTable' => 0, 'cabang' => $cabang]);
        return view('v2/detail-cabang', ['mListTable' => 0, 'cabang' => $cabang, 'bgDetailJaringan' => $bgDetailJaringan]);
    }

    public function getKategoriProduk($slug)
    {
        $category_product = CategoryProductModel::select('id', 'name', 'slug')->where('slug', $slug)->where('delete', 0)->first();

        if($category_product == null)
        {
            return redirect()->route('getHome');
        }

        $product = ProductModel::select('product.id', 'product.slug', 'product.name', 'product.description', 'product.thumbnail')
        ->where('product.category_id', $category_product->id)
        ->where('product.delete', 0)
        ->get();

        $bgProduk = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgProduk)->where('delete', 0)->first();

        $bgHeaderProduk = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderProduk)->where('delete', 0)->first();
        // return view('front-end/product-by-category', ['mListTable' => 1, 'content_product' => $product, 'content_category_product' => $category_product]);
        return view('v2/product-by-category', ['mListTable' => 1, 'content_product' => $product, 'content_category_product' => $category_product, 'bgProduk' => $bgProduk, 'bgHeaderProduk' => $bgHeaderProduk]);
    }

    public function getKategoriProdukApi($id)
    {
        $category_product = CategoryProductModel::select('id', 'name', 'slug')->where('id', $id)->where('delete', 0)->first();

        if($category_product == null)
        {
            return redirect()->route('getHome');
        }

        $product = ProductModel::select('product_image.path', 'product.id', 'product.slug', 'product.name', 'product.description')
        ->join('product_image', 'product_image.product_id', '=', 'product.id')
        ->where('product.category_id', $category_product->id)
        ->where('product_image.set_image', 1)
        ->where('product.delete', 0)
        ->where('product_image.delete', 0)
        ->get();

        return view('api/product-by-category', ['mListTable' => 1, 'content_product' => $product, 'content_category_product' => $category_product]);
    }

    public function getProduk()
    {
        $product = ProductModel::select('product_image.path', 'product.id', 'product.slug', 'product.name', 'product.description')
        ->join('product_image', 'product_image.product_id', '=', 'product.id')
        ->where('product_image.set_image', 1)
        ->where('product.delete', 0)
        ->where('product_image.delete', 0)
        ->get();

        return view('front-end/product', ['mListTable' => 1, 'content_product' => $product]);
    }

    public function getProdukDetail($slug)
    {
        $product = ProductModel::select('id', 'slug', 'name', 'description', 'price', 'description', 'tipe_mesin', 'volume_langkah', 'sistem_pendingin', 'sistem_suplai_bahan_bakar', 'diameter_x_langkah', 'tipe_transmisi', 'rasio_kompresi', 'daya_maksimum', 'torsi_maksimum', 'pola_pengoperan_gigi', 'tipe_starter', 'tipe_kopling', 'kapasitas_minyak_pelumas', 'panjang_lebar_tinggi', 'jarak_sumbu_roda', 'jarak_terendah_ke_tanah', 'curb_weight', 'kapasitas_tangki_bbm', 'tipe_rangka', 'tipe_suspensi_depan', 'tipe_suspensi_belakang', 'ukuran_ban_depan', 'ukuran_ban_belakang', 'tipe_rem_depan', 'tipe_rem_belakang', 'tipe_aki', 'sistem_pengapian', 'tipe_busi')->where('slug', $slug)->where('delete', 0)->first();

        if($product == null)
        {
            return redirect()->route('getProduk');
        }

        $product_image = ProductImageModel::select('path')->where('product_id', $product->id)->where('delete', 0)->get();

        $comment = CommentModel::select('comment_fullname', 'comment_email', 'comment_description', 'date_time', 'role')
        ->where('product_or_news_id', $product->id)
        ->where('tipe', 'product')
        ->where('approval', 1)
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->get();

        $bgDetailProduk = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailProduk)->where('delete', 0)->first();

        $bgHeaderProduk = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderProduk)->where('delete', 0)->first();
        // return view('front-end/detail_product', ['mListTable' => 0, 'content_product' => $product, 'content_product_image' => $product_image, 'comment' => $comment]);
        return view('v2/detail_product', ['mListTable' => 0, 'content_product' => $product, 'content_product_image' => $product_image, 'comment' => $comment, 'bgDetailProduk' => $bgDetailProduk, 'bgHeaderProduk' => $bgHeaderProduk]);
    }

    public function getProdukDetailApi($slug)
    {
        $product = ProductModel::select('id', 'slug', 'name', 'description', 'price', 'tipe_mesin', 'volume_langkah', 'sistem_pendingin', 'sistem_suplai_bahan_bakar', 'diameter_x_langkah', 'tipe_transmisi', 'rasio_kompresi', 'daya_maksimum', 'torsi_maksimum', 'pola_pengoperan_gigi', 'tipe_starter', 'tipe_kopling', 'kapasitas_minyak_pelumas', 'panjang_lebar_tinggi', 'jarak_sumbu_roda', 'jarak_terendah_ke_tanah', 'curb_weight', 'kapasitas_tangki_bbm', 'tipe_rangka', 'tipe_suspensi_depan', 'tipe_suspensi_belakang', 'ukuran_ban_depan', 'ukuran_ban_belakang', 'tipe_rem_depan', 'tipe_rem_belakang', 'tipe_aki', 'sistem_pengapian', 'tipe_busi')->where('slug', $slug)->where('delete', 0)->first();

        if($product == null)
        {
            return redirect()->route('getProduk');
        }

        $product_image = ProductImageModel::select('path')->where('product_id', $product->id)->where('delete', 0)->get();

        $comment = CommentModel::select('comment_fullname', 'comment_email', 'comment_description', 'date_time')
        ->where('product_or_news_id', $product->id)
        ->where('tipe', 'product')
        ->where('approval', 1)
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->get();

        return view('api/detail_product', ['mListTable' => 0, 'content_product' => $product, 'content_product_image' => $product_image, 'comment' => $comment]);
    }

    public function getBrosur()
    {
        $title = '';
        $brosur = DownloadBrosurDanSukuCadangModel::select('id', 'title', 'path', 'created_at')->where('tipe', 0)->where('delete', 0)->paginate(8);

        $bgDownload = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDownload)->where('delete', 0)->first();
        // return view('front-end/brosur', ['mListTable' => 1, 'brosur' => $brosur, 'title' => $title]);
        return view('v2/brosur', ['mListTable' => 1, 'brosur' => $brosur, 'title' => $title, 'bgDownload' => $bgDownload]);
    }

    public function getCariBrosur(Request $request)
    {
        $title = $request->get('search_brosur');

        if($title == null)
        {
            return redirect()->route('getBrosur');
        }

        return redirect()->route('getCariBrosurByTitle', ['title' => $title]);
    }

    public function getCariBrosurByTitle(Request $request, $title)
    {
        $brosur = DownloadBrosurDanSukuCadangModel::select('id', 'title', 'path', 'created_at')
        ->where('title', 'like', '%'.$title.'%')
        ->where('tipe', 0)
        ->where('delete', 0)
        ->paginate(8);

        $bgDownload = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDownload)->where('delete', 0)->first();
        // return view('front-end/brosur', ['mListTable' => 1, 'brosur' => $brosur, 'title' => $title]);
        return view('v2/brosur', ['mListTable' => 1, 'brosur' => $brosur, 'title' => $title, 'bgDownload' => $bgDownload]);
    }

    public function getKatalog()
    {
        $katalog = DownloadBrosurDanSukuCadangModel::select('id', 'title', 'path', 'created_at')->where('tipe', 1)->where('delete', 0)->paginate(8);

        $bgDownload = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDownload)->where('delete', 0)->first();
        // return view('front-end/katalog', ['mListTable' => 1, 'katalog' => $katalog]);
        return view('v2/katalog', ['mListTable' => 1, 'katalog' => $katalog, 'bgDownload' => $bgDownload]);
    }

    public function getCariKatalog(Request $request)
    {
        $title = $request->get('search_katalog');

        if($title == null)
        {
            return redirect()->route('getKatalog');
        }

        return redirect()->route('getCariKatalogByTitle', ['title' => $title]);
    }

    public function getCariKatalogByTitle(Request $request, $title)
    {
        $katalog = DownloadBrosurDanSukuCadangModel::select('id', 'title', 'path', 'created_at')
        ->where('title', 'like', '%'.$title.'%')
        ->where('tipe', 1)
        ->where('delete', 0)
        ->paginate(8);

        $bgDownload = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDownload)->where('delete', 0)->first();
        // return view('front-end/katalog', ['mListTable' => 1, 'katalog' => $katalog, 'title' => $title]);
        return view('v2/katalog', ['mListTable' => 1, 'katalog' => $katalog, 'bgDownload' => $bgDownload]);
    }

    public function getLayananKarir()
    {
        $karir = KarirModel::select('description')->where('delete', 0)->first();

        // return view('front-end/karir', ['mListTable' => 0, 'karir' => $karir]);
        return view('v2/karir', ['mListTable' => 0, 'karir' => $karir]);
    }

    public function postCommentProduct(AddCommentProductRequest $request)
    {
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $description = $request->get('description');
        $product_id = $request->get('product_id');

        $slug = $request->get('product_slug');

        CommentModel::create([
            'tipe' => 'product',
            'product_or_news_id' => $product_id,
            'comment_fullname' => $fullname,
            'comment_email' => $email,
            'comment_description' => $description,
            'date_time' => date('Y-m-d H:i:s'),
            'role' => 'anonymous',
            'approval' => 0,
            'delete' => 0
        ]);

        return redirect()->route('getProdukDetail', ['slug' => $slug])->with(['done' => 'Terima kasih, komentar anda sedang di review.'] );
    }

    public function getNews()
    {
        $news = NewsModel::select('id', 'slug', 'title', 'description', 'img_path', 'date_time')
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->paginate(9);

        $bgHeaderBerita = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderBerita)->where('delete', 0)->first();

        $bgBerita = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgBerita)->where('delete', 0)->first();
        // return view('front-end/news', ['mListTable' => 1, 'news' => $news]);
        return view('v2/news', ['mListTable' => 1, 'news' => $news, 'bgHeaderBerita' => $bgHeaderBerita, 'bgBerita' => $bgBerita]);
    }

    public function getNewsDetail($slug)
    {
        $news = NewsModel::select('id', 'slug', 'title', 'description', 'img_path', 'date_time')
        ->where('slug', $slug)
        ->where('delete', 0)
        ->first();

        if($news == null)
        {
            return redirect()->route('getNews');
        }

        $bgDetailBerita = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailBerita)->where('delete', 0)->first();
        // return view('front-end/detail_news', ['mListTable' => 0, 'news' => $news]);
        return view('v2/detail_news', ['mListTable' => 0, 'news' => $news, 'bgDetailBerita' => $bgDetailBerita]);
    }

    public function postCommentNews(AddCommentNewsRequest $request)
    {
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $description = $request->get('description');
        $news_id = $request->get('news_id');

        CommentModel::create([
            'tipe' => 'product',
            'product_or_news_id' => $product_id,
            'comment_fullname' => $fullname,
            'comment_email' => $email,
            'comment_description' => $description,
            'date_time' => date('Y-m-d H:i:s'),
            'role' => 'anonymous',
            'approval' => 0,
            'delete' => 0
        ]);
    }

    public function getHubungiKami()
    {
        $cabang = CabangModel::select('id', 'slug', 'name')->where('delete', 0)->get();

        $default_cabang = CabangModel::select('id', 'name', 'email_customer_service_cabang', 'address', 'no_telepon', 'informasi_hari_dan_jam', 'lat', 'lng')->where('id', 1)->where('delete', 0)->first();

        if($default_cabang == null)
            $default_cabang == null;
        
        // return view('front-end/hubungi-kami', ['mListTable' => 0, 'cabang' => $cabang, 'default_cabang' => $default_cabang]);
        return view('v2/hubungi-kami', ['mListTable' => 0, 'cabang' => $cabang, 'default_cabang' => $default_cabang]);
    }

    public function getCabangHubungiKami($slug)
    {
        $cabang = CabangModel::select('id', 'slug', 'name')->where('delete', 0)->get();

        $selected_cabang = CabangModel::select('id', 'name', 'email_customer_service_cabang', 'address', 'no_telepon', 'informasi_hari_dan_jam', 'lat', 'lng')->where('slug', $slug)->where('delete', 0)->first();

        if($selected_cabang == null)
        {
            return redirect()->route('getHubungiKami');
        }

        // return view('front-end/hubungi-kami', ['mListTable' => 0, 'cabang' => $cabang, 'default_cabang' => $selected_cabang]);
        return view('v2/hubungi-kami', ['mListTable' => 0, 'cabang' => $cabang, 'default_cabang' => $selected_cabang]);
    }

    public function getCabangHubungiKamiApi($slug)
    {
        $cabang = CabangModel::select('id', 'slug', 'name')->where('delete', 0)->get();

        $selected_cabang = CabangModel::select('id', 'name', 'email_customer_service_cabang', 'address', 'no_telepon', 'informasi_hari_dan_jam', 'lat', 'lng')->where('slug', $slug)->where('delete', 0)->first();

        if($selected_cabang == null)
        {
            return redirect()->route('getHubungiKami');
        }

        return view('api/hubungi-kami', ['mListTable' => 0, 'cabang' => $cabang, 'default_cabang' => $selected_cabang]);
    }

    public function getHubungiKamiApi()
    {
        $cabang = CabangModel::select('id', 'slug', 'name')->where('delete', 0)->get();

        $default_cabang = CabangModel::select('id', 'name', 'email_customer_service_cabang', 'address', 'no_telepon', 'informasi_hari_dan_jam', 'lat', 'lng')->where('id', 1)->where('delete', 0)->first();

        return view('api/hubungi-kami', ['mListTable' => 0, 'cabang' => $cabang, 'default_cabang' => $default_cabang]);
    }

    public function postHubungiKamiApi(AddHubungiKamiRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $description = $request->get('description');

        KritikDanSaranModel::create([
            'cabang_id' => $cabang_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'pesan' => $description,
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        $data = CabangModel::select('name', 'email_customer_service_cabang', 'email_pic_cabang')->where('id', $cabang_id)->where('delete', 0)->first();

        $sender = "vwijaya08@gmail.com";

        $subject = "Kritik / Saran Dari Customer";

        $htmlContent = '
            <html>
            <head>
                <title>Kritik / Saran Dari Customer</title>
            </head>
            <body>
                <h1>Kritik / Saran Dari Customer Untuk '.$data->name.'</h1>
                <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                    <tr>
                        <th>Nama Lengkap:</th><td>'.$fullname.'</td>
                    </tr>
                    <tr>
                        <th>Email Pelanggan:</th><td>'.$email.'</td>
                    </tr>
                    <tr>
                        <th>No. Telepon Pelanggan:</th><td>'.$no_telepon.'</td>
                    </tr>
                    <tr style="background-color: #e0e0e0;">
                        <th>Kritik / Saran:</th><td>'.$description.'</td>
                    </tr>
                </table>
            </body>
            </html>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: SPS Motor <'.$sender.'>' . "\r\n";
        $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

        $check_mail_cs = mail($data->email_customer_service_cabang, 'Kritik / Saran Dari Customer', $htmlContent, $headers);

        $check_mail_pic = mail($data->email_pic_cabang, 'Kritik / Saran Dari Customer', $htmlContent, $headers);

        if($check_mail_cs && $check_mail_pic)
            $msg = 'Kritik / Saran Anda Berhasil Di Kirim.';
        else
            $msg = 'Gagal';

        return redirect()->route('getHubungiKamiApi')->with(['done' => $msg] );
    }

    public function postHubungiKami(AddHubungiKamiRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $description = $request->get('description');

        KritikDanSaranModel::create([
            'cabang_id' => $cabang_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'pesan' => $description,
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        $data = CabangModel::select('name', 'email_customer_service_cabang', 'email_pic_cabang')->where('id', $cabang_id)->where('delete', 0)->first();

        $sender = "vwijaya08@gmail.com";

        $subject = "Kritik / Saran Dari Customer";

        $htmlContent = '
            <html>
            <head>
                <title>Kritik / Saran Dari Customer</title>
            </head>
            <body>
                <h1>Kritik / Saran Dari Customer Untuk '.$data->name.'</h1>
                <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                    <tr>
                        <th>Nama Lengkap:</th><td>'.$fullname.'</td>
                    </tr>
                    <tr>
                        <th>Email Pelanggan:</th><td>'.$email.'</td>
                    </tr>
                    <tr>
                        <th>No. Telepon Pelanggan:</th><td>'.$no_telepon.'</td>
                    </tr>
                    <tr style="background-color: #e0e0e0;">
                        <th>Kritik / Saran:</th><td>'.$description.'</td>
                    </tr>
                </table>
            </body>
            </html>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: SPS Motor <'.$sender.'>' . "\r\n";
        $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

        $check_mail_cs = mail($data->email_customer_service_cabang, 'Kritik / Saran Dari Customer', $htmlContent, $headers);

        $check_mail_pic = mail($data->email_pic_cabang, 'Kritik / Saran Dari Customer', $htmlContent, $headers);

        if($check_mail_cs && $check_mail_pic)
            $msg = 'Kritik / Saran Anda Berhasil Di Kirim.';
        else
            $msg = 'Gagal';

        return redirect()->route('getHubungiKami')->with(['done' => $msg] );
    }

    public function getSubscribeSuccess()
    {
        // return view('front-end/subscribe-success', ['mListTable' => 0]);
        return view('v2/subscribe-success', ['mListTable' => 0]);
    }

    public function postSubscribe(AddSubscribeRequest $request)
    {
        $fullname = $request->get('fullname');
        $email = $request->get('email');

        // 0 Pending
        // 1 Approved
        // 2 Deleted
        SubscribeMailModel::create([
            'email' => $email,
            'fullname' => $fullname,
            'status' => 1,
            'delete' => 0
        ]);

        return redirect()->route('getSubscribeSuccess')->with(['done' => 'Terima kasih, langganan berita berhasil di daftarkan.']);
    }

    public function getLayananServiceBooking()
    {
        $cabang = CabangModel::select('id', 'name')->where('delete', 0)->get();

        // return view('front-end/layanan-service-booking', ['mListTable' => 0, 'cabang' => $cabang]);
        return view('v2/layanan-service-booking', ['mListTable' => 0, 'cabang' => $cabang]);
    }

    public function getLayananServiceBookingApi()
    {
        $cabang = CabangModel::select('id', 'name')->where('delete', 0)->get();

        return view('api/layanan-service-booking', ['mListTable' => 0, 'cabang' => $cabang]);
    }

    public function postLayananServiceBooking(AddFrontEndBookingServiceRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $no_polisi = $request->get('no_polisi');
        $jenis_kendaraan = $request->get('jenis_kendaraan');
        $jenis_service = $request->get('jenis_service');
        $tanggal_booking = $request->get('tanggal_booking');
        $jam_booking = $request->get('jam_booking');
        $description = $request->get('description');

        $formatedTanggalBooking = date("Y-m-d", strtotime($tanggal_booking));

        ServiceBookingModel::create([
            'cabang_id' => $cabang_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'no_polisi' => $no_polisi,
            'jenis_kendaraan' => $jenis_kendaraan,
            'jenis_service' => $jenis_service,
            'tanggal_booking' => $formatedTanggalBooking,
            'jam_booking' => $jam_booking,
            'description' => $description,
            'delete' => 0
        ]);

        $data = CabangModel::select('name', 'email_bengkel_cabang')->where('id', $cabang_id)->where('delete', 0)->first();
        
        $sender = "vwijaya08@gmail.com";

        $subject = "Service Booking";

        $htmlContent = '
            <html>
            <head>
                <title>Service Booking</title>
            </head>
            <body>
                <h1>Service Booking</h1>
                <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                    <tr>
                        <th>Cabang:</th><td>'.$data->name.'</td>
                    </tr>
                    <tr>
                        <th>Nama Lengkap Pelanggan:</th><td>'.$fullname.'</td>
                    </tr>
                    <tr>
                        <th>Email Pelanggan:</th><td>'.$email.'</td>
                    </tr>
                    <tr>
                        <th>No. Telepon Pelanggan:</th><td>'.$no_telepon.'</td>
                    </tr>
                    <tr>
                        <th>No. Polisi Pelanggan:</th><td>'.$no_polisi.'</td>
                    </tr>
                    <tr>
                        <th>Jenis Kendaraan Pelanggan:</th><td>'.$jenis_kendaraan.'</td>
                    </tr>
                    <tr>
                        <th>Jenis Service:</th><td>'.$jenis_service.'</td>
                    </tr>
                    <tr>
                        <th>Tanggal Booking:</th><td>'.$tanggal_booking.'</td>
                    </tr>
                    <tr>
                        <th>Jam Booking:</th><td>'.$jam_booking.'</td>
                    </tr>
                    <tr style="background-color: #e0e0e0;">
                        <th>Keluhan Sepeda Motor Pelanggan:</th><td>'.$description.'</td>
                    </tr>
                </table>
            </body>
            </html>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: SPS Motor <'.$sender.'>' . "\r\n";
        $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

        $check = mail($data->email_bengkel_cabang, 'Service Booking', $htmlContent, $headers);

        if($check)
            $msg = 'Formulir berhasil dikirim.';
        else
            $msg = 'Gagal';

        return redirect()->route('getLayananServiceBooking')->with(['done' => $msg] );
    }

    public function postLayananServiceBookingApi(AddFrontEndBookingServiceRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $no_polisi = $request->get('no_polisi');
        $jenis_kendaraan = $request->get('jenis_kendaraan');
        $jenis_service = $request->get('jenis_service');
        $tanggal_booking = $request->get('tanggal_booking');
        $jam_booking = $request->get('jam_booking');
        $description = $request->get('description');

        $formatedTanggalBooking = date("Y-m-d", strtotime($tanggal_booking));

        ServiceBookingModel::create([
            'cabang_id' => $cabang_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'no_polisi' => $no_polisi,
            'jenis_kendaraan' => $jenis_kendaraan,
            'jenis_service' => $jenis_service,
            'tanggal_booking' => $formatedTanggalBooking,
            'jam_booking' => $jam_booking,
            'description' => $description,
            'delete' => 0
        ]);

        $data = CabangModel::select('name', 'email_bengkel_cabang')->where('id', $cabang_id)->where('delete', 0)->first();
        
        $sender = "vwijaya08@gmail.com";

        $subject = "Service Booking";

        $htmlContent = '
            <html>
            <head>
                <title>Service Booking</title>
            </head>
            <body>
                <h1>Service Booking</h1>
                <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                    <tr>
                        <th>Cabang:</th><td>'.$data->name.'</td>
                    </tr>
                    <tr>
                        <th>Nama Lengkap Pelanggan:</th><td>'.$fullname.'</td>
                    </tr>
                    <tr>
                        <th>Email Pelanggan:</th><td>'.$email.'</td>
                    </tr>
                    <tr>
                        <th>No. Telepon Pelanggan:</th><td>'.$no_telepon.'</td>
                    </tr>
                    <tr>
                        <th>No. Polisi Pelanggan:</th><td>'.$no_polisi.'</td>
                    </tr>
                    <tr>
                        <th>Jenis Kendaraan Pelanggan:</th><td>'.$jenis_kendaraan.'</td>
                    </tr>
                    <tr>
                        <th>Jenis Service:</th><td>'.$jenis_service.'</td>
                    </tr>
                    <tr>
                        <th>Tanggal Booking:</th><td>'.$tanggal_booking.'</td>
                    </tr>
                    <tr>
                        <th>Jam Booking:</th><td>'.$jam_booking.'</td>
                    </tr>
                    <tr style="background-color: #e0e0e0;">
                        <th>Keluhan Sepeda Motor Pelanggan:</th><td>'.$description.'</td>
                    </tr>
                </table>
            </body>
            </html>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: SPS Motor <'.$sender.'>' . "\r\n";
        $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

        $check = mail($data->email_bengkel_cabang, 'Service Booking', $htmlContent, $headers);

        if($check)
            $msg = 'Formulir berhasil dikirim.';
        else
            $msg = 'Gagal';

        return redirect()->route('getLayananServiceBookingApi')->with(['done' => $msg] );
    }

    public function getPesanSparepart()
    {
        $cabang = CabangModel::select('id', 'name')->where('delete', 0)->get();

        $produk = ProductModel::select('id', 'name')->where('delete', 0)->get();

        // return view('front-end/layanan-pesan-sparepart', ['mListTable' => 0, 'cabang' => $cabang, 'produk' => $produk]);
        return view('v2/layanan-pesan-sparepart', ['mListTable' => 0, 'cabang' => $cabang, 'produk' => $produk]);
    }

    public function postPesanSparepart(AddPesanSparepartRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $jenis_kendaraan = $request->get('jenis_kendaraan');
        $product_id = $request->get('product_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $tahun_rakitan = $request->get('tahun_rakitan');
        $description = $request->get('description');

        PesanSparepartModel::create([
            'cabang_id' => $cabang_id,
            'jenis_kendaraan' => $jenis_kendaraan,
            'product_id' => $product_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'tahun_rakitan' => $tahun_rakitan,
            'description' => $description,
            'delete' => 0
        ]);

        $data = CabangModel::select('name', 'email_technical_service_department_cabang')->where('id', $cabang_id)->where('delete', 0)->first();

        $data_produk = ProductModel::select('name')->where('id', $product_id)->where('delete', 0)->first();
        
        $sender = "vwijaya08@gmail.com";

        $subject = "Service Booking";

        $htmlContent = '
            <html>
            <head>
                <title>Service Booking</title>
            </head>
            <body>
                <h1>Service Booking</h1>
                <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                    <tr>
                        <th>Cabang:</th><td>'.$data->name.'</td>
                    </tr>
                    <tr>
                        <th>Kendaraan Pelanggan:</th><td>'.$data_produk->name.'</td>
                    </tr>
                    <tr>
                        <th>Jenis Kendaraan Pelanggan:</th><td>'.$jenis_kendaraan.'</td>
                    </tr>
                    <tr>
                        <th>Jenis Kendaraan Pelanggan:</th><td>'.$tahun_rakitan.'</td>
                    </tr>
                    <tr>
                        <th>Nama Lengkap Pelanggan:</th><td>'.$fullname.'</td>
                    </tr>
                    <tr>
                        <th>Email Pelanggan:</th><td>'.$email.'</td>
                    </tr>
                    <tr>
                        <th>No. Telepon Pelanggan:</th><td>'.$no_telepon.'</td>
                    </tr>
                    <tr style="background-color: #e0e0e0;">
                        <th>Keterangan:</th><td>'.$description.'</td>
                    </tr>
                </table>
            </body>
            </html>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: SPS Motor <'.$sender.'>' . "\r\n";
        $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

        $check = mail($data->email_technical_service_department_cabang, 'Pemesanan Sparepart', $htmlContent, $headers);

        if($check)
            $msg = 'Formulir berhasil dikirim.';
        else
            $msg = 'Gagal';

        return redirect()->route('getPesanSparepart')->with(['done' => $msg] );
    }

    public function getPesanSparepartApi()
    {
        $cabang = CabangModel::select('id', 'name')->where('delete', 0)->get();

        $produk = ProductModel::select('id', 'name')->where('delete', 0)->get();

        return view('api/layanan-pesan-sparepart', ['mListTable' => 0, 'cabang' => $cabang, 'produk' => $produk]);
    }

    public function postPesanSparepartApi(AddPesanSparepartRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $jenis_kendaraan = $request->get('jenis_kendaraan');
        $product_id = $request->get('product_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $tahun_rakitan = $request->get('tahun_rakitan');
        $description = $request->get('description');

        PesanSparepartModel::create([
            'cabang_id' => $cabang_id,
            'jenis_kendaraan' => $jenis_kendaraan,
            'product_id' => $product_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'tahun_rakitan' => $tahun_rakitan,
            'description' => $description,
            'delete' => 0
        ]);

        $data = CabangModel::select('name', 'email_technical_service_department_cabang')->where('id', $cabang_id)->where('delete', 0)->first();

        $data_produk = ProductModel::select('name')->where('id', $product_id)->where('delete', 0)->first();
        
        $sender = "vwijaya08@gmail.com";

        $subject = "Service Booking";

        $htmlContent = '
            <html>
            <head>
                <title>Service Booking</title>
            </head>
            <body>
                <h1>Service Booking</h1>
                <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                    <tr>
                        <th>Cabang:</th><td>'.$data->name.'</td>
                    </tr>
                    <tr>
                        <th>Kendaraan Pelanggan:</th><td>'.$data_produk->name.'</td>
                    </tr>
                    <tr>
                        <th>Jenis Kendaraan Pelanggan:</th><td>'.$jenis_kendaraan.'</td>
                    </tr>
                    <tr>
                        <th>Jenis Kendaraan Pelanggan:</th><td>'.$tahun_rakitan.'</td>
                    </tr>
                    <tr>
                        <th>Nama Lengkap Pelanggan:</th><td>'.$fullname.'</td>
                    </tr>
                    <tr>
                        <th>Email Pelanggan:</th><td>'.$email.'</td>
                    </tr>
                    <tr>
                        <th>No. Telepon Pelanggan:</th><td>'.$no_telepon.'</td>
                    </tr>
                    <tr style="background-color: #e0e0e0;">
                        <th>Keterangan:</th><td>'.$description.'</td>
                    </tr>
                </table>
            </body>
            </html>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: SPS Motor <'.$sender.'>' . "\r\n";
        $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

        $check = mail($data->email_technical_service_department_cabang, 'Pemesanan Sparepart', $htmlContent, $headers);

        if($check)
            $msg = 'Formulir berhasil dikirim.';
        else
            $msg = 'Gagal';

        return redirect()->route('getPesanSparepartApi')->with(['done' => $msg] );
    }
}







