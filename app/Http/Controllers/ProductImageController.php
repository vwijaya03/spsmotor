<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddGalleryProductRequest;
use App\Http\Requests\EditGalleryProductRequest;
use App\ProductImageModel;
use App\ProductModel;
use Auth, Hash, DB, Log;

class ProductImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaGalleryProduct($product_id)
    {
        $data = ProductImageModel::select('product_image.set_image', 'product.name as product_name', 'product.id as product_id', 'product_image.id', 'product_image.path')
        ->join('product', 'product.id', '=', 'product_image.product_id')
        ->where('product_image.product_id', $product_id)
        ->where('product_image.delete', 0)
        ->where('product.delete', 0)
        ->get();

        if($data == null)
        {
            return redirect()->route('getSemuaProduct');
        }

        $data_header_gallery_product = ProductModel::select('name')->where('id', $product_id)->first();

        return view('admin/gallery-product', ['users' => Auth::user(), 'gallery_product' => $data, 'product_id' => $product_id, 'header' => $data_header_gallery_product]);
    }

    public function getAddGalleryProduct($product_id)
    {
        $product = ProductModel::select('id', 'name')->where('delete', 0)->get();

        return view('admin/add-gallery-product', ['users' => Auth::user(), 'product' => $product, 'product_id' => $product_id]);
    }

    public function postAddGalleryProduct(AddGalleryProductRequest $request)
    {
        $product_id = $request->get('product_id');
        $img = $request->file('img');
        $path = $this->createImage($img);
        
        $data = ProductImageModel::create([
            'product_id' => $product_id,
            'set_image' => 0,
            'path' => $path,
            'delete' => 0
        ]);

        return redirect()->route('getEditGalleryProduct', ['product_id' => $product_id, 'id' => $data->id])->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function getEditGalleryProduct($product_id, $id)
    {
        $image = ProductImageModel::select('id', 'product_id', 'path')->where('id', $id)->where('delete', 0)->first();
        
        if($image == null)
        {
            return redirect()->route('getSemuaGalleryProduct', ['product_id' => $product_id]);
        }

        $selected_product = ProductModel::select('id', 'name')->where('id', $image->product_id)->where('delete', 0)->first();

        $diff_product = ProductModel::select('id', 'name')->where('id', '!=', $image->product_id)->where('delete', 0)->get();

        return view('admin/edit-gallery-product', ['users' => Auth::user(), 'data' => $image, 'selected_product' => $selected_product, 'diff_product' => $diff_product, 'product_id' => $product_id]);
    }

    public function postEditGalleryProduct(EditGalleryProductRequest $request, $product_id, $id)
    {
        $product_id = $request->get('product_id');

        if($request->hasFile('img'))
        {
            $file_img = $request->file('img');
            $img = $this->createImage($file_img);

            $oldData = ProductImageModel::find($id);

            if($oldData->path != null || $oldData->path != null)
            {
                $oldPath = $oldData->path;
                $this->deleteImage($oldPath);
            }

            ProductImageModel::where('id', $id)->where('delete', 0)
            ->update([
                'product_id' => $product_id,
                'path' => $img
            ]);
        }
        else
        {
            ProductImageModel::where('id', $id)->where('delete', 0)
            ->update([
                'product_id' => $product_id
            ]);
        }

        return redirect()->route('getEditGalleryProduct', ['product_id' => $product_id, 'id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteGalleryProduct($product_id, $id)
    {
        Log::info('isi product id nya = '.$product_id);
        ProductImageModel::where('id', $id)->where('product_id', $product_id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    public function postSetImageProduct($product_id, $id)
    {
        Log::info('isi product id nya = '.$product_id);
        ProductImageModel::where('product_id', $product_id)->where('delete', 0)
        ->update(
            [
                'set_image' => 0,
            ]
        );

        ProductImageModel::where('id', $id)->where('product_id', $product_id)->where('delete', 0)
        ->update(
            [
                'set_image' => 1,
            ]
        );

        $arr['message'] = 'success';
        return json_encode($arr);
    }

    public function postRemoveImageProduct($product_id, $id)
    {
        Log::info('isi product id nya = '.$product_id);
        ProductImageModel::where('id', $id)->where('product_id', $product_id)->where('delete', 0)
        ->update(
            [
                'set_image' => 0,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    public function createImage($img)
    {
        $path = '/public/thumbnail-product/'; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$img->guessExtension();
        $img->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public function deleteImage($oldpath)
    {
        $oldpath = getcwd().$oldpath;
        
        unlink(realpath($oldpath));
    }
}