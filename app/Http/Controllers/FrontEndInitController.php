<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontEndInitController extends Controller
{
    public function getHome()
    {
        return view('home');
    }

    public function getDetail()
    {
        return view('detail');
    }

    public function getListing()
    {
        return view('listing');
    }

    public function getListingTable()
    {
        return view('listTable');
    }

    public function getAbout()
    {
        return view('about');
    }

    public function getArticle()
    {
        return view('article');
    }

    public function getBlog()
    {
        return view('blog');
    }

    public function getContact()
    {
        return view('contact');
    }

    public function get404()
    {
        return view('404');
    }

    public function getSubmit1()
    {
        return view('submit1');
    }

    public function getSubmit2()
    {
        return view('submit2');
    }

    public function getSubmit3()
    {
        return view('submit3');
    }

    public function getSubmit4()
    {
        return view('submit4');
    }

    public function getSubmit5()
    {
        return view('submit5');
    }

    public function postContact(ContactRequest $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $message = $request->get('message');

        $body = '';
        $body .= 'Nama: "'.$name.'"\r\n';
        $body .= 'Email: "'.$email.'"\r\n';
        $body .= 'Nomor HP: "'.$phone.'"\r\n';
        $body .= '"'.$message.'"';
        $body .= '';
        $check = mail('mimilodho@gmail.com', 'Kontak Kami', $body);
        
        $data['success'] = 'sent';
        \Log::info(json_encode($arr));
        return json_encode($arr);
    }
}
