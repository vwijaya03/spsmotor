<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddSejarahRequest;
use App\Http\Requests\AddSpsYoutubeRequest;
use App\Http\Requests\EditSejarahRequest;
use App\Http\Requests\EditSpsYoutubeRequest;
use App\SpsProfileModel;
use App\SpsYoutubeModel;
use Auth, Hash, DB, Log, Carbon;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaSejarah()
    {
        $add_button = true;
        $data = SpsProfileModel::select('id', 'description')->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        return view('admin/sejarah', ['users' => Auth::user(), 'sejarah' => $data, 'add_button' => $add_button]);
    }

    public function getAddSejarah()
    {
        $data = SpsProfileModel::select('id', 'description')->where('delete', 0)->get();

        if(count($data) > 0)
        {
            return redirect()->route('getSemuaSejarah');
        }

        return view('admin/add-sejarah', ['users' => Auth::user()]);
    }

    public function postAddSejarah(AddSejarahRequest $request)
    {
        $description = $request->get('description');
        
        $data = SpsProfileModel::create([
            'user_id' => Auth::user()->id,
            'description' => $description,
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        return redirect()->route('getEditSejarah', ['id' => $data->id])->with(['done' => 'Konten berhasil di tambahkan.'] );
    }

    public function getEditSejarah($id)
    {
        $konten = SpsProfileModel::select('description', 'id')->where('id', $id)->first();

        if($konten == null)
        {
            return redirect()->route('getSemuaSejarah');
        }

        return view('admin/edit-sejarah', ['users' => Auth::user(), 'data' => $konten]);
    }

    public function postEditSejarah(EditSejarahRequest $request, $id)
    {
        $description = $request->get('description');

        SpsProfileModel::where('id', $id)->where('delete', 0)
        ->update([
            'user_id' => Auth::user()->id,
            'description' => $description,
            'date_time' => date('Y-m-d H:i:s')
        ]);

        return redirect()->route('getEditSejarah', ['id' => $id])->with(['done' => 'Konten berhasil di ubah.'] );
    }

    public function postDeleteSejarah($id)
    {
        SpsProfileModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }

/*--------------------------------------------- SPS Youtube -------------------------------------------------------*/

    public function getSemuaSpsYoutube()
    {
        $data = SpsYoutubeModel::select('id', 'title', 'url')->where('delete', 0)->get();

        return view('admin/sps-youtube', ['users' => Auth::user(), 'sps_youtube' => $data]);
    }

    public function getAddSpsYoutube()
    {
        return view('admin/add-sps-youtube', ['users' => Auth::user()]);
    }

    public function postAddSpsYoutube(AddSpsYoutubeRequest $request)
    {
        $title = $request->get('title');
        $url = $request->get('url');
        
        $data = SpsYoutubeModel::create([
            'user_id' => Auth::user()->id,
            'title' => $title,
            'url' => $url,
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        return redirect()->route('getEditSpsYoutube', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditSpsYoutube($id)
    {
        $sps_youtube = SpsYoutubeModel::select('id', 'title', 'url')->where('id', $id)->first();

        return view('admin/edit-sps-youtube', ['users' => Auth::user(), 'data' => $sps_youtube]);
    }

    public function postEditSpsYoutube(EditSpsYoutubeRequest $request, $id)
    {
        $title = $request->get('title');
        $url = $request->get('url');

        SpsYoutubeModel::where('id', $id)->where('delete', 0)
        ->update([
            'user_id' => Auth::user()->id,
            'title' => $title,
            'url' => $url
        ]);

        return redirect()->route('getEditSpsYoutube', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteSpsYoutube($id)
    {
        SpsYoutubeModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }
}
