<?php

namespace App\Http\Controllers;
use App\Http\Requests\AddServiceBookingRequest;
use App\Http\Requests\EditServiceBookingRequest;
use App\CabangModel;
use App\ServiceBookingModel;
use Auth, Hash, DB, Log;

use Illuminate\Http\Request;

class ServiceBookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaServiceBooking()
    {
        $data = ServiceBookingModel::select('cabang.name as cabang_name', 'service_booking.id', 'service_booking.fullname', 'service_booking.email', 'service_booking.no_polisi', 'service_booking.no_telepon', 'service_booking.jenis_kendaraan', 'service_booking.jenis_service', 'service_booking.tanggal_booking', 'service_booking.jam_booking', 'service_booking.description')
        ->join('cabang', 'cabang.id', '=', 'service_booking.cabang_id')
        ->where('service_booking.delete', 0)
        ->where('cabang.delete', 0)
        ->get();

        return view('admin/service-booking', ['users' => Auth::user(), 'service_booking' => $data]);
    }

    public function getAddServiceBooking()
    {
    	$cabang = CabangModel::select('id', 'name')->where('delete', 0)->get();

        return view('admin/add-service-booking', ['users' => Auth::user(), 'cabang' => $cabang]);
    }

    public function postAddServiceBooking(AddServiceBookingRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $no_polisi = $request->get('no_polisi');
        $jenis_kendaraan = $request->get('jenis_kendaraan');
        $jenis_service = $request->get('jenis_service');
        $tanggal_booking = $request->get('tanggal_booking');
        $jam_booking = $request->get('jam_booking');
        $description = $request->get('description');

        $formatedTanggalBooking = date("Y-m-d", strtotime($tanggal_booking));

        $data = ServiceBookingModel::create([
            'cabang_id' => $cabang_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'no_polisi' => $no_polisi,
            'jenis_kendaraan' => $jenis_kendaraan,
            'jenis_service' => $jenis_service,
            'tanggal_booking' => $formatedTanggalBooking,
            'jam_booking' => $jam_booking,
            'description' => $description,
            'delete' => 0
        ]);

        return redirect()->route('getEditServiceBooking', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditServiceBooking($id)
    {
        $data_service_booking = ServiceBookingModel::select('service_booking.id', 'service_booking.cabang_id', 'service_booking.fullname', 'service_booking.email', 'service_booking.no_polisi', 'service_booking.no_telepon', 'service_booking.jenis_kendaraan', 'service_booking.jenis_service', 'service_booking.tanggal_booking', 'service_booking.jam_booking', 'service_booking.description')->where('id', $id)->first();

        if($data_service_booking == null)
        {
            return redirect()->route('getSemuaServiceBooking');
        }

        $selected_cabang = CabangModel::select('id', 'name')->where('id', $data_service_booking->cabang_id)->where('delete', 0)->first();

        $diff_cabang = CabangModel::select('id', 'name')->where('id', '!=', $data_service_booking->cabang_id)->where('delete', 0)->get();

        return view('admin/edit-service-booking', ['users' => Auth::user(), 'data' => $data_service_booking, 'selected_cabang' => $selected_cabang, 'diff_cabang' => $diff_cabang]);
    }

    public function postEditServiceBooking(EditServiceBookingRequest $request, $id)
    {
        $cabang_id = $request->get('cabang_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $no_polisi = $request->get('no_polisi');
        $jenis_kendaraan = $request->get('jenis_kendaraan');
        $jenis_service = $request->get('jenis_service');
        $tanggal_booking = $request->get('tanggal_booking');
        $jam_booking = $request->get('jam_booking');
        $description = $request->get('description');

        $formatedTanggalBooking = date("Y-m-d", strtotime($tanggal_booking));

        ServiceBookingModel::where('id', $id)->where('delete', 0)
        ->update([
            'cabang_id' => $cabang_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'no_polisi' => $no_polisi,
            'jenis_kendaraan' => $jenis_kendaraan,
            'jenis_service' => $jenis_service,
            'tanggal_booking' => $formatedTanggalBooking,
            'jam_booking' => $jam_booking,
            'description' => $description
        ]);

        return redirect()->route('getEditServiceBooking', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteServiceBooking($id)
    {
        ServiceBookingModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }
}
