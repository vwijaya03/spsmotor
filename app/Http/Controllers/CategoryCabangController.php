<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddCategoryCabangRequest;
use App\Http\Requests\EditCategoryCabangRequest;
use App\CategoryCabangModel;
use Auth, Hash, DB, Log, Carbon;

class CategoryCabangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaCategoryCabang()
    {
        $data = CategoryCabangModel::select('id', 'slug', 'name')->where('delete', 0)->get();

        return view('admin/category-cabang', ['users' => Auth::user(), 'kategori_cabang' => $data]);
    }

    public function getDetailCategoryCabang($slug)
    {
        //
    }

    public function getAddCategoryCabang()
    {
        return view('admin/add-category-cabang', ['users' => Auth::user()]);
    }

    public function postAddCategoryCabang(AddCategoryCabangRequest $request)
    {
        $category_cabang_name = $request->get('name');
        $category_cabang_name_slug = str_slug($category_cabang_name, '-');
        
        CategoryCabangModel::create([
            'slug' => $category_cabang_name_slug,
            'name' => $category_cabang_name,
            'delete' => 0
        ]);

        return redirect()->route('getEditCategoryCabang', ['slug' => $category_cabang_name_slug])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditCategoryCabang($slug)
    {
        $cabang = CategoryCabangModel::select('name', 'slug')->where('slug', $slug)->first();

        if($cabang == null)
        {
            return redirect()->route('getSemuaCategoryCabang');
        }

        return view('admin/edit-category-cabang', ['users' => Auth::user(), 'cabang' => $cabang]);
    }

    public function postEditCategoryCabang(EditCategoryCabangRequest $request, $slug)
    {
        $category_cabang_name = $request->get('name');
        $category_cabang_name_slug = str_slug($category_cabang_name, '-');

        CategoryCabangModel::where('slug', $slug)->where('delete', 0)
        ->update([
            'slug' => $category_cabang_name_slug,
            'name' => $category_cabang_name,
        ]);

        return redirect()->route('getEditCategoryCabang', ['slug' => $category_cabang_name_slug])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteCategoryCabang($slug)
    {
        CategoryCabangModel::where('slug', $slug)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }
}
