<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddNewsRequest;
use App\Http\Requests\EditNewsRequest;
use App\NewsModel;
use App\CommentModel;
use App\SubscribeMailModel;
use Auth, Hash, DB, Log;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaNews()
    {
        $data = NewsModel::select('id', 'slug', 'title', 'description', 'img_path')
        ->where('delete', 0)
        ->get();

        return view('admin/news', ['users' => Auth::user(), 'news' => $data]);
    }

    public function getAddNews()
    {
        return view('admin/add-news', ['users' => Auth::user()]);
    }

    public function postAddNews(AddNewsRequest $request)
    {
        $title = $request->get('title');
        $description = $request->get('description');
        $img = $request->file('img');
        $img_path = $this->createImage($img);
        $title_slug = str_slug($title, '-');
        
        $data = NewsModel::create([
            'user_id' => Auth::user()->id,
            'slug' => $title_slug,
            'title' => $title,
            'description' => $description,
            'img_path' => $img_path,
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        $subscribers = SubscribeMailModel::select('fullname', 'email')->where('delete', 0)->get();

        $base_url = url('/');

        $sender = "vwijaya08@gmail.com";

        foreach($subscribers as $subscriber)
        {
            $subject = $title;
            $htmlContent = '
                <html>
                <head>
                    <title>'.$title.'</title>
                </head>
                <body>
                    <h1>Hi ! '.$subscriber->fullname.'</h1>
                    <table cellspacing="0" style="width: 600px; height: 200px;">
                        <tr>
                            <img width="600px" height="300px" src="'.$base_url.$img_path.'">
                        </tr>
                        <tr style="background-color: #e0e0e0;">
                            <td>'.$description.'</td>
                        </tr>
                    </table>
                </body>
                </html>';

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= 'From: SPS Motor <'.$sender.'>' . "\r\n";
            $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

            $check_mail_cs = mail($subscriber->email, $title, $htmlContent, $headers);
        }

        return redirect()->route('getEditNews', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditNews($id)
    {

        $data_news = NewsModel::select('id', 'slug', 'title', 'description', 'img_path')->where('delete', 0)->where('id', $id)->first();

        if($data_news == null)
        {
            return redirect()->route('getSemuaNews');
        }

        return view('admin/edit-news', ['users' => Auth::user(), 'data' => $data_news]);
    }

    public function postEditNews(EditNewsRequest $request, $id)
    {
        $title = $request->get('title');
        $description = $request->get('description');
        $title_slug = str_slug($title, '-');

        if($request->hasFile('img'))
        {
            $img = $request->file('img');
            $img_path = $this->createImage($img);

            $oldData = NewsModel::find($id);

            $oldPath = $oldData->img_path;
            $this->deleteImage($oldPath);

            NewsModel::where('id', $id)->where('delete', 0)
            ->update([
                'user_id' => Auth::user()->id,
                'slug' => $title_slug,
                'title' => $title,
                'description' => $description,
                'img_path' => $img_path
            ]);
        }
        else
        {
            NewsModel::where('id', $id)->where('delete', 0)
            ->update([
                'user_id' => Auth::user()->id,
                'slug' => $title_slug,
                'title' => $title,
                'description' => $description,
            ]);
        }

        return redirect()->route('getEditNews', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteNews($id)
    {
        NewsModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    public function createImage($img)
    {
        $path = '/public/news-image/'; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$img->guessExtension();
        $img->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public function deleteImage($oldpath)
    {
        $oldpath = getcwd().$oldpath;
        
        unlink(realpath($oldpath));
    }

    public function remote_file_exists($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if( $httpCode == 200 ){return true;}
        return false;
    }
}
