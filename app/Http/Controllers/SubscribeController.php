<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubscribeMailModel;
use Auth, Hash, DB, Log;

class SubscribeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSubscriber()
    {
        $data = SubscribeMailModel::select('id', 'fullname', 'email')
        ->where('delete', 0)
        ->get(); 

        return view('admin/subscriber', ['users' => Auth::user(), 'subscriber' => $data]);
    }

    public function postDeleteSubscriber($id)
    {
        SubscribeMailModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }
}
