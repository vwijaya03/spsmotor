<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddCabangRequest;
use App\Http\Requests\EditCabangRequest;
use App\CabangModel;
use App\CategoryCabangModel;
use Auth, Hash, DB, Log;

class CabangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaCabang()
    {
        $data = CabangModel::select('cabang.id', 'category_cabang.name as category_name', 'cabang.slug as cabang_slug', 'cabang.name as cabang_name', 'cabang.img_path as cabang_image')
        ->join('category_cabang', 'category_cabang.id', '=', 'cabang.category_cabang_id')
        ->where('cabang.delete', 0)
        ->where('category_cabang.delete', 0)
        ->get(); 

        return view('admin/cabang', ['users' => Auth::user(), 'cabang' => $data]);
    }

    public function getAddCabang()
    {
        $data_category_cabang = CategoryCabangModel::select('id', 'name')->where('delete', 0)->get();

        return view('admin/add-cabang', ['users' => Auth::user(), 'category_cabang' => $data_category_cabang]);
    }

    public function postAddCabang(AddCabangRequest $request)
    {
        $category_cabang_id = $request->get('category_cabang_id');

        $email_bengkel_cabang = $request->get('email_bengkel_cabang');
        $email_pic_cabang = $request->get('email_pic_cabang');
        $email_technical_service_department_cabang = $request->get('email_technical_service_department_cabang');
        $email_customer_service_cabang = $request->get('email_customer_service_cabang');
        $fb_url = $request->get('fb_url');
        $twitter_url = $request->get('twitter_url');
        $ig_url = $request->get('ig_url');
        $name = $request->get('name');
        $description = $request->get('description');
        $address = $request->get('address');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $informasi_hari_dan_jam = $request->get('informasi_hari_dan_jam');
        $img = $request->file('img');
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $name_slug = str_slug($name, '-');
        $img_path = $this->createFile($img);
        
        CabangModel::create([
            'category_cabang_id' => $category_cabang_id,
            'user_id' => Auth::user()->id,
            'slug' => $name_slug,
            'email_bengkel_cabang' => $email_bengkel_cabang,
            'email_pic_cabang' => $email_pic_cabang,
            'email_technical_service_department_cabang' => $email_technical_service_department_cabang,
            'email_customer_service_cabang' => $email_customer_service_cabang,
            'fb_url' => $fb_url,
            'twitter_url' => $twitter_url,
            'ig_url' => $ig_url,
            'name' => $name,
            'description' => $description,
            'img_path' => $img_path,
            'address' => $address,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'informasi_hari_dan_jam' => $informasi_hari_dan_jam,
            'date_time' => date('Y-m-d H:i:s'),
            'lat' => $lat,
            'lng' => $lng,
            'delete' => 0
        ]);

        return redirect()->route('getEditCabang', ['slug' => $name_slug])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditCabang($slug)
    {
        $cabang = CabangModel::select('cabang.category_cabang_id', 'cabang.slug', 'cabang.email_bengkel_cabang', 'cabang.email_pic_cabang', 'cabang.email_technical_service_department_cabang', 'cabang.email_customer_service_cabang', 'cabang.fb_url', 'cabang.twitter_url', 'cabang.ig_url', 'cabang.name', 'cabang.description', 'cabang.address', 'cabang.no_telepon', 'cabang.informasi_hari_dan_jam', 'cabang.informasi_hari_dan_jam', 'cabang.lat', 'cabang.lng', 'cabang.img_path')
        ->where('cabang.slug', $slug)
        ->where('delete', 0)
        ->first();

        if($cabang == null)
        {
            return redirect()->route('getSemuaCabang');
        }

        $selected_category_cabang = CategoryCabangModel::select('id', 'name')->where('id', $cabang->category_cabang_id)->where('delete', 0)->first();

        $diff_category_cabang = CategoryCabangModel::select('id', 'name')->where('id', '!=', $cabang->category_cabang_id)->where('delete', 0)->get();

        return view('admin/edit-cabang', ['users' => Auth::user(), 'data' => $cabang, 'selected_category_cabang' => $selected_category_cabang, 'diff_category_cabang' => $diff_category_cabang]);
    }

    public function postEditCabang(EditCabangRequest $request, $slug)
    {
        $category_cabang_id = $request->get('category_cabang_id');
        $email_bengkel_cabang = $request->get('email_bengkel_cabang');
        $email_pic_cabang = $request->get('email_pic_cabang');
        $email_technical_service_department_cabang = $request->get('email_technical_service_department_cabang');
        $email_customer_service_cabang = $request->get('email_customer_service_cabang');
        $fb_url = $request->get('fb_url');
        $twitter_url = $request->get('twitter_url');
        $ig_url = $request->get('ig_url');
        $name = $request->get('name');
        $description = $request->get('description');
        $img = $request->file('img');
        $address = $request->get('address');
        $no_telepon = $request->get('no_telepon');
        $informasi_hari_dan_jam = $request->get('informasi_hari_dan_jam');
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $name_slug = str_slug($name, '-');

        if($request->hasFile('img'))
        {
            $img = $request->file('img');
            $img_path = $this->createFile($img);

            $oldData = CabangModel::select('img_path')->where('slug', $slug)->where('delete', 0)->first();

            $oldPath = $oldData->img_path;
            $this->deleteFile($oldPath);

            CabangModel::where('slug', $slug)->where('delete', 0)
            ->update([
                'category_cabang_id' => $category_cabang_id,
                'user_id' => Auth::user()->id,
                'slug' => $name_slug,
                'email_bengkel_cabang' => $email_bengkel_cabang,
                'email_pic_cabang' => $email_pic_cabang,
                'email_technical_service_department_cabang' => $email_technical_service_department_cabang,
                'email_customer_service_cabang' => $email_customer_service_cabang,
                'fb_url' => $fb_url,
                'twitter_url' => $twitter_url,
                'ig_url' => $ig_url,
                'name' => $name,
                'description' => $description,
                'img_path' => $img_path,
                'address' => $address,
                'no_telepon' => $no_telepon,
                'informasi_hari_dan_jam' => $informasi_hari_dan_jam,
                'lat' => $lat,
                'lng' => $lng
            ]);
        }
        else
        {
            CabangModel::where('slug', $slug)->where('delete', 0)
            ->update([
                'category_cabang_id' => $category_cabang_id,
                'user_id' => Auth::user()->id,
                'slug' => $name_slug,
                'email_bengkel_cabang' => $email_bengkel_cabang,
                'email_pic_cabang' => $email_pic_cabang,
                'email_technical_service_department_cabang' => $email_technical_service_department_cabang,
                'email_customer_service_cabang' => $email_customer_service_cabang,
                'fb_url' => $fb_url,
                'twitter_url' => $twitter_url,
                'ig_url' => $ig_url,
                'name' => $name,
                'description' => $description,
                'address' => $address,
                'no_telepon' => $no_telepon,
                'informasi_hari_dan_jam' => $informasi_hari_dan_jam,
                'lat' => $lat,
                'lng' => $lng
            ]);
        }

        return redirect()->route('getEditCabang', ['slug' => $name_slug])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteCabang($slug)
    {
        CabangModel::where('slug', $slug)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }

    public function createFile($file)
    {
        $path = '/public/branch-image/'; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$file->guessExtension();
        $file->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public function deleteFile($oldpath)
    {
        $oldpath = getcwd().$oldpath;
        
        unlink(realpath($oldpath));
    }
}
