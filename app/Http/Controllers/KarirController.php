<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddKarirRequest;
use App\Http\Requests\EditKarirRequest;
use App\KarirModel;
use Auth, Hash, DB, Log, Carbon;

class KarirController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaKarir()
    {
        $add_button = true;
        $data = KarirModel::select('id', 'description')->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        return view('admin/karir', ['users' => Auth::user(), 'karir' => $data, 'add_button' => $add_button]);
    }

    public function getAddKarir()
    {
        $data = KarirModel::select('id', 'description')->where('delete', 0)->get();

        if(count($data) > 0)
        {
            return redirect()->route('getSemuaKarir');
        }

        return view('admin/add-karir', ['users' => Auth::user()]);
    }

    public function postAddKarir(AddKarirRequest $request)
    {
        $description = $request->get('description');
        
        $data = KarirModel::create([
            'user_id' => Auth::user()->id,
            'description' => $description,
            'path' => 'description',
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        return redirect()->route('getEditKarir', ['id' => $data->id])->with(['done' => 'Konten berhasil di tambahkan.'] );
    }

    public function getEditKarir($id)
    {
        $karir = KarirModel::select('id', 'description')->where('id', $id)->first();

        if($karir == null)
        {
            return redirect()->route('getSemuaKarir');
        }

        return view('admin/edit-karir', ['users' => Auth::user(), 'data' => $karir]);
    }

    public function postEditKarir(EditKarirRequest $request, $id)
    {
        $description = $request->get('description');

        KarirModel::where('id', $id)->where('delete', 0)
        ->update([
            'user_id' => Auth::user()->id,
            'description' => $description,
        ]);

        return redirect()->route('getEditKarir', ['id' => $id])->with(['done' => 'Konten berhasil di ubah.'] );
    }

    public function postDeleteKarir($id)
    {
        KarirModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }
}
