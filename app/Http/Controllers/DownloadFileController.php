<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddDownloadFileRequest;
use App\Http\Requests\EditDownloadFileRequest;
use App\DownloadBrosurDanSukuCadangModel;
use Auth, Hash, DB, Log;
use PDF;
use Response;
use File;
use \Carbon;

class DownloadFileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getFile($id)
    {
        $file = DownloadBrosurDanSukuCadangModel::select('title', 'tipe', 'id', 'path')->where('id', $id)->first();
        // return PDF::loadFile(url($file->path))->stream(url($file->path));
        // die(url($file->path));

        // $response = Response::make(file_get_contents(url($file->path)), 200);

        // header('Content-Type: application/pdf');
        // header('Content-Disposition: inline; filename='.$path);
        // header('Content-Transfer-Encoding: binary');
        // header('Accept-Ranges: bytes');

        // $response->header('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        // $response->header('Content-Type', 'application/msword');
        // // $response->header('Content-Type', 'application/pdf');
        // $response->header('Content-Disposition', 'inline; filename=asdasdasda');
        // $response->header('Content-Transfer-Encoding', 'binary');
        // $response->header('Accept-Ranges', 'bytes');
        // // $response->header('Content-Type', 'image/png');

        // return $response;
        return view('v2/file', ['users' => Auth::user(), 'path' => $file->path]);
    }

    public function getSemuaDownloadFile()
    {
        $data = DownloadBrosurDanSukuCadangModel::select('id', 'title', 'path')->where('delete', 0)->get();

        return view('admin/download-file', ['users' => Auth::user(), 'file' => $data]);
    }

    public function getAddDownloadFile()
    {
        return view('admin/add-download-file', ['users' => Auth::user()]);
    }

    public function postAddDownloadFile(AddDownloadFileRequest $request)
    {
        $title = $request->get('title');
        $tipe = $request->get('tipe');
        $file = $request->file('file');
        $path = $this->createFile($file);
        
        $data = DownloadBrosurDanSukuCadangModel::create([
            'user_id' => Auth::user()->id,
            'title' => $title,
            'path' => $path,
            'file_size' => $file->getClientSize(),
            'tipe' => $tipe,
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        return redirect()->route('getEditDownloadFile', ['id' => $data->id])->with(['done' => 'File berhasil di tambahkan.'] );
    }

    public function getEditDownloadFile($id)
    {
        $file = DownloadBrosurDanSukuCadangModel::select('title', 'tipe', 'id', 'path')->where('id', $id)->first();

        if($file == null)
        {
            return redirect()->route('getSemuaDownloadFile');
        }

        return view('admin/edit-download-file', ['users' => Auth::user(), 'data' => $file]);
    }

    public function postEditDownloadFile(EditDownloadFileRequest $request, $id)
    {
        $title = $request->get('title');
        $tipe = $request->get('tipe');
        $file = $request->file('file');

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $path = $this->createFile($file);

            $oldData = DownloadBrosurDanSukuCadangModel::find($id);

            $oldPath = $oldData->path;
            $this->deleteFile($oldPath);

            DownloadBrosurDanSukuCadangModel::where('id', $id)->where('delete', 0)
            ->update([
                'user_id' => Auth::user()->id,
                'title' => $title,
                'path' => $path,
                'file_size' => $file->getClientSize(),
                'tipe' => $tipe,
                'date_time' => date('Y-m-d H:i:s'),
                'delete' => 0
            ]);
        }
        else
        {
            DownloadBrosurDanSukuCadangModel::where('id', $id)->where('delete', 0)
            ->update([
                'user_id' => Auth::user()->id,
                'title' => $title,
                'tipe' => $tipe,
            ]);
        }

        return redirect()->route('getEditDownloadFile', ['id' => $id])->with(['done' => 'File berhasil di ubah.'] );
    }

    public function postDeleteDownloadFile($id)
    {
        DownloadBrosurDanSukuCadangModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    public function createFile($file)
    {
        $path = '/public/download-file/'; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$file->guessExtension();
        $file->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public function deleteFile($oldpath)
    {
        $oldpath = getcwd().$oldpath;
        
        unlink(realpath($oldpath));
    }
}
