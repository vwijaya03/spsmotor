<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddCabangRequest;
use App\Http\Requests\EditCabangRequest;
use App\Http\Requests\AdminReplyCommentRequest;
use App\ProductModel;
use App\NewsModel;
use App\CommentModel;
use Auth, Hash, DB, Log;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaComment()
    {
        $comment_product = CommentModel::select('product.id as product_id', 'product.name as product_or_news_title', 'comment.id', 'comment.tipe', 'comment.product_or_news_id', 'comment.comment_fullname', 'comment.comment_email', 'comment.comment_description', 'comment.approval', 'comment.date_time')
        ->join('product', 'product.id', '=', 'comment.product_or_news_id')
        ->where('comment.delete', 0)
        ->where('product.delete', 0)
        ->where('comment.tipe', 'product')
        ->get();

        $comment_news = CommentModel::select('news.id as news_id', 'news.title as product_or_news_title', 'comment.id', 'comment.tipe', 'comment.product_or_news_id', 'comment.comment_fullname', 'comment.comment_email', 'comment.comment_description', 'comment.approval', 'comment.date_time')
        ->join('news', 'news.id', '=', 'comment.product_or_news_id')
        ->where('comment.delete', 0)
        ->where('news.delete', 0)
        ->where('comment.tipe', 'news')
        ->get();

        $data = [];

        $data = array_merge_recursive($comment_product->toArray(), $comment_news->toArray());

        return view('admin/comment', ['users' => Auth::user(), 'comment' => $data]);
    }

    public function getReplyComment($id)
    {
        $comment = CommentModel::select('id')->where('id', $id)->where('delete', 0)->first();

        if($comment == null)
        {
            return redirect()->route('getSemuaComment');
        }

        return view('admin/reply-comment', ['users' => Auth::user(), 'comment_id' => $id]);
    }

    public function postReplyComment(AdminReplyCommentRequest $request, $id)
    {
        $comment = CommentModel::select('id', 'tipe', 'product_or_news_id')->where('id', $id)->where('delete', 0)->first();

        if($comment == null)
        {
            return redirect()->route('getSemuaComment');
        }

        CommentModel::create([
            'tipe' => $comment->tipe,
            'product_or_news_id' => $comment->product_or_news_id,
            'comment_fullname' => 'Admin',
            'comment_email' => ' ',
            'comment_description' => $request->get('comment'),
            'approval' => 1,
            'date_time' => date('Y-m-d H:i:s'),
            'role' => 'admin',
            'delete' => 0
        ]);

        return redirect()->route('getSemuaComment');
    }

    public function postChangeStatusComment($id, $status)
    {
        CommentModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'approval' => $status,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }

    public function postDeleteComment($id)
    {
        CommentModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }
}
