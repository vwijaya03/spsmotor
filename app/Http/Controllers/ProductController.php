<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\EditProductRequest;
use App\ProductModel;
use App\CategoryProductModel;
use Auth, Hash, DB, Log, Feeds;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaProduct()
    {
        $data = ProductModel::select('category_product.id as category_product_id', 'category_product.name as category_product_name', 'product.id', 'product.slug', 'product.name', 'product.description')
        ->join('category_product', 'category_product.id', '=', 'product.category_id')
        ->where('product.delete', 0)
        ->where('category_product.delete', 0)
        ->get();

        // $feed = Feeds::make(['http://feed.liputan6.com/rss'], 1); // if RSS Feed has invalid mime types, force to read

        // $isi_feed = array(
        //   'title'     => $feed->get_title(),
        //   'permalink' => $feed->get_permalink(),
        //   'items'     => $feed->get_items(),
        // );

        // print("<pre>".print_r($isi_feed['items'][0]->feed->data['child']['']['rss'][0]['child']['']['channel'],true)."</pre>");
        // die();

        return view('admin/product', ['users' => Auth::user(), 'product' => $data]);
    }

    public function getAddProduct()
    {
        $category_product = CategoryProductModel::select('id', 'name')->where('delete', 0)->get();

        return view('admin/add-product', ['users' => Auth::user(), 'category_product' => $category_product]);
    }

    public function postAddProduct(AddProductRequest $request)
    {
        $category_id = $request->get('category_id');
        $name = $request->get('name');
        $description = $request->get('description');
        $price = $request->get('price');
        $thumbnail = $request->file('thumbnail');
        $thumbnail_path = $this->createImage($thumbnail);

        $tipe_mesin = $request->get('tipe_mesin');
        $volume_langkah = $request->get('volume_langkah');
        $sistem_pendingin = $request->get('sistem_pendingin');
        $sistem_suplai_bahan_bakar = $request->get('sistem_suplai_bahan_bakar');
        $diameter_x_langkah = $request->get('diameter_x_langkah');
        $tipe_transmisi = $request->get('tipe_transmisi');
        $rasio_kompresi = $request->get('rasio_kompresi');
        $daya_maksimum = $request->get('daya_maksimum');
        $torsi_maksimum = $request->get('torsi_maksimum');
        $pola_pengoperan_gigi = $request->get('pola_pengoperan_gigi');
        $tipe_starter = $request->get('tipe_starter');
        $tipe_kopling = $request->get('tipe_kopling');
        $kapasitas_minyak_pelumas = $request->get('kapasitas_minyak_pelumas');
        $panjang_lebar_tinggi = $request->get('panjang_lebar_tinggi');
        $jarak_sumbu_roda = $request->get('jarak_sumbu_roda');
        $jarak_terendah_ke_tanah = $request->get('jarak_terendah_ke_tanah');
        $curb_weight = $request->get('curb_weight');
        $kapasitas_tangki_bbm = $request->get('kapasitas_tangki_bbm');
        $tipe_rangka = $request->get('tipe_rangka');
        $tipe_suspensi_depan = $request->get('tipe_suspensi_depan');
        $tipe_suspensi_belakang = $request->get('tipe_suspensi_belakang');
        $ukuran_ban_depan = $request->get('ukuran_ban_depan');
        $ukuran_ban_belakang = $request->get('ukuran_ban_belakang');
        $tipe_rem_depan = $request->get('tipe_rem_depan');
        $tipe_rem_belakang = $request->get('tipe_rem_belakang');
        $tipe_aki = $request->get('tipe_aki');
        $sistem_pengapian = $request->get('sistem_pengapian');
        $tipe_busi = $request->get('tipe_busi');

        $name_slug = str_slug($name, '-');
        
        ProductModel::create([
            'category_id' => $category_id,
            'user_id' => Auth::user()->id,
            'slug' => $name_slug,
            'name' => $name,
            'description' => $description,
            'price' => $price,
            'thumbnail' => $thumbnail_path,
            'tipe_mesin' => $tipe_mesin,
            'volume_langkah' => $volume_langkah,
            'sistem_pendingin' => $sistem_pendingin,
            'sistem_suplai_bahan_bakar' => $sistem_suplai_bahan_bakar,
            'diameter_x_langkah' => $diameter_x_langkah,
            'tipe_transmisi' => $tipe_transmisi,
            'rasio_kompresi' => $rasio_kompresi,
            'daya_maksimum' => $daya_maksimum,
            'torsi_maksimum' => $torsi_maksimum,
            'pola_pengoperan_gigi' => $pola_pengoperan_gigi,
            'tipe_starter' => $tipe_starter,
            'tipe_kopling' => $tipe_kopling,
            'kapasitas_minyak_pelumas' => $kapasitas_minyak_pelumas,
            'panjang_lebar_tinggi' => $panjang_lebar_tinggi,
            'jarak_sumbu_roda' => $jarak_sumbu_roda,
            'jarak_terendah_ke_tanah' => $jarak_terendah_ke_tanah,
            'curb_weight' => $curb_weight,
            'kapasitas_tangki_bbm' => $kapasitas_tangki_bbm,
            'tipe_rangka' => $tipe_rangka,
            'tipe_suspensi_depan' => $tipe_suspensi_depan,
            'tipe_suspensi_belakang' => $tipe_suspensi_belakang,
            'ukuran_ban_depan' => $ukuran_ban_depan,
            'ukuran_ban_belakang' => $ukuran_ban_belakang,
            'tipe_rem_depan' => $tipe_rem_depan,
            'tipe_rem_belakang' => $tipe_rem_belakang,
            'tipe_aki' => $tipe_aki,
            'sistem_pengapian' => $sistem_pengapian,
            'tipe_busi' => $tipe_busi,

            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        return redirect()->route('getEditProduct', ['slug' => $name_slug])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditProduct($slug)
    {
        $product = ProductModel::select('category_id', 'name', 'description', 'slug', 'description', 'price', 'thumbnail', 'tipe_mesin', 'volume_langkah', 'sistem_pendingin', 'sistem_suplai_bahan_bakar', 'diameter_x_langkah', 'tipe_transmisi', 'rasio_kompresi', 'daya_maksimum', 'torsi_maksimum', 'pola_pengoperan_gigi', 'tipe_starter', 'tipe_kopling', 'kapasitas_minyak_pelumas', 'panjang_lebar_tinggi', 'jarak_sumbu_roda', 'jarak_terendah_ke_tanah', 'curb_weight', 'kapasitas_tangki_bbm', 'tipe_rangka', 'tipe_suspensi_depan', 'tipe_suspensi_belakang', 'ukuran_ban_depan', 'ukuran_ban_belakang', 'tipe_rem_depan', 'tipe_rem_belakang', 'tipe_aki', 'sistem_pengapian', 'tipe_busi')->where('slug', $slug)->where('delete', 0)->first();

        if($product == null)
        {
            return redirect()->route('getSemuaProduct');
        }

        $selected_category_product = CategoryProductModel::select('id', 'name')->where('id', $product->category_id)->where('delete', 0)->first();

        $diff_category_product = CategoryProductModel::select('id', 'name')->where('id', '!=', $product->category_id)->where('delete', 0)->get();

        return view('admin/edit-product', ['users' => Auth::user(), 'data' => $product, 'selected_category_product' => $selected_category_product, 'diff_category_product' => $diff_category_product]);
    }

    public function postEditProduct(EditProductRequest $request, $slug)
    {
        $category_id = $request->get('category_id');
        $name = $request->get('name');
        $description = $request->get('description');
        $price = $request->get('price');
        $name_slug = str_slug($name, '-');

        $tipe_mesin = $request->get('tipe_mesin');
        $volume_langkah = $request->get('volume_langkah');
        $sistem_pendingin = $request->get('sistem_pendingin');
        $sistem_suplai_bahan_bakar = $request->get('sistem_suplai_bahan_bakar');
        $diameter_x_langkah = $request->get('diameter_x_langkah');
        $tipe_transmisi = $request->get('tipe_transmisi');
        $rasio_kompresi = $request->get('rasio_kompresi');
        $daya_maksimum = $request->get('daya_maksimum');
        $torsi_maksimum = $request->get('torsi_maksimum');
        $pola_pengoperan_gigi = $request->get('pola_pengoperan_gigi');
        $tipe_starter = $request->get('tipe_starter');
        $tipe_kopling = $request->get('tipe_kopling');
        $kapasitas_minyak_pelumas = $request->get('kapasitas_minyak_pelumas');
        $panjang_lebar_tinggi = $request->get('panjang_lebar_tinggi');
        $jarak_sumbu_roda = $request->get('jarak_sumbu_roda');
        $jarak_terendah_ke_tanah = $request->get('jarak_terendah_ke_tanah');
        $curb_weight = $request->get('curb_weight');
        $kapasitas_tangki_bbm = $request->get('kapasitas_tangki_bbm');
        $tipe_rangka = $request->get('tipe_rangka');
        $tipe_suspensi_depan = $request->get('tipe_suspensi_depan');
        $tipe_suspensi_belakang = $request->get('tipe_suspensi_belakang');
        $ukuran_ban_depan = $request->get('ukuran_ban_depan');
        $ukuran_ban_belakang = $request->get('ukuran_ban_belakang');
        $tipe_rem_depan = $request->get('tipe_rem_depan');
        $tipe_rem_belakang = $request->get('tipe_rem_belakang');
        $tipe_aki = $request->get('tipe_aki');
        $sistem_pengapian = $request->get('sistem_pengapian');
        $tipe_busi = $request->get('tipe_busi');

        if($request->hasFile('thumbnail'))
        {
            $thumbnail = $request->file('thumbnail');
            $thumbnail_path = $this->createImage($thumbnail);

            $oldData = ProductModel::select('thumbnail')
            ->where('product.delete', 0)
            ->where('product.slug', $slug)
            ->first();

            $oldPath = $oldData->thumbnail;

            if($oldPath != null || $oldPath != '')
            {
                $this->deleteImage($oldPath);
            }

            ProductModel::where('slug', $slug)->where('delete', 0)
            ->update([
                'category_id' => $category_id,
                'slug' => $name_slug,
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'thumbnail' => $thumbnail_path,
                'tipe_mesin' => $tipe_mesin,
                'volume_langkah' => $volume_langkah,
                'sistem_pendingin' => $sistem_pendingin,
                'sistem_suplai_bahan_bakar' => $sistem_suplai_bahan_bakar,
                'diameter_x_langkah' => $diameter_x_langkah,
                'tipe_transmisi' => $tipe_transmisi,
                'rasio_kompresi' => $rasio_kompresi,
                'daya_maksimum' => $daya_maksimum,
                'torsi_maksimum' => $torsi_maksimum,
                'pola_pengoperan_gigi' => $pola_pengoperan_gigi,
                'tipe_starter' => $tipe_starter,
                'tipe_kopling' => $tipe_kopling,
                'kapasitas_minyak_pelumas' => $kapasitas_minyak_pelumas,
                'panjang_lebar_tinggi' => $panjang_lebar_tinggi,
                'jarak_sumbu_roda' => $jarak_sumbu_roda,
                'jarak_terendah_ke_tanah' => $jarak_terendah_ke_tanah,
                'curb_weight' => $curb_weight,
                'kapasitas_tangki_bbm' => $kapasitas_tangki_bbm,
                'tipe_rangka' => $tipe_rangka,
                'tipe_suspensi_depan' => $tipe_suspensi_depan,
                'tipe_suspensi_belakang' => $tipe_suspensi_belakang,
                'ukuran_ban_depan' => $ukuran_ban_depan,
                'ukuran_ban_belakang' => $ukuran_ban_belakang,
                'tipe_rem_depan' => $tipe_rem_depan,
                'tipe_rem_belakang' => $tipe_rem_belakang,
                'tipe_aki' => $tipe_aki,
                'sistem_pengapian' => $sistem_pengapian,
                'tipe_busi' => $tipe_busi
            ]);
        }
        else
        {
            ProductModel::where('slug', $slug)->where('delete', 0)
            ->update([
                'category_id' => $category_id,
                'slug' => $name_slug,
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'tipe_mesin' => $tipe_mesin,
                'volume_langkah' => $volume_langkah,
                'sistem_pendingin' => $sistem_pendingin,
                'sistem_suplai_bahan_bakar' => $sistem_suplai_bahan_bakar,
                'diameter_x_langkah' => $diameter_x_langkah,
                'tipe_transmisi' => $tipe_transmisi,
                'rasio_kompresi' => $rasio_kompresi,
                'daya_maksimum' => $daya_maksimum,
                'torsi_maksimum' => $torsi_maksimum,
                'pola_pengoperan_gigi' => $pola_pengoperan_gigi,
                'tipe_starter' => $tipe_starter,
                'tipe_kopling' => $tipe_kopling,
                'kapasitas_minyak_pelumas' => $kapasitas_minyak_pelumas,
                'panjang_lebar_tinggi' => $panjang_lebar_tinggi,
                'jarak_sumbu_roda' => $jarak_sumbu_roda,
                'jarak_terendah_ke_tanah' => $jarak_terendah_ke_tanah,
                'curb_weight' => $curb_weight,
                'kapasitas_tangki_bbm' => $kapasitas_tangki_bbm,
                'tipe_rangka' => $tipe_rangka,
                'tipe_suspensi_depan' => $tipe_suspensi_depan,
                'tipe_suspensi_belakang' => $tipe_suspensi_belakang,
                'ukuran_ban_depan' => $ukuran_ban_depan,
                'ukuran_ban_belakang' => $ukuran_ban_belakang,
                'tipe_rem_depan' => $tipe_rem_depan,
                'tipe_rem_belakang' => $tipe_rem_belakang,
                'tipe_aki' => $tipe_aki,
                'sistem_pengapian' => $sistem_pengapian,
                'tipe_busi' => $tipe_busi
            ]);
        }

        return redirect()->route('getEditProduct', ['slug' => $name_slug])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteProduct($slug)
    {
        ProductModel::where('slug', $slug)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }

    public function createImage($img)
    {
        $path = '/public/gallery-product/'; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$img->guessExtension();
        $img->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public function deleteImage($oldpath)
    {
        $oldpath = getcwd().$oldpath;
        
        unlink(realpath($oldpath));
    }
}
