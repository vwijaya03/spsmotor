<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log, Carbon;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDashboard()
    {
        return view('admin/dashboard', ['users' => Auth::user()]);
    }
}
