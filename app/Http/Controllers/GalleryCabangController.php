<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddGalleryCabangRequest;
use App\Http\Requests\EditGalleryCabangRequest;
use App\GalleryCabangModel;
use App\CabangModel;
use App\CategoryCabangModel;
use Auth, Hash, DB, Log;

class GalleryCabangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getGalleryCabang()
    {
        $data = GalleryCabangModel::select('cabang.name as cabang_name', 'gallery_cabang.id', 'gallery_cabang.title', 'gallery_cabang.description', 'gallery_cabang.img_path')
        ->join('cabang', 'cabang.id', '=', 'gallery_cabang.cabang_id')
        ->where('gallery_cabang.delete', 0)
        ->where('cabang.delete', 0)
        ->get();

        return view('admin/gallery-cabang', ['users' => Auth::user(), 'gallery_cabang' => $data]);
    }

    public function getAddGalleryCabang()
    {
        $cabang = CabangModel::select('id', 'name')->where('delete', 0)->get();

        return view('admin/add-gallery-cabang', ['users' => Auth::user(), 'cabang' => $cabang]);
    }

    public function postAddGalleryCabang(AddGalleryCabangRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $title = $request->get('title');
        $description = $request->get('description');
        $img = $request->file('img');
        $img_path = $this->createImage($img);
        $title_slug = str_slug($title, '-');
        
        $data = GalleryCabangModel::create([
            'cabang_id' => $cabang_id,
            'user_id' => Auth::user()->id,
            'slug' => $title_slug,
            'title' => $title,
            'description' => $description,
            'img_path' => $img_path,
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        return redirect()->route('getEditGalleryCabang', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditGalleryCabang($id)
    {

        $data_gallery_cabang = GalleryCabangModel::select('id', 'cabang_id', 'title', 'description', 'img_path')->where('delete', 0)->where('id', $id)->first();

        if($data_gallery_cabang == null)
        {
            return redirect()->route('getGalleryCabang');
        }

        $selected_cabang = CabangModel::select('id', 'name')->where('id', $data_gallery_cabang->cabang_id)->where('delete', 0)->first();

        $diff_cabang = CabangModel::select('id', 'name')->where('id', '!=', $data_gallery_cabang->cabang_id)->where('delete', 0)->get();

        return view('admin/edit-gallery-cabang', ['users' => Auth::user(), 'data' => $data_gallery_cabang, 'selected_cabang' => $selected_cabang, 'diff_cabang' => $diff_cabang]);
    }

    public function postEditGalleryCabang(EditGalleryCabangRequest $request, $id)
    {
        $cabang_id = $request->get('cabang_id');
        $title = $request->get('title');
        $description = $request->get('description');
        $title_slug = str_slug($title, '-');

        if($request->hasFile('img'))
        {
            $img = $request->file('img');
            $img_path = $this->createImage($img);

            $oldData = GalleryCabangModel::find($id);

            $oldPath = $oldData->img_path;
            $this->deleteImage($oldPath);

            GalleryCabangModel::where('id', $id)->where('delete', 0)
            ->update([
                'cabang_id' => $cabang_id,
                'user_id' => Auth::user()->id,
                'slug' => $title_slug,
                'title' => $title,
                'description' => $description,
                'img_path' => $img_path
            ]);
        }
        else
        {
            GalleryCabangModel::where('id', $id)->where('delete', 0)
            ->update([
                'cabang_id' => $cabang_id,
                'user_id' => Auth::user()->id,
                'slug' => $title_slug,
                'title' => $title,
                'description' => $description,
            ]);
        }

        return redirect()->route('getEditGalleryCabang', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteGalleryCabang($id)
    {
        GalleryCabangModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    public function createImage($img)
    {
        $path = '/public/gallery-cabang/'; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$img->guessExtension();
        $img->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public function deleteImage($oldpath)
    {
        $oldpath = getcwd().$oldpath;
        
        unlink(realpath($oldpath));
    }

    public function remote_file_exists($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if( $httpCode == 200 ){return true;}
        return false;
    }
}
