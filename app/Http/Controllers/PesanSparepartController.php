<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PesanSparepartModel;
use Auth, Hash, DB, Log;

class PesanSparepartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPesanSparepartAdmin()
    {
        $data = PesanSparepartModel::select('cabang.name as cabang_name', 'product.name as product_name', 'pesan_sparepart.id', 'pesan_sparepart.fullname', 'pesan_sparepart.email', 'pesan_sparepart.no_telepon', 'pesan_sparepart.jenis_kendaraan')
        ->join('cabang', 'cabang.id', '=', 'pesan_sparepart.cabang_id')
        ->join('product', 'product.id', '=', 'pesan_sparepart.product_id')
        ->where('pesan_sparepart.delete', 0)
        ->where('cabang.delete', 0)
        ->where('product.delete', 0)
        ->get(); 

        return view('admin/pesan-sparepart', ['users' => Auth::user(), 'pemesanan_sparepart' => $data]);
    }

    public function getDetailPesanSparepartAdmin($id)
    {
        $data_pemesanan_sparepart = PesanSparepartModel::select('cabang.name as cabang_name', 'product.name as product_name', 'pesan_sparepart.id', 'pesan_sparepart.fullname', 'pesan_sparepart.email', 'pesan_sparepart.no_telepon', 'pesan_sparepart.jenis_kendaraan', 'pesan_sparepart.tahun_rakitan', 'pesan_sparepart.description')
        ->join('cabang', 'cabang.id', '=', 'pesan_sparepart.cabang_id')
        ->join('product', 'product.id', '=', 'pesan_sparepart.product_id')
        ->where('pesan_sparepart.id', $id)
        ->where('pesan_sparepart.delete', 0)
        ->where('cabang.delete', 0)
        ->where('product.delete', 0)
        ->first(); 

        if($data_pemesanan_sparepart == null)
        {
        	return redirect()->route('getPesanSparepartAdmin');
        }

        

        return view('admin/detail-pesan-sparepart', ['users' => Auth::user(), 'data' => $data_pemesanan_sparepart]);
    }

    public function postDeletePesanSparepart($id)
    {
        PesanSparepartModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }
}
