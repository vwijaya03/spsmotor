<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddBackgroundImageRequest;
use App\BackgroundImagesModel;
use Auth, Hash, DB, Log;
use \Carbon;

class BackgroundImageController extends Controller
{
    private $bgLayananSPSMotorHome = 'bg-layanan-spsmotor-home';
    private $bgBeritaTerbaruHome = 'bg-berita-terbaru-home';
    private $bgLanggananBeritaHome = 'bg-langganan-berita-home';
    private $bgPopupHome = 'bg-pop-up-home';
    private $bgHeaderSejarah = 'bg-header-sejarah';
    private $bgContentSejarah = 'bg-content-sejarah';
    private $bgGalleryFotoCabang = 'bg-gallery-foto-cabang';
    private $bgDetailFotoCabang = 'bg-detail-foto-cabang';
    private $bgSpsYoutube = 'bg-sps-youtube';
    private $bgJaringan = 'bg-jaringan';
    private $bgDetailJaringan = 'bg-detail-jaringan';
    private $bgHeaderProduk = 'bg-header-produk';
    private $bgProduk = 'bg-produk';
    private $bgDetailProduk = 'bg-detail-produk';
    private $bgHeaderBerita = 'bg-header-berita';
    private $bgBerita = 'bg-berita';
    private $bgDetailBerita = 'bg-detail-berita';
    private $bgDownload = 'bg-download';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUploadBgLayananSpsmotorHome()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgLayananSPSMotorHome)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-layanan-spsmotor-home';
        $delete_url = 'delete-upload-bg-layanan-spsmotor-home';

        return view('admin/upload-bg-layanan-spsmotor-home', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgLayananSpsmotorHome(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgLayananSPSMotorHome)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgLayananSpsmotorHome');
        }

        $tipe = $this->bgLayananSPSMotorHome;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgLayananSPSMotorHome);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgLayananSpsmotorHome')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgLayananSpsmotorHome($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgLayananSPSMotorHome)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgLayananSpsmotorHome');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgLayananSPSMotorHome)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgBeritaTerbaruHome()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgBeritaTerbaruHome)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-berita-terbaru-home';
        $delete_url = 'delete-upload-bg-berita-terbaru-home';

        return view('admin/upload-bg-berita-terbaru-home', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgBeritaTerbaruHome(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgBeritaTerbaruHome)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgBeritaTerbaruHome');
        }

        $tipe = $this->bgBeritaTerbaruHome;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgBeritaTerbaruHome);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgBeritaTerbaruHome')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgBeritaTerbaruHome($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgBeritaTerbaruHome)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgBeritaTerbaruHome');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgBeritaTerbaruHome)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgLanggananBeritaHome()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgLanggananBeritaHome)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-langganan-berita-home';
        $delete_url = 'delete-upload-bg-langganan-berita-home';

        return view('admin/upload-bg-langganan-berita-home', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgLanggananBeritaHome(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgLanggananBeritaHome)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgLanggananBeritaHome');
        }

        $tipe = $this->bgLanggananBeritaHome;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgLanggananBeritaHome);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgLanggananBeritaHome')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgLanggananBeritaHome($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgLanggananBeritaHome)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgLanggananBeritaHome');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgLanggananBeritaHome)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadPopupHome()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgPopupHome)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-popup-home';
        $delete_url = 'delete-upload-popup-home';

        return view('admin/upload-popup-home', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadPopupHome(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgPopupHome)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadPopupHome');
        }

        $tipe = $this->bgPopupHome;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgPopupHome);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadPopupHome')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadPopupHome($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgPopupHome)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadPopupHome');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgPopupHome)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgHeaderSejarah()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderSejarah)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-header-sejarah';
        $delete_url = 'delete-upload-bg-header-sejarah';

        return view('admin/upload-bg-header-sejarah', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgHeaderSejarah(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderSejarah)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgHeaderSejarah');
        }

        $tipe = $this->bgHeaderSejarah;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgHeaderSejarah);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgHeaderSejarah')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgHeaderSejarah($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderSejarah)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgHeaderSejarah');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgHeaderSejarah)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgContentSejarah()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgContentSejarah)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-content-sejarah';
        $delete_url = 'delete-upload-bg-content-sejarah';

        return view('admin/upload-bg-content-sejarah', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgContentSejarah(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgContentSejarah)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgContentSejarah');
        }

        $tipe = $this->bgContentSejarah;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgContentSejarah);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgContentSejarah')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgContentSejarah($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgContentSejarah)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgContentSejarah');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgContentSejarah)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgGalleryFotoCabang()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgGalleryFotoCabang)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-gallery-foto-cabang';
        $delete_url = 'delete-upload-bg-gallery-foto-cabang';

        return view('admin/upload-bg-gallery-foto-cabang', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgGalleryFotoCabang(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgGalleryFotoCabang)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgGalleryFotoCabang');
        }

        $tipe = $this->bgGalleryFotoCabang;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgGalleryFotoCabang);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgGalleryFotoCabang')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgGalleryFotoCabang($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgGalleryFotoCabang)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgGalleryFotoCabang');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgGalleryFotoCabang)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgDetailFotoCabang()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailFotoCabang)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-detail-foto-cabang';
        $delete_url = 'delete-upload-bg-detail-foto-cabang';

        return view('admin/upload-bg-detail-foto-cabang', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgDetailFotoCabang(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailFotoCabang)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDetailFotoCabang');
        }

        $tipe = $this->bgDetailFotoCabang;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgDetailFotoCabang);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgDetailFotoCabang')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgDetailFotoCabang($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailFotoCabang)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDetailFotoCabang');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgDetailFotoCabang)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgSpsYoutube()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgSpsYoutube)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-sps-youtube';
        $delete_url = 'delete-upload-bg-sps-youtube';

        return view('admin/upload-bg-sps-youtube', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgSpsYoutube(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgSpsYoutube)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgSpsYoutube');
        }

        $tipe = $this->bgSpsYoutube;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgSpsYoutube);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgSpsYoutube')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgSpsYoutube($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgSpsYoutube)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgSpsYoutube');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgSpsYoutube)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgJaringan()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgJaringan)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-jaringan';
        $delete_url = 'delete-upload-bg-jaringan';

        return view('admin/upload-bg-jaringan', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgJaringan(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgJaringan)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgJaringan');
        }

        $tipe = $this->bgJaringan;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgJaringan);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgJaringan')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgJaringan($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgJaringan)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgJaringan');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgJaringan)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgDetailJaringan()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailJaringan)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-detail-jaringan';
        $delete_url = 'delete-upload-bg-detail-jaringan';

        return view('admin/upload-bg-detail-jaringan', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgDetailJaringan(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailJaringan)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDetailJaringan');
        }

        $tipe = $this->bgDetailJaringan;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgDetailJaringan);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgDetailJaringan')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgDetailJaringan($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailJaringan)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDetailJaringan');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgDetailJaringan)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgProduk()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgProduk)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-produk';
        $delete_url = 'delete-upload-bg-produk';

        return view('admin/upload-bg-produk', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgProduk(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgProduk)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgProduk');
        }

        $tipe = $this->bgProduk;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgProduk);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgProduk')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgProduk($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgProduk)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgProduk');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgProduk)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgHeaderProduk()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderProduk)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-header-produk';
        $delete_url = 'delete-upload-bg-header-produk';

        return view('admin/upload-bg-header-produk', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgHeaderProduk(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderProduk)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgHeaderProduk');
        }

        $tipe = $this->bgHeaderProduk;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgHeaderProduk);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgHeaderProduk')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgHeaderProduk($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderProduk)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgHeaderProduk');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgHeaderProduk)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgDetailProduk()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailProduk)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-detail-produk';
        $delete_url = 'delete-upload-bg-detail-produk';

        return view('admin/upload-bg-detail-produk', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgDetailProduk(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailProduk)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDetailProduk');
        }

        $tipe = $this->bgDetailProduk;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgDetailProduk);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgDetailProduk')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgDetailProduk($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailProduk)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDetailProduk');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgDetailProduk)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgHeaderBerita()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderBerita)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-header-berita';
        $delete_url = 'delete-upload-bg-header-berita';

        return view('admin/upload-bg-header-berita', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgHeaderBerita(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderBerita)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgHeaderBerita');
        }

        $tipe = $this->bgHeaderBerita;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgHeaderBerita);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgHeaderBerita')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgHeaderBerita($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgHeaderBerita)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgHeaderBerita');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgHeaderBerita)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgBerita()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgBerita)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-berita';
        $delete_url = 'delete-upload-bg-berita';

        return view('admin/upload-bg-berita', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgBerita(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgBerita)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgBerita');
        }

        $tipe = $this->bgBerita;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgBerita);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgBerita')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgBerita($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgBerita)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgBerita');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgBerita)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgDetailBerita()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailBerita)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-detail-berita';
        $delete_url = 'delete-upload-bg-detail-berita';

        return view('admin/upload-bg-detail-berita', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgDetailBerita(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailBerita)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDetailBerita');
        }

        $tipe = $this->bgDetailBerita;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgDetailBerita);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgDetailBerita')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgDetailBerita($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDetailBerita)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDetailBerita');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgDetailBerita)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function getUploadBgDownload()
    {
        $add_button = true;
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDownload)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
        }

        $redirect_url = 'upload-bg-download';
        $delete_url = 'delete-upload-bg-download';

        return view('admin/upload-bg-download', ['users' => Auth::user(), 'file' => $data, 'delete_url' => $delete_url, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function postAddUploadBgDownload(AddBackgroundImageRequest $request)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDownload)->where('delete', 0)->get();

        if(count($data) > 0)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDownload');
        }

        $tipe = $this->bgDownload;
        $file = $request->file('file');
        $path = $this->createFile($file, $this->bgDownload);
        
        $data = BackgroundImagesModel::create([
            'path' => $path,
            'tipe' => $tipe,
            'delete' => 0
        ]);

        return redirect()->route('getUploadBgDownload')->with(['done' => 'Gambar berhasil di tambahkan.'] );
    }

    public function postDeleteUploadBgDownload($id)
    {
        $data = BackgroundImagesModel::select('id', 'path', 'created_at')->where('tipe', $this->bgDownload)->where('delete', 0)->get();

        if(count($data) > 1)
        {
            $add_button = false;
            return redirect()->route('getUploadBgDownload');
        }

        BackgroundImagesModel::where('id', $id)->where('tipe', $this->bgDownload)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }

    //-------------------------------------------------------------------------------------------------------------------

    public function createFile($file, $path)
    {
        $path = '/public/'.$path.'/'; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$file->guessExtension();
        $file->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public function deleteFile($oldpath)
    {
        $oldpath = getcwd().$oldpath;
        
        unlink(realpath($oldpath));
    }
}
