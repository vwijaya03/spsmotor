<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddSettingRequest;
use App\Http\Requests\EditSettingRequest;
use App\SettingModel;
use Auth, Hash, DB, Log;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAddSetting()
    {
        $check_setting = SettingModel::select('id')->where('delete', 0)->first();

        if(count($check_setting) > 0)
        {
        	return redirect()->route('getEditSetting', ['id' => $check_setting->id])->with(['done' => 'Data setting sudah ada.'] );
        }

        return view('admin/add-setting', ['users' => Auth::user()]);
    }

    public function postAddSetting(AddSettingRequest $request)
    {
    	$check_setting = SettingModel::select('id')->where('delete', 0)->first();

        if(count($check_setting) > 0)
        {
        	return redirect()->route('getEditSetting', ['id' => $check_setting->id])->with(['done' => 'Data setting sudah ada.'] );
        }

        $address = $request->get('address');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $no_fax = $request->get('no_fax');
        $informasi_hari_dan_jam = $request->get('informasi_hari_dan_jam');
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        
        $data = SettingModel::create([
            'address' => $address,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'no_fax' => $no_fax,
            'informasi_hari_dan_jam' => $informasi_hari_dan_jam,
            'lat' => $lat,
            'lng' => $lng,
            'delete' => 0
        ]);

        return redirect()->route('getEditSetting', ['id' => $data->id])->with(['done' => 'Pengaturan berhasil.'] );
    }

    public function getEditSetting($id)
    {
        $setting = SettingModel::select('id', 'address', 'email', 'no_telepon', 'no_fax', 'informasi_hari_dan_jam', 'lat', 'lng')->where('id', $id)->first();

        if($setting == null)
        {
            return redirect()->route('getAddSetting');
        }

        return view('admin/edit-setting', ['users' => Auth::user(), 'data' => $setting]);
    }

    public function postEditSetting(EditSettingRequest $request, $id)
    {
        $address = $request->get('address');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $no_fax = $request->get('no_fax');
        $informasi_hari_dan_jam = $request->get('informasi_hari_dan_jam');
        $lat = $request->get('lat');
        $lng = $request->get('lng');

        SettingModel::where('id', $id)->where('delete', 0)
        ->update([
            'address' => $address,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'no_fax' => $no_fax,
            'informasi_hari_dan_jam' => $informasi_hari_dan_jam,
            'lat' => $lat,
            'lng' => $lng
        ]);

        return redirect()->route('getEditSetting', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteCabang($id)
    {
        SettingModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }
}
