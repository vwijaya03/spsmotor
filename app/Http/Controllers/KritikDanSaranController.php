<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddKritikDanSaranRequest;
use App\KritikDanSaranModel;
use App\CabangModel;
use Auth, Hash, DB, Log;

class KritikDanSaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaKritikDanSaran()
    {
    	$data = KritikDanSaranModel::select('cabang.name as cabang_name', 'kritik_dan_saran.id', 'kritik_dan_saran.fullname', 'kritik_dan_saran.email', 'kritik_dan_saran.no_telepon', 'kritik_dan_saran.pesan', 'kritik_dan_saran.date_time')
    	->join('cabang', 'cabang.id', '=', 'kritik_dan_saran.cabang_id')
    	->where('kritik_dan_saran.delete', 0)
    	->where('cabang.delete', 0)
    	->get();

    	return view('admin/kritik-dan-saran', ['users' => Auth::user(), 'kritik_dan_saran' => $data]);
    }

    public function postDeleteKritikDanSaran($id)
    {
    	KritikDanSaranModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        $arr['message'] = 'success';
        return json_encode($arr);
    }
}
