<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\UserModel;
use Auth, Hash, DB, Log, Carbon;

class LoginController extends Controller
{ 
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getLogin', 'postLogin']]);
    }

    public function getLogin()
    {
        if(Auth::check())
        {
            return redirect()->route('getDashboard');
        }

        return view('admin/login');
    }

    public function postLogin(LoginRequest $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');

        if(Auth::attempt(['username' => $username, 'password' => $password, 'delete' => 0]))
        {
            return redirect()->route('getDashboard');
        }
        else if(!Auth::attempt(['username' => $username, 'password' => $password, 'delete' => 0]))
        {
            return redirect()->route('getLogin')->with(['err' => 'Username or password is incorrect.']);
        }
        else
        {
            return redirect()->route('getLogin')->with(['err' => 'You are not allowed to access this area.']);
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('getLogin');
    }

    public function getSignUp()
    {
        return view('register');
    }

    public function postSignUp(RegisterRequest $request)
    {
        $data = DB::table('Token')
        ->select('token')
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->first();

        if($data != null)
        {
            $default_secret_token = $data->token;

            $secret_token = $request->get('secret_token');
            $nama = $request->get('nama'); 
            $alamat = $request->get('alamat'); 
            $username = $request->get('username'); 
            $password = Hash::make($request->get('password')); 
            $phone = $request->get('phone'); 
            $email = $request->get('email'); 
            $joinAt = $request->get('joinAt'); 
            $idType = $request->get('idType'); 
            $idCard = $request->get('idCard'); 
            $gaji = $request->get('gaji'); 
            $jabatan = $request->get('jabatan'); 
            $dob = $request->get('dob');

            $removed_idr = str_replace("IDR ", "", $gaji);
            $removed_dot = str_replace(".", "", $removed_idr);

            if($secret_token != $default_secret_token)
            {
                return redirect()->route('getSignUp')->with(['err' => 'Token salah.']);
            }

            Karyawan::create
            ([
                'nama' => $nama,
                'username' => $username,
                'email' => $email,
                'password' => $password,
                'alamat' => $alamat,
                'phone' => $phone,
                'dob' => $dob,  
                'joinAt' => $joinAt,
                'idType' => $idType,
                'idCard' => $idCard,
                'gaji' => $removed_dot,
                'jabatan' => $jabatan,
            ]);

            return redirect()->route('login');
        }
        else
        {
            return redirect()->route('getSignUp')->with(['err' => 'Token salah.']);
        }
    }
}
