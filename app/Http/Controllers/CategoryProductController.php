<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddCategoryProductRequest;
use App\Http\Requests\EditCategoryProductRequest;
use App\CategoryProductModel;
use Auth, Hash, DB, Log, Carbon;

class CategoryProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSemuaCategoryProduct()
    {
        $data = CategoryProductModel::select('id', 'slug', 'name')->where('delete', 0)->get();

        return view('admin/category-product', ['users' => Auth::user(), 'category_product' => $data]);
    }

    public function getAddCategoryProduct()
    {
        return view('admin/add-category-product', ['users' => Auth::user()]);
    }

    public function postAddCategoryProduct(AddCategoryProductRequest $request)
    {
        $category_product_name = $request->get('name');
        $category_product_name_slug = str_slug($category_product_name, '-');
        
        CategoryProductModel::create([
            'slug' => $category_product_name_slug,
            'name' => $category_product_name,
            'delete' => 0
        ]);

        return redirect()->route('getEditCategoryProduct', ['slug' => $category_product_name_slug])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditCategoryProduct($slug)
    {
        $product = CategoryProductModel::select('name', 'slug')->where('slug', $slug)->where('delete', 0)->first();

        if($product == null)
        {
            return redirect()->route('getSemuaCategoryProduct');
        }

        return view('admin/edit-category-product', ['users' => Auth::user(), 'product' => $product]);
    }

    public function postEditCategoryProduct(EditCategoryProductRequest $request, $slug)
    {
        $category_product_name = $request->get('name');
        $category_product_name_slug = str_slug($category_product_name, '-');

        CategoryProductModel::where('slug', $slug)->where('delete', 0)
        ->update([
            'slug' => $category_product_name_slug,
            'name' => $category_product_name,
        ]);

        return redirect()->route('getEditCategoryProduct', ['slug' => $category_product_name_slug])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteCategoryProduct($slug)
    {
        CategoryProductModel::where('slug', $slug)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );
        // $arr['delete_message'] = $id;
        $arr['message'] = 'success';
        // \Log::info(json_encode($arr));
        return json_encode($arr);
    }
}
