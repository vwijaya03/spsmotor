<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\HubungiKamiMail;
use App\Mail\ResetPasswordMail;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use App\UserModel;
use Auth, Hash, DB, Log;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getResetPassword', 'postResetPassword']]);
    }

    public function getUser()
    {
        $delete_route = 'delete-user/';
        $redirect_url = 'user';

        $data_users = UserModel::select('id', 'fullname', 'username', 'email', 'password', 'phone')->where('delete', 0)->get();

        return view('admin/user', ['users' => Auth::user(), 'data_users' => $data_users, 'delete_route' => $delete_route, 'redirect_url' => $redirect_url]);
    }

    public function getAddUser()
    {
        return view('admin/add-user', ['users' => Auth::user()]);
    }

    public function postAddUser(AddUserRequest $request)
    {
    	$data = '';

		$data = UserModel::create([
            'fullname' => $request->get('fullname'),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'jabatan' => $request->get('jabatan'),
            'phone' => $request->get('phone'),
            'delete' => 0
        ]);
    	
        return redirect()->route('getEditUser', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditUser($id)
    {
        $data = UserModel::select('id', 'fullname', 'username', 'email', 'phone')->where('id', $id)->where('delete', 0)->first();

    	return view('admin/edit-user', ['users' => Auth::user(), 'data' => $data]);
    }

    public function postEditUser(EditUserRequest $request, $id)
    {	
    	$data = '';

        if ($request->has('password')) 
        {   
            UserModel::where('id', $id)->where('delete', 0)
            ->update([
                'fullname' => $request->get('fullname'),
                'username' => $request->get('username'),
                'email' => $request->get('email'),
	            'password' => Hash::make($request->get('password')),
	            'phone' => $request->get('phone')
            ]);
        }
        else 
        {
            UserModel::where('id', $id)->where('delete', 0)
            ->update([
                'fullname' => $request->get('fullname'),
                'username' => $request->get('username'),
	            'email' => $request->get('email'),
	            'phone' => $request->get('phone')
            ]);
        }

        return redirect()->route('getEditUser', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteUser($id)
    {
    	UserModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );

        $arr['message'] = 'success';

        return json_encode($arr);
    }

    public function createImage($img)
    {
        $path = '/public/user-image/'; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$img->guessExtension();
        $img->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public function deleteImage($oldpath)
    {
        $oldpath = getcwd().$oldpath;
        
        unlink(realpath($oldpath));
    }

    public function postChangePassword(Request $request)
    {
        if(Auth::user()->delete == "1" || Auth::user()->delete == 1)
        {   
            return redirect()->route('getLogin')->with(['err' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.']);
        }

        $current_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        $email = Auth::user()->email;

        if(Hash::check($current_password, Auth::user()->password))
        {
            Log::info('masuk if');
            UserModel::where('email', $email)->where('delete', 0)
            ->update([
                'password' => Hash::make($new_password)
            ]);
            return redirect()->route('getChangePassword')->with(['done' => 'Password berhasil di ubah.']);
        }
        else
        {
        	return redirect()->route('getChangePassword')->with(['err' => 'Password gagal di ubah.']);
        }
    }

    public function getChangePassword()
    {
    	return view('admin/change-password', ['users' => Auth::user()]);
    }

    public function getResetPassword()
    {
    	return view('admin/reset-password', ['users' => Auth::user()]);
    }

    public function postResetPassword(Request $request)
    {
    	$faker = \Faker\Factory::create();
        $reset_password = $faker->randomNumber($nbDigits = 6, $strict = false);
        $msg = '';
        $status_json = '';

        $email = $request->get('email');
        Log::info('new password: '.$reset_password);

        $result = UserModel::select('id', 'email')->where('email', $email)->where('delete', 0)->first();

        if($result != null)
        {
            UserModel::where('email', $email)->where('delete', 0)
            ->update([
                'password' => Hash::make($reset_password)
            ]);

            $to_email = 'kang.raditya23@gmail.com';

            Mail::to($to_email)->send(new ResetPasswordMail($reset_password));
            $msg = 'Password Baru Anda Berhasil Di Kirim Ke Email.';

            return redirect()->route('getResetPassword')->with(['done' => $msg]);
        }
        else 
        {
        	return redirect()->route('getResetPassword')->with(['err' => 'Data user tidak ditemukan.']);
        }
    }
}
