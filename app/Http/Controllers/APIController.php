<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddCommentNewsRequest;
use App\Http\Requests\AddCommentProductRequest;
use App\Http\Requests\AddHubungiKamiRequest;
use App\Http\Requests\AddFrontEndBookingServiceRequest;
use App\CabangModel;
use App\CategoryCabangModel;
use App\CategoryProductModel;
use App\ProductModel;
use App\ProductImageModel;
use App\CommentModel;
use App\DownloadBrosurDanSukuCadangModel;
use App\KarirModel;
use App\GalleryCabangModel;
use App\NewsModel;
use App\KritikDanSaranModel;
use App\ServiceBookingModel;
use App\SettingModel;
use App\SpsProfileModel;
use App\SpsYoutubeModel;
use App\SubscribeMailModel;

class APIController extends Controller
{
    public function getHome()
    {
        $product = ProductModel::select('category_product.name as category_name', 'product.id', 'product.name', 'product.slug', 'product_image.path', 'product_image.updated_at')
        ->join('product_image', 'product_image.product_id', '=', 'product.id')
        ->join('category_product', 'category_product.id', '=', 'product.category_id')
        ->where('product.delete', 0)
        ->where('product_image.set_image', 1)
        ->where('product_image.delete', 0)
        ->orderBy('product_image.updated_at', 'desc')
        ->limit(5)
        ->get();

        $news = NewsModel::select('slug', 'title', 'description', 'img_path')->where('delete', 0)->orderBy('date_time', 'desc')->get();

        return view('front-end/home', ['product' => $product, 'news' => $news, 'mListTable' => 0]);
    }

    public function getTentangKami()
    {
        $profile = SpsProfileModel::select('description')->where('delete', 0)->first();

        return response()->json(['result' => $profile]);
    }

    public function getGalleryCabangFrontEnd()
    {
        $cabang = CabangModel::select('id', 'slug', 'name', 'description', 'img_path')->where('delete', 0)->get();

        return response()->json(['result' => $cabang]);
    }

    public function getDetailGalleryCabangFrontEnd($slug)
    {
        $cabang = CabangModel::select('id', 'slug', 'name')->where('slug', $slug)->where('delete', 0)->first();

        if($cabang == null)
        {
            return redirect()->route('getGalleryCabangFrontEnd');
        }

        $gallery_cabang = GalleryCabangModel::select('title', 'description', 'img_path')->where('cabang_id', $cabang->id)->where('delete', 0)->get();

        // return view('front-end/detail-gallery-cabang', ['gallery_cabang' => $gallery_cabang, 'cabang' => $cabang, 'mListTable' => 1]);

        return response()->json(['result' => $gallery_cabang, 'result_cabang' => $cabang]);
    }

    public function getSpsYoutube()
    {
        $sps_youtube = SpsYoutubeModel::select('id', 'title', 'url')
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->get();

        // return view('front-end/sps-youtube', ['sps_youtube' => $sps_youtube, 'mListTable' => 1]);

        return response()->json(['result' => $sps_youtube]);
    }

    public function getDetailJaringan($slug)
    {
        $content_category_cabang = CategoryCabangModel::select('id', 'slug', 'name')->where('slug', $slug)->where('delete', 0)->first();

        if($content_category_cabang == null)
        {
            return redirect()->route('getHome');
        }

        $content_cabang = CabangModel::select('id', 'slug', 'name', 'description', 'img_path')->where('category_cabang_id', $content_category_cabang->id)->where('delete', 0)->get();

        // return view('front-end/detail-jaringan', ['content_category_cabang' => $content_category_cabang, 'content_cabang' => $content_cabang, 'mListTable' => 1]);

        return response()->json(['result' => $content_cabang, 'result_category_cabang' => $content_category_cabang]);
    }

    public function getCabang()
    {
        $cabang = CabangModel::select('id', 'slug', 'name', 'description', 'img_path', 'lat', 'lng')->where('delete', 0)->get();

        // return view('front-end/cabang', ['mListTable' => 1, 'cabang' => $cabang]);
        return response()->json(['result' => $cabang]);
    }

    public function getDetailCabang($slug)
    {
        $cabang = CabangModel::select('id', 'slug', 'name', 'description', 'img_path', 'lat', 'lng')->where('slug', $slug)->where('delete', 0)->first();

        if($cabang == null)
        {
            return redirect()->route('getCabang');
        }

        // return view('front-end/detail-cabang', ['mListTable' => 0, 'cabang' => $cabang]);
        return response()->json(['result' => $cabang]);
    }

    public function getAllKategoriProduk()
    {
        $category_product = CategoryProductModel::select('id', 'name', 'slug')->where('delete', 0)->orderBy('name', 'asc')->get();

        // return view('front-end/product-by-category', ['mListTable' => 1, 'content_product' => $product, 'content_category_product' => $category_product]);

        return response()->json(['result' => $category_product]);
    }

    public function getProdukByKategori($id)
    {
        $category_product = CategoryProductModel::select('id', 'name', 'slug')->where('id', $id)->where('delete', 0)->first();

        if($category_product == null)
        {
            return response()->json(['result' => [], 'result_category_product' => []]);
        }

        $product = ProductModel::select('product_image.path', 'product.id', 'product.slug', 'product.name', 'product.description')
        ->join('product_image', 'product_image.product_id', '=', 'product.id')
        ->where('product.category_id', $category_product->id)
        ->where('product_image.set_image', 1)
        ->where('product.delete', 0)
        ->where('product_image.delete', 0)
        ->get();

        // return view('front-end/product-by-category', ['mListTable' => 1, 'content_product' => $product, 'content_category_product' => $category_product]);

        return response()->json(['result' => $product, 'result_category_product' => $category_product]);
    }

    public function getProduk()
    {
        $product = ProductModel::select('product_image.path', 'product.id', 'product.slug', 'product.name', 'product.description')
        ->join('product_image', 'product_image.product_id', '=', 'product.id')
        ->where('product_image.set_image', 1)
        ->where('product.delete', 0)
        ->where('product_image.delete', 0)
        ->get();

        // return view('front-end/product', ['mListTable' => 1, 'content_product' => $product]);
        return response()->json(['result' => $product]);
    }

    public function getProdukDetail($slug)
    {
        $product = ProductModel::select('id', 'slug', 'name', 'description')->where('slug', $slug)->where('delete', 0)->first();

        if($product == null)
        {
            return response()->json(['result_product' => [], 'result_product_image' => [] ]);
        }

        $product_image = ProductImageModel::select('path')->where('product_id', $product->id)->where('delete', 0)->get();

        $comment = CommentModel::select('comment_fullname', 'comment_email', 'comment_description', 'date_time', 'role')
        ->where('product_or_news_id', $product->id)
        ->where('tipe', 'product')
        ->where('approval', 1)
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->get();

        // return view('front-end/detail_product', ['mListTable' => 0, 'content_product' => $product, 'content_product_image' => $product_image, 'comment' => $comment]);
        return response()->json(['result_product' => $product, 'result_product_image' => $product_image]);
    }

    public function getBrosur()
    {
        $brosur = DownloadBrosurDanSukuCadangModel::select('id', 'title', 'path')->where('tipe', 0)->where('delete', 0)->get();

        // return view('front-end/brosur', ['mListTable' => 1, 'brosur' => $brosur]);
        return response()->json(['result' => $brosur]);
    }

    public function getKatalog()
    {
        $katalog = DownloadBrosurDanSukuCadangModel::select('id', 'title', 'path')->where('tipe', 1)->where('delete', 0)->get();

        // return view('front-end/katalog', ['mListTable' => 1, 'katalog' => $katalog]);
        return response()->json(['result' => $katalog]);
    }

    public function getLayananKarir()
    {
        $karir = KarirModel::select('description')->where('delete', 0)->first();

        // return view('front-end/karir', ['mListTable' => 0, 'karir' => $karir]);
        return response()->json(['result' => $karir]);
    }

    public function postCommentProduct(AddCommentProductRequest $request)
    {
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $description = $request->get('description');
        $product_id = $request->get('product_id');

        $slug = $request->get('product_slug');

        CommentModel::create([
            'tipe' => 'product',
            'product_or_news_id' => $product_id,
            'comment_fullname' => $fullname,
            'comment_email' => $email,
            'comment_description' => $description,
            'date_time' => date('Y-m-d H:i:s'),
            'role' => '0',
            'approval' => 0,
            'delete' => 0
        ]);

        return redirect()->route('getProdukDetail', ['slug' => $slug])->with(['done' => 'Terima kasih, komentar anda sedang di review.'] );
    }

    public function getNews()
    {
        $news = NewsModel::select('id', 'slug', 'title', 'description', 'img_path', 'date_time')
        ->where('delete', 0)
        ->orderBy('date_time', 'desc')
        ->paginate(4);

        return response()->json(['result' => $news]);
    }

    public function getNewsDetail($slug)
    {
        $news = NewsModel::select('id', 'slug', 'title', 'description', 'img_path', 'date_time')
        ->where('slug', $slug)
        ->where('delete', 0)
        ->first();

        if($news == null)
        {
            return response()->json(['result' => []]);
        }

        // return view('front-end/detail_news', ['mListTable' => 0, 'news' => $news]);
        return response()->json(['result' => $news]);
    }

    public function postCommentNews(AddCommentNewsRequest $request)
    {
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $description = $request->get('description');
        $news_id = $request->get('news_id');

        CommentModel::create([
            'tipe' => 'product',
            'product_or_news_id' => $product_id,
            'comment_fullname' => $fullname,
            'comment_email' => $email,
            'comment_description' => $description,
            'date_time' => date('Y-m-d H:i:s'),
            'role' => '0',
            'approval' => 0,
            'delete' => 0
        ]);
    }

    public function getHubungiKami()
    {
        $cabang = CabangModel::select('id', 'name')->where('delete', 0)->get();

        return view('front-end/hubungi-kami', ['mListTable' => 0, 'cabang' => $cabang]);
    }

    public function postHubungiKami(AddHubungiKamiRequest $request)
    {
        $cabang_id = $request->get('cabang_id');
        $fullname = $request->get('fullname');
        $email = $request->get('email');
        $no_telepon = $request->get('no_telepon');
        $description = $request->get('description');

        KritikDanSaranModel::create([
            'cabang_id' => $cabang_id,
            'fullname' => $fullname,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'pesan' => $description,
            'date_time' => date('Y-m-d H:i:s'),
            'delete' => 0
        ]);

        $data = CabangModel::select('name', 'email_customer_service_cabang')->where('id', $cabang_id)->where('delete', 0)->first();

        $sender = "vwijaya08@gmail.com";

        $subject = "Kritik / Saran Dari Customer";

        $htmlContent = '
            <html>
            <head>
                <title>Kritik / Saran Dari Customer</title>
            </head>
            <body>
                <h1>Kritik / Saran Dari Customer Untuk '.$data->name.'</h1>
                <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                    <tr>
                        <th>Nama Lengkap:</th><td>'.$fullname.'</td>
                    </tr>
                    <tr>
                        <th>Email Pelanggan:</th><td>'.$email.'</td>
                    </tr>
                    <tr>
                        <th>No. Telepon Pelanggan:</th><td>'.$no_telepon.'</td>
                    </tr>
                    <tr style="background-color: #e0e0e0;">
                        <th>Kritik / Saran:</th><td>'.$description.'</td>
                    </tr>
                </table>
            </body>
            </html>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: SPS Motor HO <'.$sender.'>' . "\r\n";
        $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

        $check = mail($data->email_customer_service_cabang, 'Kritik / Saran Dari Customer', $htmlContent, $headers);

        if($check)
            $msg = 'Kritik / Saran Anda Berhasil Di Kirim.';
        else
            $msg = 'Gagal';

        return redirect()->route('getHubungiKami')->with(['done' => $msg] );
    }
}
