<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImageModel extends Model
{
    protected $table = 'product_image';
    protected $primaryKey = 'id';
    protected $fillable = ['product_id', 'set_image', 'path', 'delete'];
}
