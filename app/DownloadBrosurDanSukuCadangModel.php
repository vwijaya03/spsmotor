<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DownloadBrosurDanSukuCadangModel extends Model
{
    protected $table = 'download_brosur_dan_suku_cadang';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'title', 'path', 'file_size', 'tipe', 'date_time', 'delete'];
}
