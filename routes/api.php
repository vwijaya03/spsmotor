<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// API
Route::get('/', 'APIController@getHome')->name('getHome');
Route::get('/index', 'APIController@getHome')->name('getHome');
Route::get('/home', 'APIController@getHome')->name('getHome');
Route::get('/tentang-kami', 'APIController@getTentangKami')->name('getTentangKami');
Route::get('/gallery-cabang', 'APIController@getGalleryCabangFrontEnd')->name('getGalleryCabangFrontEnd');
Route::get('/gallery-cabang/{slug}', 'APIController@getDetailGalleryCabangFrontEnd')->name('getDetailGalleryCabangFrontEnd');
Route::get('/sps-youtube', 'APIController@getSpsYoutube')->name('getSpsYoutube');

Route::get('/jaringan/{slug}', 'APIController@getDetailJaringan')->name('getDetailJaringan');

Route::get('/cabang', 'APIController@getCabang')->name('getCabang');
Route::get('/cabang/{slug}', 'APIController@getDetailCabang')->name('getDetailCabang');

Route::get('/news', 'APIController@getNews')->name('getNews');
Route::get('/news/{slug}', 'APIController@getNewsDetail')->name('getNewsDetail');

Route::get('/kategori', 'APIController@getAllKategoriProduk')->name('getAllKategoriProduk');
Route::get('/kategori-produk/{id}', 'APIController@getProdukByKategori')->name('getProdukByKategori');
Route::get('/produk', 'APIController@getProduk')->name('getProduk');
Route::get('/produk/{slug}', 'APIController@getProdukDetail')->name('getProdukDetail');

Route::get('/brosur', 'APIController@getBrosur')->name('getBrosur');
Route::get('/katalog', 'APIController@getKatalog')->name('getKatalog');

Route::get('/layanan-karir', 'APIController@getLayananKarir')->name('getLayananKarir');

Route::get('/hubungi-kami', 'APIController@getHubungiKami')->name('getHubungiKami');
Route::post('/hubungi-kami', 'APIController@postHubungiKami')->name('postHubungiKami');

Route::post('/comment-product', 'APIController@postCommentProduct')->name('postCommentProduct');
Route::post('/comment-news', 'APIController@postCommentNews')->name('postCommentNews');