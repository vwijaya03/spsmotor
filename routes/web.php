<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front End Initialize
// Route::get('/', 'FrontEndInitController@getHome')->name('getHome');
// Route::get('/index', 'FrontEndInitController@getHome')->name('getHome');
// Route::get('/home', 'FrontEndInitController@getHome')->name('getHome');
// Route::get('/listing', 'FrontEndInitController@getListing')->name('getListing');
// Route::get('/detail', 'FrontEndInitController@getDetail')->name('getDetail');
// Route::get('/listing-table', 'FrontEndInitController@getListingTable')->name('getListingTable');
// Route::get('/about', 'FrontEndInitController@getAbout')->name('getAbout');
// Route::get('/article', 'FrontEndInitController@getArticle')->name('getArticle');
// Route::get('/blog', 'FrontEndInitController@getBlog')->name('getBlog');
// Route::get('/contact', 'FrontEndInitController@getContact')->name('getContact');
// Route::get('/404', 'FrontEndInitController@get404')->name('get404');
// Route::get('/submit1', 'FrontEndInitController@getSubmit1')->name('getSubmit1');
// Route::get('/submit2', 'FrontEndInitController@getSubmit2')->name('getSubmit2');
// Route::get('/submit3', 'FrontEndInitController@getSubmit3')->name('getSubmit3');
// Route::get('/submit4', 'FrontEndInitController@getSubmit4')->name('getSubmit4');
// Route::get('/submit5', 'FrontEndInitController@getSubmit5')->name('getSubmit5');

// Front End
Route::get('/', 'FrontEndController@getHome')->name('getHome');
Route::get('/index', 'FrontEndController@getHome')->name('getHome');
Route::get('/home', 'FrontEndController@getHome')->name('getHome');

Route::get('/tentang-kami', 'FrontEndController@getTentangKami')->name('getTentangKami');
Route::get('/tentang-kami-api', 'FrontEndController@getTentangKamiApi')->name('getTentangKamiApi');

Route::get('/gallery-cabang', 'FrontEndController@getGalleryCabangFrontEnd')->name('getGalleryCabangFrontEnd');
Route::get('/gallery-cabang/{slug}', 'FrontEndController@getDetailGalleryCabangFrontEnd')->name('getDetailGalleryCabangFrontEnd');

Route::get('/gallery-cabang-api', 'FrontEndController@getGalleryCabangApi')->name('getGalleryCabangApi');
Route::get('/gallery-cabang-api/{slug}', 'FrontEndController@getDetailGalleryCabangApi')->name('getDetailGalleryCabangApi');

Route::get('/sps-youtube', 'FrontEndController@getSpsYoutube')->name('getSpsYoutube');

Route::get('/jaringan/{slug}', 'FrontEndController@getDetailJaringan')->name('getDetailJaringan');

Route::get('/cabang', 'FrontEndController@getCabang')->name('getCabang');
Route::get('/cabang/{slug}', 'FrontEndController@getDetailCabang')->name('getDetailCabang');

Route::get('/news', 'FrontEndController@getNews')->name('getNews');
Route::get('/news/{slug}', 'FrontEndController@getNewsDetail')->name('getNewsDetail');

Route::get('/kategori-produk/{slug}', 'FrontEndController@getKategoriProduk')->name('getKategoriProduk');
Route::get('/kategori-produk-api/{id}', 'FrontEndController@getKategoriProdukApi')->name('getKategoriProdukApi');

// Route::get('/produk', 'FrontEndController@getProduk')->name('getProduk');
Route::get('/produk/{slug}', 'FrontEndController@getProdukDetail')->name('getProdukDetail');
Route::get('/produk-api/{slug}', 'FrontEndController@getProdukDetailApi')->name('getProdukDetailApi');

Route::get('/brosur', 'FrontEndController@getBrosur')->name('getBrosur');
Route::get('/cari-brosur', 'FrontEndController@getCariBrosur')->name('getCariBrosur');
Route::get('/cari-brosur/{title}', 'FrontEndController@getCariBrosurByTitle')->name('getCariBrosurByTitle');

Route::get('/katalog', 'FrontEndController@getKatalog')->name('getKatalog');
Route::get('/katalog/{param}', 'FrontEndController@getCariKatalog')->name('getCariKatalog');
Route::get('/cari-katalog', 'FrontEndController@getCariKatalog')->name('getCariKatalog');
Route::get('/cari-katalog/{title}', 'FrontEndController@getCariKatalogByTitle')->name('getCariKatalogByTitle');

Route::get('/layanan-service-booking', 'FrontEndController@getLayananServiceBooking')->name('getLayananServiceBooking');
Route::post('/layanan-service-booking', 'FrontEndController@postLayananServiceBooking')->name('postLayananServiceBooking');

Route::get('/layanan-service-booking-api', 'FrontEndController@getLayananServiceBookingApi')->name('getLayananServiceBookingApi');
Route::post('/layanan-service-booking-api', 'FrontEndController@postLayananServiceBookingApi')->name('postLayananServiceBookingApi');

Route::get('/layanan-karir', 'FrontEndController@getLayananKarir')->name('getLayananKarir');

Route::get('/hubungi-kami', 'FrontEndController@getHubungiKami')->name('getHubungiKami');
Route::get('/hubungi-kami/{slug}', 'FrontEndController@getCabangHubungiKami')->name('getCabangHubungiKami');
Route::post('/hubungi-kami', 'FrontEndController@postHubungiKami')->name('postHubungiKami');

Route::get('/hubungi-kami-api', 'FrontEndController@getHubungiKamiApi')->name('getHubungiKamiApi');
Route::post('/hubungi-kami-api', 'FrontEndController@postHubungiKamiApi')->name('postHubungiKamiApi');
Route::get('/hubungi-kami-api/{slug}', 'FrontEndController@getCabangHubungiKamiApi')->name('getCabangHubungiKamiApi');

Route::post('/comment-product', 'FrontEndController@postCommentProduct')->name('postCommentProduct');
Route::post('/comment-news', 'FrontEndController@postCommentNews')->name('postCommentNews');

Route::get('/subscribe_success', 'FrontEndController@getSubscribeSuccess')->name('getSubscribeSuccess');
Route::post('/subscribe', 'FrontEndController@postSubscribe')->name('postSubscribe');

Route::get('/layanan-pesan-sparepart', 'FrontEndController@getPesanSparepart')->name('getPesanSparepart');
Route::post('/layanan-pesan-sparepart', 'FrontEndController@postPesanSparepart')->name('postPesanSparepart');

Route::get('/layanan-pesan-sparepart-api', 'FrontEndController@getPesanSparepartApi')->name('getPesanSparepartApi');
Route::post('/layanan-pesan-sparepart-api', 'FrontEndController@postPesanSparepartApi')->name('postPesanSparepartApi');

// Admin Back End
Route::group(['prefix' => '/admin/backend/'], function () {

	// Upload Background Layanan Di SPS Motor Home
	Route::get('/upload-bg-layanan-spsmotor-home', 'BackgroundImageController@getUploadBgLayananSpsmotorHome')->name('getUploadBgLayananSpsmotorHome'); 
	Route::post('/add-upload-bg-layanan-spsmotor-home', 'BackgroundImageController@postAddUploadBgLayananSpsmotorHome')->name('postAddUploadBgLayananSpsmotorHome'); 
	Route::post('/delete-upload-bg-layanan-spsmotor-home/{id}', 'BackgroundImageController@postDeleteUploadBgLayananSpsmotorHome')->name('postDeleteUploadBgLayananSpsmotorHome');

	// Upload Background Berita Terbaru Home
	Route::get('/upload-bg-berita-terbaru-home', 'BackgroundImageController@getUploadBgBeritaTerbaruHome')->name('getUploadBgBeritaTerbaruHome'); 
	Route::post('/add-upload-bg-berita-terbaru-home', 'BackgroundImageController@postAddUploadBgBeritaTerbaruHome')->name('postAddUploadBgBeritaTerbaruHome'); 
	Route::post('/delete-upload-bg-berita-terbaru-home/{id}', 'BackgroundImageController@postDeleteUploadBgBeritaTerbaruHome')->name('postDeleteUploadBgBeritaTerbaruHome');

	// Upload Background Langganan Berita Home
	Route::get('/upload-bg-langganan-berita-home', 'BackgroundImageController@getUploadBgLanggananBeritaHome')->name('getUploadBgLanggananBeritaHome'); 
	Route::post('/add-upload-bg-langganan-berita-home', 'BackgroundImageController@postAddUploadBgLanggananBeritaHome')->name('postAddUploadBgLanggananBeritaHome'); 
	Route::post('/delete-upload-bg-langganan-berita-home/{id}', 'BackgroundImageController@postDeleteUploadBgLanggananBeritaHome')->name('postDeleteUploadBgLanggananBeritaHome'); 

	// Upload Popup Home
	Route::get('/upload-popup-home', 'BackgroundImageController@getUploadPopupHome')->name('getUploadPopupHome'); 
	Route::post('/add-upload-popup-home', 'BackgroundImageController@postAddUploadPopupHome')->name('postAddUploadPopupHome'); 
	Route::post('/delete-upload-popup-home/{id}', 'BackgroundImageController@postDeleteUploadPopupHome')->name('postDeleteUploadPopupHome'); 

	// Upload Background Header Sejarah
	Route::get('/upload-bg-header-sejarah', 'BackgroundImageController@getUploadBgHeaderSejarah')->name('getUploadBgHeaderSejarah'); 
	Route::post('/add-upload-bg-header-sejarah', 'BackgroundImageController@postAddUploadBgHeaderSejarah')->name('postAddUploadBgHeaderSejarah'); 
	Route::post('/delete-upload-bg-header-sejarah/{id}', 'BackgroundImageController@postDeleteUploadBgHeaderSejarah')->name('postDeleteUploadBgHeaderSejarah'); 

	// Upload Background Content Sejarah
	Route::get('/upload-bg-content-sejarah', 'BackgroundImageController@getUploadBgContentSejarah')->name('getUploadBgContentSejarah'); 
	Route::post('/add-upload-bg-content-sejarah', 'BackgroundImageController@postAddUploadBgContentSejarah')->name('postAddUploadBgContentSejarah'); 
	Route::post('/delete-upload-bg-content-sejarah/{id}', 'BackgroundImageController@postDeleteUploadBgContentSejarah')->name('postDeleteUploadBgContentSejarah');

	// Upload Background Gallery Foto Cabang
	Route::get('/upload-bg-gallery-foto-cabang', 'BackgroundImageController@getUploadBgGalleryFotoCabang')->name('getUploadBgGalleryFotoCabang'); 
	Route::post('/add-upload-bg-gallery-foto-cabang', 'BackgroundImageController@postAddUploadBgGalleryFotoCabang')->name('postAddUploadBgGalleryFotoCabang'); 
	Route::post('/delete-upload-bg-gallery-foto-cabang/{id}', 'BackgroundImageController@postDeleteUploadBgGalleryFotoCabang')->name('postDeleteUploadBgGalleryFotoCabang'); 

	// Upload Background Detail Foto Cabang
	Route::get('/upload-bg-detail-foto-cabang', 'BackgroundImageController@getUploadBgDetailFotoCabang')->name('getUploadBgDetailFotoCabang'); 
	Route::post('/add-upload-bg-detail-foto-cabang', 'BackgroundImageController@postAddUploadBgDetailFotoCabang')->name('postAddUploadBgDetailFotoCabang'); 
	Route::post('/delete-upload-bg-detail-foto-cabang/{id}', 'BackgroundImageController@postDeleteUploadBgDetailFotoCabang')->name('postDeleteUploadBgDetailFotoCabang');

	// Upload Background Sps Youtube
	Route::get('/upload-bg-sps-youtube', 'BackgroundImageController@getUploadBgSpsYoutube')->name('getUploadBgSpsYoutube');
	Route::post('/add-upload-bg-sps-youtube', 'BackgroundImageController@postAddUploadBgSpsYoutube')->name('postAddUploadBgSpsYoutube'); 
	Route::post('/delete-upload-bg-sps-youtube/{id}', 'BackgroundImageController@postDeleteUploadBgSpsYoutube')->name('postDeleteUploadBgSpsYoutube'); 

	// Upload Background Jaringan
	Route::get('/upload-bg-jaringan', 'BackgroundImageController@getUploadBgJaringan')->name('getUploadBgJaringan');
	Route::post('/add-upload-bg-jaringan', 'BackgroundImageController@postAddUploadBgJaringan')->name('postAddUploadBgJaringan'); 
	Route::post('/delete-upload-bg-jaringan/{id}', 'BackgroundImageController@postDeleteUploadBgJaringan')->name('postDeleteUploadBgJaringan'); 

	// Upload Background Detail Jaringan
	Route::get('/upload-bg-detail-jaringan', 'BackgroundImageController@getUploadBgDetailJaringan')->name('getUploadBgDetailJaringan');
	Route::post('/add-upload-bg-detail-jaringan', 'BackgroundImageController@postAddUploadBgDetailJaringan')->name('postAddUploadBgDetailJaringan'); 
	Route::post('/delete-upload-bg-detail-jaringan/{id}', 'BackgroundImageController@postDeleteUploadBgDetailJaringan')->name('postDeleteUploadBgDetailJaringan');

	// Upload Background Produk
	Route::get('/upload-bg-produk', 'BackgroundImageController@getUploadBgProduk')->name('getUploadBgProduk');
	Route::post('/add-upload-bg-produk', 'BackgroundImageController@postAddUploadBgProduk')->name('postAddUploadBgProduk'); 
	Route::post('/delete-upload-bg-produk/{id}', 'BackgroundImageController@postDeleteUploadBgProduk')->name('postDeleteUploadBgProduk');

	// Upload Background Header Produk
	Route::get('/upload-bg-header-produk', 'BackgroundImageController@getUploadBgHeaderProduk')->name('getUploadBgHeaderProduk');
	Route::post('/add-upload-bg-header-produk', 'BackgroundImageController@postAddUploadBgHeaderProduk')->name('postAddUploadBgHeaderProduk'); 
	Route::post('/delete-upload-bg-header-produk/{id}', 'BackgroundImageController@postDeleteUploadBgHeaderProduk')->name('postDeleteUploadBgHeaderProduk'); 

	// Upload Background Detail Produk
	Route::get('/upload-bg-detail-produk', 'BackgroundImageController@getUploadBgDetailProduk')->name('getUploadBgDetailProduk');
	Route::post('/add-upload-bg-detail-produk', 'BackgroundImageController@postAddUploadBgDetailProduk')->name('postAddUploadBgDetailProduk'); 
	Route::post('/delete-upload-bg-detail-produk/{id}', 'BackgroundImageController@postDeleteUploadBgDetailProduk')->name('postDeleteUploadBgDetailProduk');

	// Upload Background Header Berita
	Route::get('/upload-bg-header-berita', 'BackgroundImageController@getUploadBgHeaderBerita')->name('getUploadBgHeaderBerita');
	Route::post('/add-upload-bg-header-berita', 'BackgroundImageController@postAddUploadBgHeaderBerita')->name('postAddUploadBgHeaderBerita'); 
	Route::post('/delete-upload-bg-header-berita/{id}', 'BackgroundImageController@postDeleteUploadBgHeaderBerita')->name('postDeleteUploadBgHeaderBerita'); 

	// Upload Background Berita
	Route::get('/upload-bg-berita', 'BackgroundImageController@getUploadBgBerita')->name('getUploadBgBerita');
	Route::post('/add-upload-bg-berita', 'BackgroundImageController@postAddUploadBgBerita')->name('postAddUploadBgBerita'); 
	Route::post('/delete-upload-bg-berita/{id}', 'BackgroundImageController@postDeleteUploadBgBerita')->name('postDeleteUploadBgBerita');

	// Upload Background Detail Berita
	Route::get('/upload-bg-detail-berita', 'BackgroundImageController@getUploadBgDetailBerita')->name('getUploadBgDetailBerita');
	Route::post('/add-upload-bg-detail-berita', 'BackgroundImageController@postAddUploadBgDetailBerita')->name('postAddUploadBgDetailBerita'); 
	Route::post('/delete-upload-bg-detail-berita/{id}', 'BackgroundImageController@postDeleteUploadBgDetailBerita')->name('postDeleteUploadBgDetailBerita'); 

	// Upload Background Download
	Route::get('/upload-bg-download', 'BackgroundImageController@getUploadBgDownload')->name('getUploadBgDownload');
	Route::post('/add-upload-bg-download', 'BackgroundImageController@postAddUploadBgDownload')->name('postAddUploadBgDownload'); 
	Route::post('/delete-upload-bg-download/{id}', 'BackgroundImageController@postDeleteUploadBgDownload')->name('postDeleteUploadBgDownload');

	Route::get('/', 'LoginController@getLogin')->name('getLogin'); 
	Route::get('/login', 'LoginController@getLogin')->name('getLogin'); 
	Route::post('/login', 'LoginController@postLogin')->name('postLogin'); 
	Route::get('/logout', 'LoginController@getLogout')->name('getLogout'); 
	Route::get('/dashboard', 'DashboardController@getDashboard')->name('getDashboard');

	//Category Cabang
	Route::get('/semua-category-cabang', 'CategoryCabangController@getSemuaCategoryCabang')->name('getSemuaCategoryCabang'); 
	
	Route::get('/add-category-cabang', 'CategoryCabangController@getAddCategoryCabang')->name('getAddCategoryCabang'); 
	Route::post('/add-category-cabang', 'CategoryCabangController@postAddCategoryCabang')->name('postAddCategoryCabang');
	 
	Route::get('/edit-category-cabang/{slug}', 'CategoryCabangController@getEditCategoryCabang')->name('getEditCategoryCabang');
	Route::post('/edit-category-cabang/{slug}', 'CategoryCabangController@postEditCategoryCabang')->name('postEditCategoryCabang');

	Route::post('/delete-category-cabang/{slug}', 'CategoryCabangController@postDeleteCategoryCabang')->name('postDeleteCategoryCabang');

	//Cabang
	Route::get('/cabang', 'CabangController@getSemuaCabang')->name('getSemuaCabang'); 
	
	Route::get('/add-cabang', 'CabangController@getAddCabang')->name('getAddCabang'); 
	Route::post('/add-cabang', 'CabangController@postAddCabang')->name('postAddCabang');
	 
	Route::get('/edit-cabang/{slug}', 'CabangController@getEditCabang')->name('getEditCabang');
	Route::post('/edit-cabang/{slug}', 'CabangController@postEditCabang')->name('postEditCabang');

	Route::post('/delete-cabang/{slug}', 'CabangController@postDeleteCabang')->name('postDeleteCabang');

	//Gallery Cabang
	Route::get('/gallery-cabang', 'GalleryCabangController@getGalleryCabang')->name('getGalleryCabang'); 
	
	Route::get('/add-gallery-cabang', 'GalleryCabangController@getAddGalleryCabang')->name('getAddGalleryCabang'); 
	Route::post('/add-gallery-cabang', 'GalleryCabangController@postAddGalleryCabang')->name('postAddGalleryCabang');
	 
	Route::get('/edit-gallery-cabang/{id}', 'GalleryCabangController@getEditGalleryCabang')->name('getEditGalleryCabang');
	Route::post('/edit-gallery-cabang/{id}', 'GalleryCabangController@postEditGalleryCabang')->name('postEditGalleryCabang');

	Route::post('/delete-gallery-cabang/{id}', 'GalleryCabangController@postDeleteGalleryCabang')->name('postDeleteGalleryCabang');

	//Category Product
	Route::get('/semua-category-product', 'CategoryProductController@getSemuaCategoryProduct')->name('getSemuaCategoryProduct'); 
	
	Route::get('/add-category-product', 'CategoryProductController@getAddCategoryProduct')->name('getAddCategoryProduct'); 
	Route::post('/add-category-product', 'CategoryProductController@postAddCategoryProduct')->name('postAddCategoryProduct');
	 
	Route::get('/edit-category-product/{slug}', 'CategoryProductController@getEditCategoryProduct')->name('getEditCategoryProduct');
	Route::post('/edit-category-product/{slug}', 'CategoryProductController@postEditCategoryProduct')->name('postEditCategoryProduct');

	Route::post('/delete-category-product/{slug}', 'CategoryProductController@postDeleteCategoryProduct')->name('postDeleteCategoryProduct');

	//Product
	Route::get('/product', 'ProductController@getSemuaProduct')->name('getSemuaProduct'); 
	
	Route::get('/add-product', 'ProductController@getAddProduct')->name('getAddProduct'); 
	Route::post('/add-product', 'ProductController@postAddProduct')->name('postAddProduct');
	 
	Route::get('/edit-product/{slug}', 'ProductController@getEditProduct')->name('getEditProduct');
	Route::post('/edit-product/{slug}', 'ProductController@postEditProduct')->name('postEditProduct');

	Route::post('/delete-product/{slug}', 'ProductController@postDeleteProduct')->name('postDeleteProduct');

	//Gallery Product
	Route::get('/product/{product_id}/gallery', 'ProductImageController@getSemuaGalleryProduct')->name('getSemuaGalleryProduct'); 
	
	Route::get('/add-gallery-product/{product_id}', 'ProductImageController@getAddGalleryProduct')->name('getAddGalleryProduct'); 
	Route::post('/add-gallery-product', 'ProductImageController@postAddGalleryProduct')->name('postAddGalleryProduct');
	 
	Route::get('/product/{product_id}/gallery/{id}', 'ProductImageController@getEditGalleryProduct')->name('getEditGalleryProduct');
	Route::post('/product/{product_id}/gallery/{id}', 'ProductImageController@postEditGalleryProduct')->name('postEditGalleryProduct');

	Route::post('/product/{product_id}/delete-gallery/{id}', 'ProductImageController@postDeleteGalleryProduct')->name('postDeleteGalleryProduct');

	Route::post('/product/{product_id}/gallery/{id}/set-image', 'ProductImageController@postSetImageProduct')->name('postSetImageProduct');

	Route::post('/product/{product_id}/gallery/{id}/remove-image', 'ProductImageController@postRemoveImageProduct')->name('postRemoveImageProduct');

	//Profile Sejarah
	Route::get('/sejarah', 'ProfileController@getSemuaSejarah')->name('getSemuaSejarah'); 
	
	Route::get('/add-sejarah', 'ProfileController@getAddSejarah')->name('getAddSejarah'); 
	Route::post('/add-sejarah', 'ProfileController@postAddSejarah')->name('postAddSejarah');
	 
	Route::get('/edit-sejarah/{id}', 'ProfileController@getEditSejarah')->name('getEditSejarah');
	Route::post('/edit-sejarah/{id}', 'ProfileController@postEditSejarah')->name('postEditSejarah');

	Route::post('/delete-sejarah/{id}', 'ProfileController@postDeleteSejarah')->name('postDeleteSejarah');

	//Profile SPS Youtube
	Route::get('/sps-youtube', 'ProfileController@getSemuaSpsYoutube')->name('getSemuaSpsYoutube'); 
	
	Route::get('/add-sps-youtube', 'ProfileController@getAddSpsYoutube')->name('getAddSpsYoutube'); 
	Route::post('/add-sps-youtube', 'ProfileController@postAddSpsYoutube')->name('postAddSpsYoutube');
	 
	Route::get('/edit-sps-youtube/{id}', 'ProfileController@getEditSpsYoutube')->name('getEditSpsYoutube');
	Route::post('/edit-sps-youtube/{id}', 'ProfileController@postEditSpsYoutube')->name('postEditSpsYoutube');

	Route::post('/delete-sps-youtube/{id}', 'ProfileController@postDeleteSpsYoutube')->name('postDeleteSpsYoutube');

	//Karir
	Route::get('/karir', 'KarirController@getSemuaKarir')->name('getSemuaKarir'); 
	
	Route::get('/add-karir', 'KarirController@getAddKarir')->name('getAddKarir'); 
	Route::post('/add-karir', 'KarirController@postAddKarir')->name('postAddKarir');
	 
	Route::get('/edit-karir/{id}', 'KarirController@getEditKarir')->name('getEditKarir');
	Route::post('/edit-karir/{id}', 'KarirController@postEditKarir')->name('postEditKarir');

	Route::post('/delete-karir/{id}', 'KarirController@postDeleteKarir')->name('postDeleteKarir');

	//News
	Route::get('/news', 'NewsController@getSemuaNews')->name('getSemuaNews'); 
	
	Route::get('/add-news', 'NewsController@getAddNews')->name('getAddNews'); 
	Route::post('/add-news', 'NewsController@postAddNews')->name('postAddNews');
	 
	Route::get('/edit-news/{id}', 'NewsController@getEditNews')->name('getEditNews');
	Route::post('/edit-news/{id}', 'NewsController@postEditNews')->name('postEditNews');

	Route::post('/delete-news/{id}', 'NewsController@postDeleteNews')->name('postDeleteNews');

	//Download File
	Route::get('/download-file', 'DownloadFileController@getSemuaDownloadFile')->name('getSemuaDownloadFile'); 

	Route::get('/files/{id}', 'DownloadFileController@getFile')->name('getFile'); 
	
	Route::get('/add-download-file', 'DownloadFileController@getAddDownloadFile')->name('getAddDownloadFile'); 
	Route::post('/add-download-file', 'DownloadFileController@postAddDownloadFile')->name('postAddDownloadFile');
	 
	Route::get('/edit-download-file/{id}', 'DownloadFileController@getEditDownloadFile')->name('getEditDownloadFile');
	Route::post('/edit-download-file/{id}', 'DownloadFileController@postEditDownloadFile')->name('postEditDownloadFile');

	Route::post('/delete-download-file/{id}', 'DownloadFileController@postDeleteDownloadFile')->name('postDeleteDownloadFile');

	//Get Customer Comment
	Route::get('/comment', 'CommentController@getSemuaComment')->name('getSemuaComment');

	Route::get('/reply-comment/{id}', 'CommentController@getReplyComment')->name('getReplyComment'); 
	Route::post('/reply-comment/{id}', 'CommentController@postReplyComment')->name('postReplyComment'); 
	 
	Route::post('/change-status-comment/{id}/status/{status}', 'CommentController@postChangeStatusComment')->name('postChangeStatusComment');

	Route::post('/delete-comment/{id}', 'CommentController@postDeleteComment')->name('postDeleteComment');

	//Get Kritik Dan Saran
	Route::get('/kritik-dan-saran', 'KritikDanSaranController@getSemuaKritikDanSaran')->name('getSemuaKritikDanSaran');
	Route::post('/delete-kritik-dan-saran/{id}', 'KritikDanSaranController@postDeleteKritikDanSaran')->name('postDeleteKritikDanSaran');

	//Service Booking
	Route::get('/service-booking', 'ServiceBookingController@getSemuaServiceBooking')->name('getSemuaServiceBooking'); 
	
	Route::get('/add-service-booking', 'ServiceBookingController@getAddServiceBooking')->name('getAddServiceBooking'); 
	Route::post('/add-service-booking', 'ServiceBookingController@postAddServiceBooking')->name('postAddServiceBooking');
	 
	Route::get('/edit-service-booking/{id}', 'ServiceBookingController@getEditServiceBooking')->name('getEditServiceBooking');
	Route::post('/edit-service-booking/{id}', 'ServiceBookingController@postEditServiceBooking')->name('postEditServiceBooking');

	Route::post('/delete-service-booking/{id}', 'ServiceBookingController@postDeleteServiceBooking')->name('postDeleteServiceBooking');

	//Setting
	Route::get('/add-setting', 'SettingController@getAddSetting')->name('getAddSetting');
	Route::post('/add-setting', 'SettingController@postAddSetting')->name('postAddSetting');

	Route::get('/edit-setting/{id}', 'SettingController@getEditSetting')->name('getEditSetting');
	Route::post('/edit-setting/{id}', 'SettingController@postEditSetting')->name('postEditSetting');

	//Subscriber
	Route::get('/subscriber', 'SubscribeController@getSubscriber')->name('getSubscriber'); 
	Route::post('/delete-subscriber/{id}', 'SubscribeController@postDeleteSubscriber')->name('postDeleteSubscriber');

	//Pesan Sparepart
	Route::get('/pesan-sparepart', 'PesanSparepartController@getPesanSparepartAdmin')->name('getPesanSparepartAdmin'); 
	Route::get('/detail-pesan-sparepart/{id}', 'PesanSparepartController@getDetailPesanSparepartAdmin')->name('getDetailPesanSparepartAdmin'); 
	Route::post('/delete-pesan-sparepart/{id}', 'PesanSparepartController@postDeletePesanSparepart')->name('postDeletePesanSparepart'); 

	// User
	Route::get('/user', 'UserController@getUser')->name('getUser'); 
	
	Route::get('/add-user', 'UserController@getAddUser')->name('getAddUser'); 
	Route::post('/add-user', 'UserController@postAddUser')->name('postAddUser');
	 
	Route::get('/edit-user/{id}', 'UserController@getEditUser')->name('getEditUser');
	Route::post('/edit-user/{id}', 'UserController@postEditUser')->name('postEditUser');

	Route::post('/delete-user/{id}', 'UserController@postDeleteUser')->name('postDeleteUser');

	Route::get('/change-password', 'UserController@getChangePassword')->name('getChangePassword');
	Route::post('/change-password', 'UserController@postChangePassword')->name('postChangePassword');

	Route::get('/reset-password', 'UserController@getResetPassword')->name('getResetPassword');
	Route::post('/reset-password', 'UserController@postResetPassword')->name('postResetPassword');
});










