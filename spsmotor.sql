-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 14, 2017 at 06:38 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `spsmotor`
--

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

CREATE TABLE `cabang` (
  `id` int(10) unsigned NOT NULL,
  `category_cabang_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_bengkel_cabang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_pic_cabang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_technical_service_department_cabang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_customer_service_cabang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ig_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `informasi_hari_dan_jam` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cabang`
--

INSERT INTO `cabang` (`id`, `category_cabang_id`, `user_id`, `slug`, `email_bengkel_cabang`, `email_pic_cabang`, `email_technical_service_department_cabang`, `email_customer_service_cabang`, `fb_url`, `twitter_url`, `ig_url`, `name`, `description`, `img_path`, `address`, `no_telepon`, `informasi_hari_dan_jam`, `date_time`, `lat`, `lng`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, 1, 'sps-gresiks', 'email_cabang1@mail.coms', 'semail_pic1@mail.coms', 'email_technical_service_department1@mail.coms', 'email_customer_service1@mail.coms', 'http://facebook.coms', 'http://twitter.coms', 'http://instagram.com', 'SPS Gresiks', '<p>SPS Gresiks</p>', '/public/no-image.jpg', 'Gresiks', '031 111 222 3332', '<p>Senin - Jumat / 09.00 - 18.00</p>\r\n\r\n<p>Sabtu&nbsp;/ 09.00 - 18.00</p>\r\n\r\n<p>Minggu / 09.00 - 12.00 (Service Motor Saja)</p>', '2017-07-12 15:00:14', -7.8624849, 112.5066177, '2017-07-12 08:00:14', '2017-07-12 09:21:06', '0'),
(2, 2, 1, 'sps-sleman', 'email_cabang2@mail.com', 'email_pic2@mail.com', 'email_technical_service_department2@mail.com', 'email_customer_service2@mail.com', 'http://facebook.com', 'http://twitter.com', 'http://instagram.com', 'SPS Sleman', '<p>SPS Sleman</p>', '/public/no-image.jpg', 'Sleman', '031 111 222 333', '<p>Senin - Jumat / 09.00 - 18.00</p>\r\n\r\n<p>Sabtu / 09.00 - 18.00</p>\r\n\r\n<p>Minggu / 09.00 - 12.00 (Service Motor Saja)</p>', '2017-07-12 15:00:14', -7.695127, 110.3094606, '2017-07-12 08:00:14', '2017-07-12 09:28:14', '0'),
(3, 3, 1, 'sps-palembang', 'email_cabang3@mail.com', 'email_pic3@mail.com', 'email_technical_service_department3@mail.com', 'email_customer_service3@mail.com', 'http://facebook.com', 'http://twitter.com', 'http://instagram.com', 'SPS Palembang', '<p>SPS Palembang</p>', '/public/no-image.jpg', 'Palembang', '031 111 222 333', '<p>Senin - Jumat / 09.00 - 18.00</p>\r\n\r\n<p>Sabtu&nbsp;/ 09.00 - 18.00</p>\r\n\r\n<p>Minggu / 09.00 - 12.00 (Service Motor Saja)</p>', '2017-07-12 15:00:14', -2.9549663, 104.6929231, '2017-07-12 08:00:14', '2017-07-12 09:26:58', '0'),
(4, 1, 1, 'sps-jember', 'sps_bengkel_jember@mail.com', 'sps_pic_jember@mail.com', 'sps_tic_jember@mail.com', 'sps_cs_jember@mail.com', 'http://facebook.com/spsjember', 'http://twitter.com/spsjember', 'http://instagram.com/spsjember', 'SPS Jember', '<p>SPS Jember</p>', '/public/branch-image/0df87e6c71d20ed7ae61530f2a88a3f0f0bf29b2.jpeg', 'Kota Jember No 12900', '773 231 888', '<p>Senin - Jumat / 09.00 - 18.00</p>\r\n\r\n<p>Sabtu, Minggu / Tutup</p>', '2017-07-12 15:12:14', -8.1768084, 113.6559506, '2017-07-12 08:12:14', '2017-07-12 08:12:14', '0');

-- --------------------------------------------------------

--
-- Table structure for table `category_cabang`
--

CREATE TABLE `category_cabang` (
  `id` int(10) unsigned NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_cabang`
--

INSERT INTO `category_cabang` (`id`, `slug`, `name`, `created_at`, `updated_at`, `delete`) VALUES
(1, 'sps-jatim', 'SPS Jatim', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(2, 'sps-jateng', 'SPS Jateng', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(3, 'sps-sumsel', 'SPS Sumsel', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0');

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `id` int(10) unsigned NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `slug`, `name`, `created_at`, `updated_at`, `delete`) VALUES
(1, 'honda-cub', 'Honda CUB', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(2, 'honda-big-bike', 'Honda Big Bike', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(3, 'honda-matic', 'Honda Matic', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(4, 'honda-sport', 'Honda Sport', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) unsigned NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_or_news_id` int(11) NOT NULL,
  `comment_fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approval` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `tipe`, `product_or_news_id`, `comment_fullname`, `comment_email`, `comment_description`, `approval`, `date_time`, `created_at`, `updated_at`, `delete`) VALUES
(1, 'product', 1, 'Dummy 1', 'dummy1@mail.com', 'mau tanya ini kok bisa gitu kok bisa gitu gitu yaaa gitu kok bisa gitu gitu yaaa gitu kok bisa gitu gitu yaaa gitu kok bisa gitu gitu yaaa ?', '0', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(2, 'product', 1, 'Dummy 1', 'dummy1@mail.com', 'Loh ?', '0', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(3, 'product', 1, 'Dummy 1', 'dummy1@mail.com', 'Gini lo ?', '0', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(4, 'product', 1, 'Dummy 2', 'dummy2@mail.com', 'Product nya kok gini ?', '0', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(5, 'product', 1, 'Dummy 2', 'dummy2@mail.com', 'Harus nya itu product nya seperti ini', '1', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(6, 'product', 1, 'Dummy 3', 'dummy3@mail.com', 'Kampret lu', '2', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(7, 'news', 1, 'Dummy 4', 'dummy4@mail.com', 'Berita nya bener ga ini ?', '0', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(8, 'news', 1, 'Dummy 5', 'dummy5@mail.com', 'ya bener lah, gimana sih lo', '0', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(9, 'news', 1, 'Dummy 5', 'dummy5@mail.com', 'Berita nya udah bener ini', '1', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(10, 'news', 1, 'Dummy 4', 'dummy4@mail.com', 'Kampret berita nya', '2', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0');

-- --------------------------------------------------------

--
-- Table structure for table `download_brosur_dan_suku_cadang`
--

CREATE TABLE `download_brosur_dan_suku_cadang` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_cabang`
--

CREATE TABLE `gallery_cabang` (
  `id` int(10) unsigned NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_cabang`
--

INSERT INTO `gallery_cabang` (`id`, `cabang_id`, `user_id`, `slug`, `title`, `description`, `img_path`, `date_time`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, 1, 'acara-bulan-juli', 'Acara Bulan Juli', '<p>Acara Bulan Juli</p>', '/public/gallery-cabang/be02f6b0fb5e189a8d0027f80bf6e54db3f004a1.png', '2017-07-14 11:33:27', '2017-07-14 04:33:27', '2017-07-14 04:33:27', '0'),
(2, 1, 1, 'acara-bulan-juni', 'Acara Bulan Juni', '<p>Acara Bulan Juni</p>', '/public/gallery-cabang/693f48617f3e6433d550edf3286908ed1851c48f.jpeg', '2017-07-14 11:33:53', '2017-07-14 04:33:53', '2017-07-14 04:33:53', '0');

-- --------------------------------------------------------

--
-- Table structure for table `karir`
--

CREATE TABLE `karir` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kritik_dan_saran`
--

CREATE TABLE `kritik_dan_saran` (
  `id` int(10) unsigned NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kritik_dan_saran`
--

INSERT INTO `kritik_dan_saran` (`id`, `cabang_id`, `fullname`, `email`, `no_telepon`, `pesan`, `date_time`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, 'Dummy 1', 'dummy1@mail.com', '1234567', 'harus nya cabang 1 begini', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(2, 1, 'Dummy 2', 'dummy2@mail.com', '1234568', 'harus nya cabang 1 begini begitu', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(3, 2, 'Dummy 3', 'dummy3@mail.com', '1234569', 'harus nya cabang 2 begini', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(4, 3, 'Dummy 4', 'dummy4@mail.com', '12345610', 'harus nya cabang 3 begini', '2017-07-12 23:13:40', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0'),
(5, 1, 'asd', 'asd@mail.com', '102310239', 'asdaosdkasd', '2017-07-14 12:16:45', '2017-07-14 05:16:45', '2017-07-14 05:16:45', '0');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2017_06_06_073137_Users', 1),
(3, '2017_06_07_070514_CategoryProduct', 1),
(4, '2017_06_08_064321_Product', 1),
(5, '2017_06_08_064829_ProductImage', 1),
(6, '2017_06_08_065308_Comment', 1),
(7, '2017_06_08_075934_SubscribeMail', 1),
(8, '2017_06_08_081047_CategoryCabang', 1),
(9, '2017_06_08_081237_Cabang', 1),
(10, '2017_06_08_084223_GalleryCabang', 1),
(11, '2017_06_08_092408_SpsYoutube', 1),
(12, '2017_06_08_092438_SpsProfile', 1),
(13, '2017_06_08_092641_DownloadBrosurDanSukuCadang', 1),
(14, '2017_06_08_093546_Karir', 1),
(15, '2017_06_08_093720_ServiceBooking', 1),
(16, '2017_06_08_093958_KritikDanSaran', 1),
(17, '2017_06_26_140418_News', 1),
(18, '2017_06_28_114406_Setting', 1),
(19, '2017_07_12_220054_PesanSparepart', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `user_id`, `slug`, `title`, `description`, `img_path`, `date_time`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, 'asd', 'asd', '<p>asd</p>', '/public/news-image/420731101eacfbd6904a059d01dffc98fe78de88.jpeg', '2017-07-11 13:59:23', '2017-07-11 06:59:23', '2017-07-11 06:59:23', '0'),
(2, 1, 'asdasd', 'asdasd', '<p>asdasd</p>', '/public/news-image/8948065117f343ee6cca331801dd70e23fcd8c28.jpeg', '2017-07-11 13:59:36', '2017-07-11 06:59:36', '2017-07-11 06:59:36', '0'),
(3, 1, 'aass', 'aass', '<p>aass</p>', '/public/news-image/e3e526f33f3e286705120e604d618b05c0211308.jpeg', '2017-07-11 13:59:49', '2017-07-11 06:59:49', '2017-07-11 06:59:49', '0'),
(4, 1, 'buba', 'buba', '<p>buba</p>', '/public/news-image/575350b6ca2601834d9bd4f524c810f5e644a3a6.jpeg', '2017-07-11 14:06:21', '2017-07-11 07:06:21', '2017-07-11 07:06:21', '0'),
(5, 1, 'bibi', 'bibi', '<p>bibi</p>', '/public/news-image/08ec51786be50e8b2979841c876604e5398279f4.jpeg', '2017-07-11 14:06:32', '2017-07-11 07:06:32', '2017-07-11 07:06:32', '0'),
(6, 1, 'boom', 'boom', '<p>boom</p>', '/public/news-image/c144f4f3b5d3e556f08e4f670d1d817addd7d18f.jpeg', '2017-07-11 14:06:45', '2017-07-11 07:06:45', '2017-07-11 07:06:45', '0'),
(7, 1, 'kacoo', 'kacoo', '<p>kacoo</p>', '/public/news-image/87acbcc626ed3204dea29f24b25a67790564507c.jpeg', '2017-07-11 14:07:49', '2017-07-11 07:07:49', '2017-07-11 07:07:49', '0'),
(8, 1, 'kachoooo', 'kachoooo', '<p>kachoooo</p>', '/public/news-image/624499391d62402ce85094053a6e2995c1e5a70e.jpeg', '2017-07-11 14:08:05', '2017-07-11 07:08:05', '2017-07-11 07:08:05', '0'),
(9, 1, 'mamamaama', 'mamamaama', '<p>asdasd</p>', '/public/news-image/67239fc3bc5d29d1815e60a9d4195e33c82c78a2.jpeg', '2017-07-11 14:08:17', '2017-07-11 07:08:17', '2017-07-11 07:08:17', '0'),
(10, 1, 'wkakaka', 'wkakaka', '<p>Senin - Jumat / 09.00 - 17.00</p>\r\n\r\n<p>Sabtu / 09.00 - 12.30</p>\r\n\r\n<p>Minggu / Service Only</p>', '/public/news-image/95ba8022ba0ae1a69a712cf75a71031a40e5f2c3.jpeg', '2017-07-11 14:09:08', '2017-07-11 07:09:08', '2017-07-11 07:09:08', '0'),
(11, 1, 'papa', 'papa', '<p>papa</p>', '/public/news-image/25193dc46acaa4134d3353081d5763920837f2cc.jpeg', '2017-07-11 16:02:26', '2017-07-11 09:02:26', '2017-07-11 09:02:26', '0'),
(12, 1, 'pipi', 'pipi', '<p>pipi</p>', '/public/news-image/39425c4c3e276ca3fa833531914ebea01b8cc97e.jpeg', '2017-07-11 16:02:37', '2017-07-11 09:02:37', '2017-07-11 09:02:37', '0'),
(13, 1, 'pupu', 'pupu', '<p>pupu</p>', '/public/news-image/0315185de9d7dbeb348d77a010f90cab4731cc5e.jpeg', '2017-07-11 16:02:49', '2017-07-11 09:02:49', '2017-07-11 09:02:49', '0'),
(14, 1, 'pepe', 'pepe', '<p>pepepopo</p>', '/public/news-image/d722fb348d9c2d8d3d67e55e5a59097c4d7a23d4.jpeg', '2017-07-11 16:03:00', '2017-07-11 09:03:00', '2017-07-11 09:03:11', '0'),
(15, 1, 'bapa', 'bapa', '<p>bapa</p>', '/public/news-image/e3574ae05d04fad31b89051bf15325176940ba46.jpeg', '2017-07-11 16:03:36', '2017-07-11 09:03:36', '2017-07-11 09:03:36', '0');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pesan_sparepart`
--

CREATE TABLE `pesan_sparepart` (
  `id` int(10) unsigned NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `jenis_kendaraan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_rakitan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pesan_sparepart`
--

INSERT INTO `pesan_sparepart` (`id`, `cabang_id`, `jenis_kendaraan`, `product_id`, `fullname`, `email`, `no_telepon`, `tahun_rakitan`, `description`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, 'cub', 1, 'Joko Widodo', 'jokowidodo@gmail.com', '0812323773381', '2012', 'a', '2017-07-12 16:16:08', '2017-07-12 16:16:08', '0'),
(2, 2, 'cub', 1, 'Anton', 'anton@mail.com', '012301', '2000', 'rusak', '2017-07-12 16:58:27', '2017-07-12 16:58:27', '0'),
(3, 4, 'cub', 1, 'Sumirjan', 'sumirjan', '101203', '2016', 'ini kok gini ya ?', '2017-07-12 16:59:35', '2017-07-12 17:01:13', '1'),
(4, 4, 'cub', 1, 'Sumirjan', 'sumirjan', '101203', '2016', 'ini kok gini ya ?', '2017-07-12 17:00:24', '2017-07-12 17:26:24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `user_id`, `slug`, `name`, `description`, `date_time`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, 1, 'supra-x-125r', 'Supra X 125R', '<p>Honda Supra X ini sendiri mendapat perubahan desain dimana motor ini terlihat agresif dengan desain yang tajam. Lampu belakang juga dibuat lebih runcing. Motor ini memiliki desain yang sangat stylish dan modern. Dimana balutan stripping khas Honda yang tajam membalut dengan halusnya di body motor bebek 125cc ini. Spesifikasi Honda Supra x 125 FI lainnya adalah pada bagian velgnya terpasang velg yang memiliki ukuran yang lebih ramping pada cakram-cakramnya yang membuat motor ini tampak lebih sporty. Fitur baru lainnya juga terlihat pada Stoplamp (Tail Light) yang sudah menggunakan teknologi LED. Hal ini membuat Spesifikasi Honda Supra Injeksi ini tampak terlihat lebih sporty dan modern</p>', '2017-07-12 10:38:58', '2017-07-12 03:38:58', '2017-07-12 03:38:58', '0');

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(11) NOT NULL,
  `set_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`id`, `product_id`, `set_image`, `path`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, '1', '/public/gallery-product/2e78590acde7ff702fa072aa5081ce1e033cf92c.png', '2017-07-12 09:29:51', '2017-07-12 09:30:08', '0');

-- --------------------------------------------------------

--
-- Table structure for table `service_booking`
--

CREATE TABLE `service_booking` (
  `id` int(10) unsigned NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_polisi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kendaraan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_booking` date NOT NULL,
  `jam_booking` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(10) unsigned NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `informasi_hari_dan_jam` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sps_profile`
--

CREATE TABLE `sps_profile` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sps_profile`
--

INSERT INTO `sps_profile` (`id`, `user_id`, `description`, `date_time`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, '<p>PT.SUMBER PURNAMA SAKTI adalah perusahaan dagang yang bergerak di bidang otomotif roda dua merk HONDA. Pertama kali berdiri pada tahun 1997 di kota Lamongan dengan Direktur Bpk. Hendra Senjaya, karena pesatnya pertumbuhan di bidang otomotif merck HONDA, PT.SUMBER PURNAMA SAKTI membuka cabang di Gresik pada tahun 1998.</p>\r\n\r\n<p>Untuk mengembangkan usaha otomotif merck HONDA maka perlu diadakannya pembangunan cabang di Indonesia sehingga pada tahun 1999 PT.SUMBER PURNAMA SAKTI membuka cabang kota Mataram &ndash; Lombok Barat serta Lombok Timur tahun 2002. Jawa Tengah tahun 2003 yaitu kota Ajibarang dan pada tahun 2004 di Sleman.<br />\r\nBegitu pesatnya permintaan kendaraan roda dua khususnya HONDA dan dengan terpenuhinya permintaan pasar maka PT.SUMBER PURNAMA SAKTI.Pada tahun 2004 diberi kepercayaan kembali untuk&nbsp; membuka cabang kembali di Jawa Timur yaitu kota Tuban serta di ikuti dengan pembukaan cabang di luar pulau Jawa.<br />\r\nHingga sekarang cabang PT.SUMBER PURNAMA SAKTI berjumlah 13 cabang dan sudah tersebar di wilayah strategis Indonesia.</p>', '2017-07-12 17:01:50', '2017-07-12 10:01:50', '2017-07-12 10:01:50', '0');

-- --------------------------------------------------------

--
-- Table structure for table `sps_youtube`
--

CREATE TABLE `sps_youtube` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sps_youtube`
--

INSERT INTO `sps_youtube` (`id`, `user_id`, `title`, `url`, `date_time`, `created_at`, `updated_at`, `delete`) VALUES
(1, 1, 'Yel Yel SPS Gresik', 'https://www.youtube.com/embed/aEnfgoEfG1w', '2017-07-12 16:39:18', '2017-07-12 09:39:18', '2017-07-12 09:39:18', '0'),
(2, 1, 'Yel Yel SPS Lamongan', 'https://www.youtube.com/embed/zrhe-kEFFcI', '2017-07-12 16:39:43', '2017-07-12 09:39:43', '2017-07-12 09:39:43', '0'),
(3, 1, 'Yel Yel SPS Tuban', 'https://www.youtube.com/embed/2jgbScePORY', '2017-07-12 16:40:16', '2017-07-12 09:40:16', '2017-07-12 09:40:16', '0'),
(4, 1, 'Yel Yel SPS Batu', 'https://www.youtube.com/embed/cw9RP1KAHP4', '2017-07-12 16:40:34', '2017-07-12 09:40:34', '2017-07-12 09:40:34', '0'),
(5, 1, 'SPS Motor Honda Mataram', 'https://www.youtube.com/embed/VtLgyt78MZY', '2017-07-12 16:40:58', '2017-07-12 09:40:58', '2017-07-12 09:40:58', '0'),
(6, 1, 'SPS Motor Honda Mataram Part 1', 'https://www.youtube.com/embed/lFhZwssb6PY', '2017-07-12 16:41:27', '2017-07-12 09:41:27', '2017-07-12 09:41:27', '0'),
(7, 1, 'SPS Honda Jatim', 'https://www.youtube.com/embed/sc1GfIXGY3Q', '2017-07-12 16:41:52', '2017-07-12 09:41:52', '2017-07-12 09:41:52', '0'),
(8, 1, 'Meeting & Gathering SPS Jatim 2017 - Batu', 'https://www.youtube.com/embed/WEF_QxBQGso', '2017-07-12 16:42:39', '2017-07-12 09:42:39', '2017-07-12 09:42:39', '0');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_mail`
--

CREATE TABLE `subscribe_mail` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `email`, `password`, `phone`, `remember_token`, `created_at`, `updated_at`, `delete`) VALUES
(1, 'Viko Wijaya', 'viko', 'viko_wijaya@yahoo.co.id', '$2y$10$F2ks4swcrI02T6QMcFFdi.X6W9419tg0/shnAy4zUtO6.FurAP33C', '12345678910', '2zE7TF6vYKuL1I2w7RIOHgKikjFG5CzT3plNaVBXlo88xn5imSJK32xYtrJu', '2017-07-12 16:13:40', '2017-07-12 16:13:40', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cabang_slug_unique` (`slug`);

--
-- Indexes for table `category_cabang`
--
ALTER TABLE `category_cabang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_cabang_slug_unique` (`slug`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_product_slug_unique` (`slug`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download_brosur_dan_suku_cadang`
--
ALTER TABLE `download_brosur_dan_suku_cadang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_cabang`
--
ALTER TABLE `gallery_cabang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `gallery_cabang_slug_unique` (`slug`);

--
-- Indexes for table `karir`
--
ALTER TABLE `karir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kritik_dan_saran`
--
ALTER TABLE `kritik_dan_saran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pesan_sparepart`
--
ALTER TABLE `pesan_sparepart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_slug_unique` (`slug`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_booking`
--
ALTER TABLE `service_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps_profile`
--
ALTER TABLE `sps_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps_youtube`
--
ALTER TABLE `sps_youtube`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe_mail`
--
ALTER TABLE `subscribe_mail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `category_cabang`
--
ALTER TABLE `category_cabang`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `download_brosur_dan_suku_cadang`
--
ALTER TABLE `download_brosur_dan_suku_cadang`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery_cabang`
--
ALTER TABLE `gallery_cabang`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `karir`
--
ALTER TABLE `karir`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kritik_dan_saran`
--
ALTER TABLE `kritik_dan_saran`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `pesan_sparepart`
--
ALTER TABLE `pesan_sparepart`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `service_booking`
--
ALTER TABLE `service_booking`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sps_profile`
--
ALTER TABLE `sps_profile`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sps_youtube`
--
ALTER TABLE `sps_youtube`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subscribe_mail`
--
ALTER TABLE `subscribe_mail`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;






-- ALTER TABLE `product`
-- ADD COLUMN `price` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `description`,
-- ADD COLUMN `volume_langkah` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_mesin`,
-- ADD COLUMN `sistem_pendingin` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `volume_langkah`,
-- ADD COLUMN `sistem_suplai_bahan_bakar` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `sistem_pendingin`,
-- ADD COLUMN `diameter_x_langkah` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `sistem_suplai_bahan_bakar`,
-- ADD COLUMN `tipe_transmisi` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `diameter_x_langkah`,
-- ADD COLUMN `rasio_kompresi` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_transmisi`,
-- ADD COLUMN `daya_maksimum` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `rasio_kompresi`,
-- ADD COLUMN `torsi_maksimum` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `daya_maksimum`,
-- ADD COLUMN `pola_pengoperan_gigi` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `torsi_maksimum`,
-- ADD COLUMN `tipe_starter` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `pola_pengoperan_gigi`,
-- ADD COLUMN `tipe_kopling` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_starter`,
-- ADD COLUMN `kapasitas_minyak_pelumas` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_kopling`,
-- ADD COLUMN `panjang_lebar_tinggi` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `kapasitas_minyak_pelumas`,
-- ADD COLUMN `jarak_sumbu_roda` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `panjang_lebar_tinggi`,
-- ADD COLUMN `jarak_terendah_ke_tanah` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `jarak_sumbu_roda`,
-- ADD COLUMN `curb_weight` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `jarak_terendah_ke_tanah`,
-- ADD COLUMN `kapasitas_tangki_bbm` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `curb_weight`,
-- ADD COLUMN `tipe_rangka` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `kapasitas_tangki_bbm`,
-- ADD COLUMN `tipe_suspensi_depan` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_rangka`,
-- ADD COLUMN `tipe_suspensi_belakang` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_suspensi_depan`,
-- ADD COLUMN `ukuran_ban_depan` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_suspensi_belakang`,
-- ADD COLUMN `ukuran_ban_belakang` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `ukuran_ban_depan`,
-- ADD COLUMN `tipe_rem_depan` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `ukuran_ban_belakang`,
-- ADD COLUMN `tipe_rem_belakang` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_rem_depan`;
-- ADD COLUMN `tipe_aki` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_rem_belakang`;
-- ADD COLUMN `sistem_pengapian` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `tipe_aki`;
-- ADD COLUMN `tipe_busi` varchar(191) COLLATE utf8mb4_unicode_ci NULL AFTER `sistem_pengapian`;