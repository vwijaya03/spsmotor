<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KritikDanSaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kritik_dan_saran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cabang_id');
            $table->string('fullname');
            $table->string('email');
            $table->string('no_telepon');
            $table->text('pesan');
            $table->dateTime('date_time');
            $table->timestamps();
            $table->string('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kritik_dan_saran');
    }
}
