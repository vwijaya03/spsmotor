<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('user_id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->string('thumbnail');
            $table->text('description');
            $table->string('price')->nullable(); // Ditambahi Kolom Price
            $table->string('tipe_mesin')->nullable();
            $table->string('volume_langkah')->nullable();
            $table->string('sistem_pendingin')->nullable();
            $table->string('sistem_suplai_bahan_bakar')->nullable();
            $table->string('diameter_x_langkah')->nullable();
            $table->string('tipe_transmisi')->nullable();
            $table->string('rasio_kompresi')->nullable();
            $table->string('daya_maksimum')->nullable();
            $table->string('torsi_maksimum')->nullable();
            $table->string('pola_pengoperan_gigi')->nullable();
            $table->string('tipe_starter')->nullable();
            $table->string('tipe_kopling')->nullable();
            $table->string('kapasitas_minyak_pelumas')->nullable();
            $table->string('panjang_lebar_tinggi')->nullable();
            $table->string('jarak_sumbu_roda')->nullable();
            $table->string('jarak_terendah_ke_tanah')->nullable();
            $table->string('curb_weight')->nullable();
            $table->string('kapasitas_tangki_bbm')->nullable();
            $table->string('tipe_rangka')->nullable();
            $table->string('tipe_suspensi_depan')->nullable();
            $table->string('tipe_suspensi_belakang')->nullable();
            $table->string('ukuran_ban_depan')->nullable();
            $table->string('ukuran_ban_belakang')->nullable();
            $table->string('tipe_rem_depan')->nullable();
            $table->string('tipe_rem_belakang')->nullable();
            $table->string('tipe_aki')->nullable();
            $table->string('sistem_pengapian')->nullable();
            $table->string('tipe_busi')->nullable();
            $table->dateTime('date_time');
            $table->timestamps();
            $table->string('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
