<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cabang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_cabang_id');
            $table->integer('user_id');
            $table->string('slug')->unique();
            $table->string('email_bengkel_cabang');
            $table->string('email_pic_cabang');
            $table->string('email_technical_service_department_cabang');
            $table->string('email_customer_service_cabang');
            $table->string('fb_url');
            $table->string('twitter_url');
            $table->string('ig_url');
            $table->string('name');
            $table->text('description');
            $table->string('img_path');
            $table->string('address');
            $table->string('no_telepon');
            $table->text('informasi_hari_dan_jam');
            $table->dateTime('date_time');
            $table->double('lat');
            $table->double('lng');
            $table->timestamps();
            $table->string('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cabang');
    }
}
