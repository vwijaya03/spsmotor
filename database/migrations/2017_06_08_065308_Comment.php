<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipe'); // Product Or News
            $table->integer('product_or_news_id');
            $table->string('comment_fullname');
            $table->string('comment_email');
            $table->string('comment_description');
            $table->string('approval');
            $table->dateTime('date_time');
            $table->string('role');
            $table->timestamps();
            $table->string('delete');
        });
    }
    // 1 id tabel comment
    // product
    // 1 id product 
    // viko
    //email
    // description

    // 2 id tabel comment
    // product
    // 1 id product
    //viko
    // email
    //description

    // 3 id tabel comment
    // news
    // 1 id news
    //viko
    // email
    //description

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment');
    }
}
