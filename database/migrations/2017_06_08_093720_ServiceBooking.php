<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServiceBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_booking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cabang_id');
            $table->string('fullname');
            $table->string('email');
            $table->string('no_polisi');
            $table->string('no_telepon');
            $table->string('jenis_kendaraan');
            $table->string('jenis_service');
            $table->date('tanggal_booking');
            $table->string('jam_booking');
            $table->text('description');
            $table->timestamps();
            $table->string('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_booking');
    }
}
