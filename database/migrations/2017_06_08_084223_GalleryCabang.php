<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GalleryCabang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_cabang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cabang_id');
            $table->integer('user_id');
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('description');
            $table->string('img_path');
            $table->dateTime('date_time');
            $table->timestamps();
            $table->string('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_cabang');
    }
}
