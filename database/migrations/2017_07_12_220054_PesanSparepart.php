<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PesanSparepart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesan_sparepart', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cabang_id');
            $table->string('jenis_kendaraan');
            $table->integer('product_id');
            $table->string('fullname');
            $table->string('email')->nullable();
            $table->string('no_telepon');
            $table->string('tahun_rakitan');
            $table->text('description');
            $table->timestamps();
            $table->string('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesan_sparepart');
    }
}
