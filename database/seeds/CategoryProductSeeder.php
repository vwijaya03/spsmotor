<?php

use Illuminate\Database\Seeder;
use App\CategoryProductModel;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryProductModel::create([
        	'slug' => str_slug("Honda CUB", '-'),
        	'name' => "Honda CUB",
        	'delete' => 0
        ]);

        CategoryProductModel::create([
        	'slug' => str_slug("Honda Big Bike", '-'),
        	'name' => "Honda Big Bike",
        	'delete' => 0
        ]);

        CategoryProductModel::create([
        	'slug' => str_slug("Honda Matic", '-'),
        	'name' => "Honda Matic",
        	'delete' => 0
        ]);

        CategoryProductModel::create([
        	'slug' => str_slug("Honda Sport", '-'),
        	'name' => "Honda Sport",
        	'delete' => 0
        ]);
    }
}
