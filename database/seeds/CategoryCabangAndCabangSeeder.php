<?php

use Illuminate\Database\Seeder;
use App\CategoryCabangModel;
use App\CabangModel;

class CategoryCabangAndCabangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryCabangModel::create([
        	'slug' => str_slug('SPS Jatim', '-'),
        	'name' => 'SPS Jatim',
        	'delete' => 0,
        ]);

        CategoryCabangModel::create([
        	'slug' => str_slug('SPS Jateng', '-'),
        	'name' => 'SPS Jateng',
        	'delete' => 0,
        ]);

        CategoryCabangModel::create([
        	'slug' => str_slug('SPS Sumsel', '-'),
        	'name' => 'SPS Sumsel',
        	'delete' => 0,
        ]);

        CabangModel::create([
        	'category_cabang_id' => 1,
        	'user_id' => 1,
        	'slug' => str_slug('SPS Gresik', '-'),
            'email_bengkel_cabang' => 'email_cabang1@mail.com',
            'email_pic_cabang' => 'email_pic1@mail.com',
            'email_technical_service_department_cabang' => 'email_technical_service_department1@mail.com',
            'email_customer_service_cabang' => 'email_customer_service1@mail.com',
            'fb_url' => 'facebook.com',
            'twitter_url' => 'twitter.com',
            'ig_url' => 'instagram.com',
        	'name' => 'SPS Gresik',
        	'description' => 'SPS Gresik',
        	'img_path' => '/public/no-image.jpg',
            'address' => 'Gresik',
            'no_telepon' => '031 111 222 333',
            'informasi_hari_dan_jam' => '',
        	'date_time' => date('Y-m-d H:i:s'),
        	'lat' => 0.01,
        	'lng' => 0.01,
        	'delete' => 0,
        ]);

        CabangModel::create([
        	'category_cabang_id' => 2,
        	'user_id' => 1,
        	'slug' => str_slug('SPS Sleman', '-'),
            'email_bengkel_cabang' => 'email_cabang2@mail.com',
            'email_pic_cabang' => 'email_pic2@mail.com',
            'email_technical_service_department_cabang' => 'email_technical_service_department2@mail.com',
            'email_customer_service_cabang' => 'email_customer_service2@mail.com',
            'fb_url' => 'facebook.com',
            'twitter_url' => 'twitter.com',
            'ig_url' => 'instagram.com',
        	'name' => 'SPS Sleman',
        	'description' => 'SPS Sleman',
        	'img_path' => '/public/no-image.jpg',
            'address' => 'Sleman',
            'no_telepon' => '031 111 222 333',
            'informasi_hari_dan_jam' => '',
        	'date_time' => date('Y-m-d H:i:s'),
        	'lat' => 0.02,
        	'lng' => 0.02,
        	'delete' => 0,
        ]);

        CabangModel::create([
        	'category_cabang_id' => 3,
        	'user_id' => 1,
        	'slug' => str_slug('SPS Palembang', '-'),
            'email_bengkel_cabang' => 'email_cabang3@mail.com',
            'email_pic_cabang' => 'email_pic3@mail.com',
            'email_technical_service_department_cabang' => 'email_technical_service_department3@mail.com',
            'email_customer_service_cabang' => 'email_customer_service3@mail.com',
            'fb_url' => 'facebook.com',
            'twitter_url' => 'twitter.com',
            'ig_url' => 'instagram.com',
        	'name' => 'SPS Palembang',
        	'description' => 'SPS Palembang',
        	'img_path' => '/public/no-image.jpg',
            'address' => 'Palembang',
            'no_telepon' => '031 111 222 333',
            'informasi_hari_dan_jam' => '',
        	'date_time' => date('Y-m-d H:i:s'),
        	'lat' => 0.03,
        	'lng' => 0.03,
        	'delete' => 0,
        ]);
    }
}
