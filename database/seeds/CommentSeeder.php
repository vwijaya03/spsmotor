<?php

use Illuminate\Database\Seeder;
use App\CommentModel;

class CommentSeeder extends Seeder
{
    // Approval, 0 Pending, 1 Approved, 2 Rejected
    public function run()
    {
        CommentModel::create
		([
			'tipe' => "product",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 1",
			'comment_email' => "dummy1@mail.com",
			'comment_description' => "mau tanya ini kok bisa gitu kok bisa gitu gitu yaaa gitu kok bisa gitu gitu yaaa gitu kok bisa gitu gitu yaaa gitu kok bisa gitu gitu yaaa ?",
			'approval' => "0",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "product",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 1",
			'comment_email' => "dummy1@mail.com",
			'comment_description' => "Loh ?",
			'approval' => "0",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "product",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 1",
			'comment_email' => "dummy1@mail.com",
			'comment_description' => "Gini lo ?",
			'approval' => "0",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "product",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 2",
			'comment_email' => "dummy2@mail.com",
			'comment_description' => "Product nya kok gini ?",
			'approval' => "0",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "product",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 2",
			'comment_email' => "dummy2@mail.com",
			'comment_description' => "Harus nya itu product nya seperti ini",
			'approval' => "1",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "product",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 3",
			'comment_email' => "dummy3@mail.com",
			'comment_description' => "Kampret lu",
			'approval' => "2",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "news",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 4",
			'comment_email' => "dummy4@mail.com",
			'comment_description' => "Berita nya bener ga ini ?",
			'approval' => "0",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "news",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 5",
			'comment_email' => "dummy5@mail.com",
			'comment_description' => "ya bener lah, gimana sih lo",
			'approval' => "0",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "news",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 5",
			'comment_email' => "dummy5@mail.com",
			'comment_description' => "Berita nya udah bener ini",
			'approval' => "1",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);

		CommentModel::create
		([
			'tipe' => "news",
			'product_or_news_id' => "1",
			'comment_fullname' => "Dummy 4",
			'comment_email' => "dummy4@mail.com",
			'comment_description' => "Kampret berita nya",
			'approval' => "2",
			'date_time' => date('Y-m-d H:i:s'),
			'delete' => 0
		]);
    }
}
