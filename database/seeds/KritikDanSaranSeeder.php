<?php

use Illuminate\Database\Seeder;
use App\KritikDanSaranModel;

class KritikDanSaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KritikDanSaranModel::create([
        	'cabang_id' => 1,
        	'fullname' => 'Dummy 1',
        	'email' => 'dummy1@mail.com',
        	'no_telepon' => '1234567',
        	'pesan' => 'harus nya cabang 1 begini',
        	'date_time' => date('Y-m-d H:i:s'),
        	'delete' => 0
        ]);

        KritikDanSaranModel::create([
        	'cabang_id' => 1,
        	'fullname' => 'Dummy 2',
        	'email' => 'dummy2@mail.com',
        	'no_telepon' => '1234568',
        	'pesan' => 'harus nya cabang 1 begini begitu',
        	'date_time' => date('Y-m-d H:i:s'),
        	'delete' => 0
        ]);

        KritikDanSaranModel::create([
        	'cabang_id' => 2,
        	'fullname' => 'Dummy 3',
        	'email' => 'dummy3@mail.com',
        	'no_telepon' => '1234569',
        	'pesan' => 'harus nya cabang 2 begini',
        	'date_time' => date('Y-m-d H:i:s'),
        	'delete' => 0
        ]);

        KritikDanSaranModel::create([
        	'cabang_id' => 3,
        	'fullname' => 'Dummy 4',
        	'email' => 'dummy4@mail.com',
        	'no_telepon' => '12345610',
        	'pesan' => 'harus nya cabang 3 begini',
        	'date_time' => date('Y-m-d H:i:s'),
        	'delete' => 0
        ]);
    }
}
