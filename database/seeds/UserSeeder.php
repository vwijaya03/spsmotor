<?php

use Illuminate\Database\Seeder;
use App\UserModel;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::create
		([
			'fullname' => "Viko Wijaya",
			'username' => "viko",
			'email' => "viko_wijaya@yahoo.co.id",
			'password' => Hash::make("viko"),
			'phone' => "12345678910",
			'delete' => 0
		]);
    }
}
